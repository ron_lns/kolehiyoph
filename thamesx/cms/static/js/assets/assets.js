(window.studiofrontendJsonP=window.studiofrontendJsonP||[]).push([[7], {
    426:function(e) {
        e.exports= {
            403: {
                message: "You are logged in to Studio, but your account doesn't have the correct permissions", alertLevel: "warning", link: null
            }
            , 404: {
                message:"You are not logged in to Studio.", alertLevel:"warning", link: {
                    url: "http://cms.thames.edu.ph/signin", text: "Log in."
                }
            }
            , 500: {
                message: "Server error from Studio", alertLevel: "danger", link: null
            }
            , 504: {
                message: "Studio is not running", alertLevel: "danger", link: null
            }
        }
    }
    , 427:function(e, t) {
        e.exports= {
            root: "root", SFE: "SFE", "m-0": "m-0", "mt-0": "mt-0", "my-0": "my-0", "mr-0": "mr-0", "mx-0": "mx-0", "mb-0": "mb-0", "ml-0": "ml-0", "m-1": "m-1", "mt-1": "mt-1", "my-1": "my-1", "mr-1": "mr-1", "mx-1": "mx-1", "mb-1": "mb-1", "ml-1": "ml-1", "m-2": "m-2", "mt-2": "mt-2", "my-2": "my-2", "mr-2": "mr-2", "mx-2": "mx-2", "mb-2": "mb-2", "ml-2": "ml-2", "m-3": "m-3", "mt-3": "mt-3", "my-3": "my-3", "mr-3": "mr-3", "mx-3": "mx-3", "mb-3": "mb-3", "ml-3": "ml-3", "m-4": "m-4", "mt-4": "mt-4", "my-4": "my-4", "mr-4": "mr-4", "mx-4": "mx-4", "mb-4": "mb-4", "ml-4": "ml-4", "m-5": "m-5", "mt-5": "mt-5", "my-5": "my-5", "mr-5": "mr-5", "mx-5": "mx-5", "mb-5": "mb-5", "ml-5": "ml-5", "p-0": "p-0", "pt-0": "pt-0", "py-0": "py-0", "pr-0": "pr-0", "px-0": "px-0", "pb-0": "pb-0", "pl-0": "pl-0", "p-1": "p-1", "pt-1": "pt-1", "py-1": "py-1", "pr-1": "pr-1", "px-1": "px-1", "pb-1": "pb-1", "pl-1": "pl-1", "p-2": "p-2", "pt-2": "pt-2", "py-2": "py-2", "pr-2": "pr-2", "px-2": "px-2", "pb-2": "pb-2", "pl-2": "pl-2", "p-3": "p-3", "pt-3": "pt-3", "py-3": "py-3", "pr-3": "pr-3", "px-3": "px-3", "pb-3": "pb-3", "pl-3": "pl-3", "p-4": "p-4", "pt-4": "pt-4", "py-4": "py-4", "pr-4": "pr-4", "px-4": "px-4", "pb-4": "pb-4", "pl-4": "pl-4", "p-5": "p-5", "pt-5": "pt-5", "py-5": "py-5", "pr-5": "pr-5", "px-5": "px-5", "pb-5": "pb-5", "pl-5": "pl-5", "m-auto": "m-auto", "mt-auto": "mt-auto", "my-auto": "my-auto", "mr-auto": "mr-auto", "mx-auto": "mx-auto", "mb-auto": "mb-auto", "ml-auto": "ml-auto", "m-sm-0": "m-sm-0", "mt-sm-0": "mt-sm-0", "my-sm-0": "my-sm-0", "mr-sm-0": "mr-sm-0", "mx-sm-0": "mx-sm-0", "mb-sm-0": "mb-sm-0", "ml-sm-0": "ml-sm-0", "m-sm-1": "m-sm-1", "mt-sm-1": "mt-sm-1", "my-sm-1": "my-sm-1", "mr-sm-1": "mr-sm-1", "mx-sm-1": "mx-sm-1", "mb-sm-1": "mb-sm-1", "ml-sm-1": "ml-sm-1", "m-sm-2": "m-sm-2", "mt-sm-2": "mt-sm-2", "my-sm-2": "my-sm-2", "mr-sm-2": "mr-sm-2", "mx-sm-2": "mx-sm-2", "mb-sm-2": "mb-sm-2", "ml-sm-2": "ml-sm-2", "m-sm-3": "m-sm-3", "mt-sm-3": "mt-sm-3", "my-sm-3": "my-sm-3", "mr-sm-3": "mr-sm-3", "mx-sm-3": "mx-sm-3", "mb-sm-3": "mb-sm-3", "ml-sm-3": "ml-sm-3", "m-sm-4": "m-sm-4", "mt-sm-4": "mt-sm-4", "my-sm-4": "my-sm-4", "mr-sm-4": "mr-sm-4", "mx-sm-4": "mx-sm-4", "mb-sm-4": "mb-sm-4", "ml-sm-4": "ml-sm-4", "m-sm-5": "m-sm-5", "mt-sm-5": "mt-sm-5", "my-sm-5": "my-sm-5", "mr-sm-5": "mr-sm-5", "mx-sm-5": "mx-sm-5", "mb-sm-5": "mb-sm-5", "ml-sm-5": "ml-sm-5", "p-sm-0": "p-sm-0", "pt-sm-0": "pt-sm-0", "py-sm-0": "py-sm-0", "pr-sm-0": "pr-sm-0", "px-sm-0": "px-sm-0", "pb-sm-0": "pb-sm-0", "pl-sm-0": "pl-sm-0", "p-sm-1": "p-sm-1", "pt-sm-1": "pt-sm-1", "py-sm-1": "py-sm-1", "pr-sm-1": "pr-sm-1", "px-sm-1": "px-sm-1", "pb-sm-1": "pb-sm-1", "pl-sm-1": "pl-sm-1", "p-sm-2": "p-sm-2", "pt-sm-2": "pt-sm-2", "py-sm-2": "py-sm-2", "pr-sm-2": "pr-sm-2", "px-sm-2": "px-sm-2", "pb-sm-2": "pb-sm-2", "pl-sm-2": "pl-sm-2", "p-sm-3": "p-sm-3", "pt-sm-3": "pt-sm-3", "py-sm-3": "py-sm-3", "pr-sm-3": "pr-sm-3", "px-sm-3": "px-sm-3", "pb-sm-3": "pb-sm-3", "pl-sm-3": "pl-sm-3", "p-sm-4": "p-sm-4", "pt-sm-4": "pt-sm-4", "py-sm-4": "py-sm-4", "pr-sm-4": "pr-sm-4", "px-sm-4": "px-sm-4", "pb-sm-4": "pb-sm-4", "pl-sm-4": "pl-sm-4", "p-sm-5": "p-sm-5", "pt-sm-5": "pt-sm-5", "py-sm-5": "py-sm-5", "pr-sm-5": "pr-sm-5", "px-sm-5": "px-sm-5", "pb-sm-5": "pb-sm-5", "pl-sm-5": "pl-sm-5", "m-sm-auto": "m-sm-auto", "mt-sm-auto": "mt-sm-auto", "my-sm-auto": "my-sm-auto", "mr-sm-auto": "mr-sm-auto", "mx-sm-auto": "mx-sm-auto", "mb-sm-auto": "mb-sm-auto", "ml-sm-auto": "ml-sm-auto", "m-md-0": "m-md-0", "mt-md-0": "mt-md-0", "my-md-0": "my-md-0", "mr-md-0": "mr-md-0", "mx-md-0": "mx-md-0", "mb-md-0": "mb-md-0", "ml-md-0": "ml-md-0", "m-md-1": "m-md-1", "mt-md-1": "mt-md-1", "my-md-1": "my-md-1", "mr-md-1": "mr-md-1", "mx-md-1": "mx-md-1", "mb-md-1": "mb-md-1", "ml-md-1": "ml-md-1", "m-md-2": "m-md-2", "mt-md-2": "mt-md-2", "my-md-2": "my-md-2", "mr-md-2": "mr-md-2", "mx-md-2": "mx-md-2", "mb-md-2": "mb-md-2", "ml-md-2": "ml-md-2", "m-md-3": "m-md-3", "mt-md-3": "mt-md-3", "my-md-3": "my-md-3", "mr-md-3": "mr-md-3", "mx-md-3": "mx-md-3", "mb-md-3": "mb-md-3", "ml-md-3": "ml-md-3", "m-md-4": "m-md-4", "mt-md-4": "mt-md-4", "my-md-4": "my-md-4", "mr-md-4": "mr-md-4", "mx-md-4": "mx-md-4", "mb-md-4": "mb-md-4", "ml-md-4": "ml-md-4", "m-md-5": "m-md-5", "mt-md-5": "mt-md-5", "my-md-5": "my-md-5", "mr-md-5": "mr-md-5", "mx-md-5": "mx-md-5", "mb-md-5": "mb-md-5", "ml-md-5": "ml-md-5", "p-md-0": "p-md-0", "pt-md-0": "pt-md-0", "py-md-0": "py-md-0", "pr-md-0": "pr-md-0", "px-md-0": "px-md-0", "pb-md-0": "pb-md-0", "pl-md-0": "pl-md-0", "p-md-1": "p-md-1", "pt-md-1": "pt-md-1", "py-md-1": "py-md-1", "pr-md-1": "pr-md-1", "px-md-1": "px-md-1", "pb-md-1": "pb-md-1", "pl-md-1": "pl-md-1", "p-md-2": "p-md-2", "pt-md-2": "pt-md-2", "py-md-2": "py-md-2", "pr-md-2": "pr-md-2", "px-md-2": "px-md-2", "pb-md-2": "pb-md-2", "pl-md-2": "pl-md-2", "p-md-3": "p-md-3", "pt-md-3": "pt-md-3", "py-md-3": "py-md-3", "pr-md-3": "pr-md-3", "px-md-3": "px-md-3", "pb-md-3": "pb-md-3", "pl-md-3": "pl-md-3", "p-md-4": "p-md-4", "pt-md-4": "pt-md-4", "py-md-4": "py-md-4", "pr-md-4": "pr-md-4", "px-md-4": "px-md-4", "pb-md-4": "pb-md-4", "pl-md-4": "pl-md-4", "p-md-5": "p-md-5", "pt-md-5": "pt-md-5", "py-md-5": "py-md-5", "pr-md-5": "pr-md-5", "px-md-5": "px-md-5", "pb-md-5": "pb-md-5", "pl-md-5": "pl-md-5", "m-md-auto": "m-md-auto", "mt-md-auto": "mt-md-auto", "my-md-auto": "my-md-auto", "mr-md-auto": "mr-md-auto", "mx-md-auto": "mx-md-auto", "mb-md-auto": "mb-md-auto", "ml-md-auto": "ml-md-auto", "m-lg-0": "m-lg-0", "mt-lg-0": "mt-lg-0", "my-lg-0": "my-lg-0", "mr-lg-0": "mr-lg-0", "mx-lg-0": "mx-lg-0", "mb-lg-0": "mb-lg-0", "ml-lg-0": "ml-lg-0", "m-lg-1": "m-lg-1", "mt-lg-1": "mt-lg-1", "my-lg-1": "my-lg-1", "mr-lg-1": "mr-lg-1", "mx-lg-1": "mx-lg-1", "mb-lg-1": "mb-lg-1", "ml-lg-1": "ml-lg-1", "m-lg-2": "m-lg-2", "mt-lg-2": "mt-lg-2", "my-lg-2": "my-lg-2", "mr-lg-2": "mr-lg-2", "mx-lg-2": "mx-lg-2", "mb-lg-2": "mb-lg-2", "ml-lg-2": "ml-lg-2", "m-lg-3": "m-lg-3", "mt-lg-3": "mt-lg-3", "my-lg-3": "my-lg-3", "mr-lg-3": "mr-lg-3", "mx-lg-3": "mx-lg-3", "mb-lg-3": "mb-lg-3", "ml-lg-3": "ml-lg-3", "m-lg-4": "m-lg-4", "mt-lg-4": "mt-lg-4", "my-lg-4": "my-lg-4", "mr-lg-4": "mr-lg-4", "mx-lg-4": "mx-lg-4", "mb-lg-4": "mb-lg-4", "ml-lg-4": "ml-lg-4", "m-lg-5": "m-lg-5", "mt-lg-5": "mt-lg-5", "my-lg-5": "my-lg-5", "mr-lg-5": "mr-lg-5", "mx-lg-5": "mx-lg-5", "mb-lg-5": "mb-lg-5", "ml-lg-5": "ml-lg-5", "p-lg-0": "p-lg-0", "pt-lg-0": "pt-lg-0", "py-lg-0": "py-lg-0", "pr-lg-0": "pr-lg-0", "px-lg-0": "px-lg-0", "pb-lg-0": "pb-lg-0", "pl-lg-0": "pl-lg-0", "p-lg-1": "p-lg-1", "pt-lg-1": "pt-lg-1", "py-lg-1": "py-lg-1", "pr-lg-1": "pr-lg-1", "px-lg-1": "px-lg-1", "pb-lg-1": "pb-lg-1", "pl-lg-1": "pl-lg-1", "p-lg-2": "p-lg-2", "pt-lg-2": "pt-lg-2", "py-lg-2": "py-lg-2", "pr-lg-2": "pr-lg-2", "px-lg-2": "px-lg-2", "pb-lg-2": "pb-lg-2", "pl-lg-2": "pl-lg-2", "p-lg-3": "p-lg-3", "pt-lg-3": "pt-lg-3", "py-lg-3": "py-lg-3", "pr-lg-3": "pr-lg-3", "px-lg-3": "px-lg-3", "pb-lg-3": "pb-lg-3", "pl-lg-3": "pl-lg-3", "p-lg-4": "p-lg-4", "pt-lg-4": "pt-lg-4", "py-lg-4": "py-lg-4", "pr-lg-4": "pr-lg-4", "px-lg-4": "px-lg-4", "pb-lg-4": "pb-lg-4", "pl-lg-4": "pl-lg-4", "p-lg-5": "p-lg-5", "pt-lg-5": "pt-lg-5", "py-lg-5": "py-lg-5", "pr-lg-5": "pr-lg-5", "px-lg-5": "px-lg-5", "pb-lg-5": "pb-lg-5", "pl-lg-5": "pl-lg-5", "m-lg-auto": "m-lg-auto", "mt-lg-auto": "mt-lg-auto", "my-lg-auto": "my-lg-auto", "mr-lg-auto": "mr-lg-auto", "mx-lg-auto": "mx-lg-auto", "mb-lg-auto": "mb-lg-auto", "ml-lg-auto": "ml-lg-auto", "m-xl-0": "m-xl-0", "mt-xl-0": "mt-xl-0", "my-xl-0": "my-xl-0", "mr-xl-0": "mr-xl-0", "mx-xl-0": "mx-xl-0", "mb-xl-0": "mb-xl-0", "ml-xl-0": "ml-xl-0", "m-xl-1": "m-xl-1", "mt-xl-1": "mt-xl-1", "my-xl-1": "my-xl-1", "mr-xl-1": "mr-xl-1", "mx-xl-1": "mx-xl-1", "mb-xl-1": "mb-xl-1", "ml-xl-1": "ml-xl-1", "m-xl-2": "m-xl-2", "mt-xl-2": "mt-xl-2", "my-xl-2": "my-xl-2", "mr-xl-2": "mr-xl-2", "mx-xl-2": "mx-xl-2", "mb-xl-2": "mb-xl-2", "ml-xl-2": "ml-xl-2", "m-xl-3": "m-xl-3", "mt-xl-3": "mt-xl-3", "my-xl-3": "my-xl-3", "mr-xl-3": "mr-xl-3", "mx-xl-3": "mx-xl-3", "mb-xl-3": "mb-xl-3", "ml-xl-3": "ml-xl-3", "m-xl-4": "m-xl-4", "mt-xl-4": "mt-xl-4", "my-xl-4": "my-xl-4", "mr-xl-4": "mr-xl-4", "mx-xl-4": "mx-xl-4", "mb-xl-4": "mb-xl-4", "ml-xl-4": "ml-xl-4", "m-xl-5": "m-xl-5", "mt-xl-5": "mt-xl-5", "my-xl-5": "my-xl-5", "mr-xl-5": "mr-xl-5", "mx-xl-5": "mx-xl-5", "mb-xl-5": "mb-xl-5", "ml-xl-5": "ml-xl-5", "p-xl-0": "p-xl-0", "pt-xl-0": "pt-xl-0", "py-xl-0": "py-xl-0", "pr-xl-0": "pr-xl-0", "px-xl-0": "px-xl-0", "pb-xl-0": "pb-xl-0", "pl-xl-0": "pl-xl-0", "p-xl-1": "p-xl-1", "pt-xl-1": "pt-xl-1", "py-xl-1": "py-xl-1", "pr-xl-1": "pr-xl-1", "px-xl-1": "px-xl-1", "pb-xl-1": "pb-xl-1", "pl-xl-1": "pl-xl-1", "p-xl-2": "p-xl-2", "pt-xl-2": "pt-xl-2", "py-xl-2": "py-xl-2", "pr-xl-2": "pr-xl-2", "px-xl-2": "px-xl-2", "pb-xl-2": "pb-xl-2", "pl-xl-2": "pl-xl-2", "p-xl-3": "p-xl-3", "pt-xl-3": "pt-xl-3", "py-xl-3": "py-xl-3", "pr-xl-3": "pr-xl-3", "px-xl-3": "px-xl-3", "pb-xl-3": "pb-xl-3", "pl-xl-3": "pl-xl-3", "p-xl-4": "p-xl-4", "pt-xl-4": "pt-xl-4", "py-xl-4": "py-xl-4", "pr-xl-4": "pr-xl-4", "px-xl-4": "px-xl-4", "pb-xl-4": "pb-xl-4", "pl-xl-4": "pl-xl-4", "p-xl-5": "p-xl-5", "pt-xl-5": "pt-xl-5", "py-xl-5": "py-xl-5", "pr-xl-5": "pr-xl-5", "px-xl-5": "px-xl-5", "pb-xl-5": "pb-xl-5", "pl-xl-5": "pl-xl-5", "m-xl-auto": "m-xl-auto", "mt-xl-auto": "mt-xl-auto", "my-xl-auto": "my-xl-auto", "mr-xl-auto": "mr-xl-auto", "mx-xl-auto": "mx-xl-auto", "mb-xl-auto": "mb-xl-auto", "ml-xl-auto": "ml-xl-auto", "sr-only": "sr-only", "sr-only-focusable": "sr-only-focusable", "api-error": "api-error"
        }
    }
    , 428:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=function() {
            function e(e, t) {
                for(var l=0;
                l<t.length;
                l++) {
                    var m=t[l];
                    m.enumerable=m.enumerable||!1, m.configurable=!0, "value"in m&&(m.writable=!0), Object.defineProperty(e, m.key, m)
                }
            }
            return function(t, l, m) {
                return l&&e(t.prototype, l), m&&e(t, m), t
            }
        }
        (), s=i(l(2)), a=l(9), p=i(l(1)), r=i(l(35)), d=l(19), o=i(l(427)), u=i(l(426)), n=l(156);
        function i(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var x=function(e) {
            function t(e) {
                !function(e, t) {
                    if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")
                }
                (this, t);
                var l=function(e, t) {
                    if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return!t||"object"!=typeof t&&"function"!=typeof t?e: t
                }
                (this, (t.__proto__||Object.getPrototypeOf(t)).call(this, e));
                return l.state= {
                    apiConnectionStatus: 200
                }
                , l
            }
            return function(e, t) {
                if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);
                e.prototype=Object.create(t&&t.prototype, {
                    constructor: {
                        value: e, enumerable: !1, writable: !0, configurable: !0
                    }
                }
                ), t&&(Object.setPrototypeOf?Object.setPrototypeOf(e, t):e.__proto__=t)
            }
            (t, s.default.Component), m(t, [ {
                key:"componentDidMount", value:function() {
                    this.props.pingStudio()
                }
            }
            , {
                key:"renderStatusMessage", value:function() {
                    var e=u.default[this.props.connectionStatus];
                    return e.link?s.default.createElement("span", null, e.message, " ", s.default.createElement("a", {
                        href: e.link.url, className: o.default["alert-link"]
                    }
                    , e.link.text)):e.message
                }
            }
            , {
                key:"render", value:function() {
                    var e=u.default[this.props.connectionStatus];
                    return e&&200!==this.props.connectionStatus?s.default.createElement("div", {
                        className: (0, r.default)(o.default["api-error"], o.default.alert, o.default["alert-"+e.alertLevel])
                    }
                    , s.default.createElement(d.Button, {
                        label: "↻", buttonType: "sm", className: [o.default["btn-outline-primary"]], onClick: this.props.pingStudio
                    }
                    ), " ", this.renderStatusMessage()):null
                }
            }
            ]), t
        }
        ();
        x.propTypes= {
            connectionStatus: p.default.number, pingStudio: p.default.func.isRequired
        }
        , x.defaultProps= {
            connectionStatus: null
        }
        ;
        var g=(0, a.connect)(function(e) {
            return {
                connectionStatus: e.connectionStatus
            }
        }
        , function(e) {
            return {
                pingStudio:function() {
                    return e((0, n.pingStudio)())
                }
            }
        }
        )(x);
        t.default=g
    }
    , 429:function(e, t) {
        e.exports= {
            root: "root", SFE: "SFE", "skip-link": "skip-link", "sr-only-focusable": "sr-only-focusable"
        }
    }
    , 430:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsPageNoResultsMessage: {
                id: "assetsPageNoResultsMessage", defaultMessage: "0 files found", description: "Message displayed in table when no results are found."
            }
            , assetsPageNoAssetsMessage: {
                id: "assetsPageNoAssetsMessage", defaultMessage: "0 files uploaded", description: "Message displayed when a course has no files to display in the table."
            }
            , assetsPageSkipLink: {
                id: "assetsPageSkipLink", defaultMessage: "Skip to table contents", description: "Link text for a link that will skip user focus over table filters to table contents."
            }
        }
        );
        t.default=m
    }
    , 431:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsClearFiltersButtonLabel: {
                id: "assetsClearFiltersButtonLabel", defaultMessage: "Clear all filters", description: "Label for a button that clears all filters applied to a table."
            }
        }
        );
        t.default=m
    }
    , 432:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=d(l(2)), s=d(l(1)), a=l(19), p=d(l(15)), r=d(l(431));
        function d(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var o=function(e) {
            var t=e.className, l=e.clearFilters, s=e.courseDetails;
            return m.default.createElement(a.Button, {
                className:[t], buttonType:"link", onClick:function() {
                    return l(s)
                }
                , label:m.default.createElement(p.default, {
                    message: r.default.assetsClearFiltersButtonLabel
                }
                )
            }
            )
        }
        ;
        o.propTypes= {
            className:s.default.string, clearFilters:s.default.func.isRequired, courseDetails:s.default.shape( {
                lang: s.default.string, url_name: s.default.string, name: s.default.string, display_course_number: s.default.string, num: s.default.string, org: s.default.string, id: s.default.string, revision: s.default.string
            }
            ).isRequired
        }
        , o.defaultProps= {
            className: ""
        }
        , t.default=o
    }
    , 433:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(432), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        , r=l(24);
        var d=(0, s.connect)(function(e) {
            return {
                courseDetails: e.studioDetails.course
            }
        }
        , function(e) {
            return {
                clearFilters:function(t) {
                    return e((0, r.clearFilters)(t))
                }
            }
        }
        )(p.default);
        t.default=d
    }
    , 436:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(157), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        ;
        var r=(0, s.connect)(function(e) {
            return {
                paginationMetadata: e.metadata.pagination, filtersMetadata: e.metadata.filters, searchMetadata: e.metadata.search
            }
        }
        )(p.default);
        t.default=r
    }
    , 437:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsStatusAlertGenericUpdateError: {
                id: "assetsTableGenericUpdateError", defaultMessage: "The action could not be completed. Refresh the page, and then try the action again.", description: "States that the action could not be completed and asks the user to refresh the page and try the action again"
            }
            , assetsStatusAlertCantDelete: {
                id:"assetsTableCantDelete", defaultMessage:"Unable to delete {assetName}.", description: "States that an item could not be deleted"
            }
            , assetsStatusAlertDeleteSuccess: {
                id:"assetsTableDeleteSuccess", defaultMessage:"{assetName} has been deleted.", description: "States that an item was successfully deleted"
            }
            , assetsStatusAlertUploadSuccess: {
                id:"assetsTableUploadSuccess", defaultMessage:"{uploaded_count} files successfully uploaded.", description: "States that files were successfully uploaded"
            }
            , assetsStatusAlertUploadInProgress: {
                id:"assetsTableUploadInProgress", defaultMessage:"{uploading_count} files uploading.", description: "States that the file upload operation is in progress"
            }
            , assetsStatusAlertTooManyFiles: {
                id:"assetsTableTooManyFiles", defaultMessage:"The maximum number of files for an upload is {max_count}. No files were uploaded.", description: "Error message shown when too many files are selected for upload"
            }
            , assetsStatusAlertTooMuchData: {
                id:"assetsTableTooMuchData", defaultMessage:"The maximum size for an upload is {max_size} MB. No files were uploaded.", description: "Error message shown when too much data is being uploaded"
            }
            , assetsStatusAlertGenericError: {
                id:"assetsTableGenericError", defaultMessage:"Error uploading {assetName}. Try again.", description: "Generic error message while uploading files"
            }
            , assetsStatusAlertFailedLock: {
                id:"assetsTableFailedLock", defaultMessage:"Failed to toggle lock for {assetName}.", description: "States that there was a failure toggling an item's lock status"
            }
            , assetsStatusAlertLoadingStatus: {
                id: "assetsTableLoadingStatus", defaultMessage: "Loading", description: "Indicates something is loading"
            }
        }
        );
        t.default=m
    }
    , 438:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=function() {
            function e(e, t) {
                for(var l=0;
                l<t.length;
                l++) {
                    var m=t[l];
                    m.enumerable=m.enumerable||!1, m.configurable=!0, "value"in m&&(m.writable=!0), Object.defineProperty(e, m.key, m)
                }
            }
            return function(t, l, m) {
                return l&&e(t.prototype, l), m&&e(t, m), t
            }
        }
        (), s=n(l(2)), a=n(l(1)), p=l(19), r=l(8), d=l(34), o=n(l(15)), u=n(l(437));
        function n(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var i= {
            statusAlertFields: {
                alertDialog: "", alertType: "info"
            }
            , statusAlertOpen:!1, uploadSuccessCount:1
        }
        , x=function(e) {
            function t(e) {
                !function(e, t) {
                    if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")
                }
                (this, t);
                var l=function(e, t) {
                    if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return!t||"object"!=typeof t&&"function"!=typeof t?e: t
                }
                (this, (t.__proto__||Object.getPrototypeOf(t)).call(this, e));
                return l.closeDeleteStatus=function() {
                    l.closeStatusAlert(), l.props.onDeleteStatusAlertClose()
                }
                , l.state=i, l.statusAlertRef= {}
                , l.closeStatusAlert=l.closeStatusAlert.bind(l), l.updateStatusAlertFields=l.updateStatusAlertFields.bind(l), l.updateUploadSuccessCount=l.updateUploadSuccessCount.bind(l), l.closeDeleteStatus=l.closeDeleteStatus.bind(l), l
            }
            return function(e, t) {
                if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);
                e.prototype=Object.create(t&&t.prototype, {
                    constructor: {
                        value: e, enumerable: !1, writable: !0, configurable: !0
                    }
                }
                ), t&&(Object.setPrototypeOf?Object.setPrototypeOf(e, t):e.__proto__=t)
            }
            (t, s.default.Component), m(t, [ {
                key:"componentWillReceiveProps", value:function(e) {
                    var t=e.assetsStatus, l=e.deletedAsset;
                    this.updateStatusAlertFields(t, l)
                }
            }
            , {
                key:"updateStatusAlertFields", value:function(e, t) {
                    var l=t.display_name, m=void 0, a=void 0, p=s.default.createElement(o.default, {
                        message: u.default.assetsStatusAlertGenericUpdateError
                    }
                    );
                    switch(e.type) {
                        case d.assetActions.delete.DELETE_ASSET_FAILURE:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertCantDelete, values: {
                                assetName: l
                            }
                        }
                        ), a="danger";
                        break;
                        case d.assetActions.delete.DELETE_ASSET_SUCCESS:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertDeleteSuccess, values: {
                                assetName: l
                            }
                        }
                        ), a="success";
                        break;
                        case d.assetActions.upload.UPLOAD_ASSET_SUCCESS:this.updateUploadSuccessCount(), m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertUploadSuccess, values: {
                                uploaded_count:s.default.createElement(r.FormattedNumber, {
                                    value: this.state.uploadSuccessCount
                                }
                                )
                            }
                        }
                        ), a="success";
                        break;
                        case d.assetActions.upload.UPLOADING_ASSETS:this.closeStatusAlert(), m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertUploadInProgress, values: {
                                uploading_count:s.default.createElement(r.FormattedNumber, {
                                    value: e.count
                                }
                                )
                            }
                        }
                        ), a="info";
                        break;
                        case d.assetActions.upload.UPLOAD_EXCEED_MAX_COUNT_ERROR:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertTooManyFiles, values: {
                                max_count:s.default.createElement(r.FormattedNumber, {
                                    value: e.maxFileCount
                                }
                                )
                            }
                        }
                        ), a="danger";
                        break;
                        case d.assetActions.upload.UPLOAD_EXCEED_MAX_SIZE_ERROR:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertTooMuchData, values: {
                                max_size:s.default.createElement(r.FormattedNumber, {
                                    value: e.maxFileSizeMB
                                }
                                )
                            }
                        }
                        ), a="danger";
                        break;
                        case d.assetActions.upload.UPLOAD_ASSET_FAILURE:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertGenericError, values: {
                                assetName: e.asset.name
                            }
                        }
                        ), a="danger";
                        break;
                        case d.assetActions.lock.TOGGLING_LOCK_ASSET_FAILURE:m=s.default.createElement(o.default, {
                            message:u.default.assetsStatusAlertFailedLock, values: {
                                assetName: e.asset.name
                            }
                        }
                        ), a="danger";
                        break;
                        case d.assetActions.clear.CLEAR_FILTERS_FAILURE:case d.assetActions.filter.FILTER_UPDATE_FAILURE:case d.assetActions.paginate.PAGE_UPDATE_FAILURE:case d.assetActions.sort.SORT_UPDATE_FAILURE:m=p, a="danger";
                        break;
                        default:return
                    }
                    this.setState( {
                        statusAlertOpen:!0, statusAlertFields: {
                            alertDialog: m, alertType: a
                        }
                    }
                    ), this.statusAlertRef.focus()
                }
            }
            , {
                key:"closeStatusAlert", value:function() {
                    this.props.clearAssetsStatus(), this.setState(i)
                }
            }
            , {
                key:"updateUploadSuccessCount", value:function() {
                    this.setState( {
                        uploadSuccessCount: this.state.uploadSuccessCount+1
                    }
                    )
                }
            }
            , {
                key:"render", value:function() {
                    var e=this, t=this.props, l=t.assetsStatus, m=t.statusAlertRef, a=this.state, r=a.statusAlertFields, o=a.statusAlertOpen, u=this.closeStatusAlert;
                    return Object.prototype.hasOwnProperty.call(d.assetActions.delete, l.type)&&(u=this.closeDeleteStatus), s.default.createElement(p.StatusAlert, {
                        alertType:r.alertType, dialog:r.alertDialog, open:o, onClose:u, ref:function(t) {
                            e.statusAlertRef=t, m(t)
                        }
                    }
                    )
                }
            }
            ]), t
        }
        ();
        t.default=x, x.propTypes= {
            assetsStatus:a.default.shape( {
                response: a.default.object, type: a.default.string
            }
            ).isRequired, clearAssetsStatus:a.default.func.isRequired, deletedAsset:a.default.shape( {
                display_name: a.default.string, content_type: a.default.string, url: a.default.string, date_added: a.default.string, id: a.default.string, portable_url: a.default.string, thumbnail: a.default.string, external_url: a.default.string
            }
            ), onDeleteStatusAlertClose:a.default.func.isRequired, statusAlertRef:a.default.func
        }
        , x.defaultProps= {
            assetsStatus: {}
            , deletedAsset: {}
            , statusAlertRef:function() {}
        }
    }
    , 439:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(24), p=l(438), r=(m=p)&&m.__esModule?m: {
            default: m
        }
        ;
        var d=(0, s.connect)(function(e) {
            return {
                assetsStatus: e.metadata.status, deletedAsset: e.metadata.deletion.deletedAsset
            }
        }
        , function(e) {
            return {
                clearAssetsStatus:function() {
                    return e((0, a.clearAssetsStatus)())
                }
            }
        }
        )(r.default);
        t.default=d
    }
    , 450:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsImagePreviewFilterLabel: {
                id: "assetsImagePreviewFilterLabel", defaultMessage: "Hide File Preview", description: "Label checkbox to hide image previews in the assets table."
            }
        }
        );
        t.default=m
    }
    , 451:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=d(l(2)), s=d(l(1)), a=l(19), p=d(l(15)), r=d(l(450));
        function d(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var o=function(e) {
            var t=e.isImagePreviewEnabled, l=e.updateImagePreview;
            return m.default.createElement(a.CheckBox, {
                id:"imagePreviewCheckbox", name:"imagePreviewCheckbox", label:m.default.createElement(p.default, {
                    message: r.default.assetsImagePreviewFilterLabel
                }
                ), checked:!t, onChange:function(e) {
                    l(!e)
                }
            }
            )
        }
        ;
        o.propTypes= {
            isImagePreviewEnabled: s.default.bool.isRequired, updateImagePreview: s.default.func.isRequired
        }
        , t.default=o
    }
    , 452:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(451), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        , r=l(24);
        var d=(0, s.connect)(function(e) {
            return {
                isImagePreviewEnabled: e.metadata.imagePreview.enabled
            }
        }
        , function(e) {
            return {
                updateImagePreview:function(t) {
                    return e((0, r.updateImagePreview)(t))
                }
            }
        }
        )(p.default);
        t.default=d
    }
    , 453:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsFiltersAudio: {
                id: "assetsFiltersAudio", defaultMessage: "Audio", description: "Label for audio file types"
            }
            , assetsFiltersCode: {
                id: "assetsFiltersCode", defaultMessage: "Code", description: "Label for code file types"
            }
            , assetsFiltersDocuments: {
                id: "assetsFiltersDocuments", defaultMessage: "Document", description: "Label for document file types"
            }
            , assetsFiltersImages: {
                id: "assetsFiltersImages", defaultMessage: "Image", description: "Label for image file types"
            }
            , assetsFiltersOther: {
                id: "assetsFiltersOther", defaultMessage: "Other", description: "Label for other file types"
            }
            , assetsFiltersSectionLabel: {
                id: "assetsFiltersSectionLabel", defaultMessage: "Filter by File Type", description: "Label for the filter section of the page"
            }
        }
        );
        t.default=m
    }
    , 454:function(e, t) {
        e.exports= {
            root: "root", SFE: "SFE", "m-0": "m-0", "mt-0": "mt-0", "my-0": "my-0", "mr-0": "mr-0", "mx-0": "mx-0", "mb-0": "mb-0", "ml-0": "ml-0", "m-1": "m-1", "mt-1": "mt-1", "my-1": "my-1", "mr-1": "mr-1", "mx-1": "mx-1", "mb-1": "mb-1", "ml-1": "ml-1", "m-2": "m-2", "mt-2": "mt-2", "my-2": "my-2", "mr-2": "mr-2", "mx-2": "mx-2", "mb-2": "mb-2", "ml-2": "ml-2", "m-3": "m-3", "mt-3": "mt-3", "my-3": "my-3", "mr-3": "mr-3", "mx-3": "mx-3", "mb-3": "mb-3", "ml-3": "ml-3", "m-4": "m-4", "mt-4": "mt-4", "my-4": "my-4", "mr-4": "mr-4", "mx-4": "mx-4", "mb-4": "mb-4", "ml-4": "ml-4", "m-5": "m-5", "mt-5": "mt-5", "my-5": "my-5", "mr-5": "mr-5", "mx-5": "mx-5", "mb-5": "mb-5", "ml-5": "ml-5", "p-0": "p-0", "pt-0": "pt-0", "py-0": "py-0", "pr-0": "pr-0", "px-0": "px-0", "pb-0": "pb-0", "pl-0": "pl-0", "p-1": "p-1", "pt-1": "pt-1", "py-1": "py-1", "pr-1": "pr-1", "px-1": "px-1", "pb-1": "pb-1", "pl-1": "pl-1", "p-2": "p-2", "pt-2": "pt-2", "py-2": "py-2", "pr-2": "pr-2", "px-2": "px-2", "pb-2": "pb-2", "pl-2": "pl-2", "p-3": "p-3", "pt-3": "pt-3", "filter-heading": "filter-heading", "py-3": "py-3", "pr-3": "pr-3", "px-3": "px-3", "pb-3": "pb-3", "pl-3": "pl-3", "p-4": "p-4", "pt-4": "pt-4", "py-4": "py-4", "pr-4": "pr-4", "px-4": "px-4", "pb-4": "pb-4", "pl-4": "pl-4", "p-5": "p-5", "pt-5": "pt-5", "py-5": "py-5", "pr-5": "pr-5", "px-5": "px-5", "pb-5": "pb-5", "pl-5": "pl-5", "m-auto": "m-auto", "mt-auto": "mt-auto", "my-auto": "my-auto", "mr-auto": "mr-auto", "mx-auto": "mx-auto", "mb-auto": "mb-auto", "ml-auto": "ml-auto", "m-sm-0": "m-sm-0", "mt-sm-0": "mt-sm-0", "my-sm-0": "my-sm-0", "mr-sm-0": "mr-sm-0", "mx-sm-0": "mx-sm-0", "mb-sm-0": "mb-sm-0", "ml-sm-0": "ml-sm-0", "m-sm-1": "m-sm-1", "mt-sm-1": "mt-sm-1", "my-sm-1": "my-sm-1", "mr-sm-1": "mr-sm-1", "mx-sm-1": "mx-sm-1", "mb-sm-1": "mb-sm-1", "ml-sm-1": "ml-sm-1", "m-sm-2": "m-sm-2", "mt-sm-2": "mt-sm-2", "my-sm-2": "my-sm-2", "mr-sm-2": "mr-sm-2", "mx-sm-2": "mx-sm-2", "mb-sm-2": "mb-sm-2", "ml-sm-2": "ml-sm-2", "m-sm-3": "m-sm-3", "mt-sm-3": "mt-sm-3", "my-sm-3": "my-sm-3", "mr-sm-3": "mr-sm-3", "mx-sm-3": "mx-sm-3", "mb-sm-3": "mb-sm-3", "ml-sm-3": "ml-sm-3", "m-sm-4": "m-sm-4", "mt-sm-4": "mt-sm-4", "my-sm-4": "my-sm-4", "mr-sm-4": "mr-sm-4", "mx-sm-4": "mx-sm-4", "mb-sm-4": "mb-sm-4", "ml-sm-4": "ml-sm-4", "m-sm-5": "m-sm-5", "mt-sm-5": "mt-sm-5", "my-sm-5": "my-sm-5", "mr-sm-5": "mr-sm-5", "mx-sm-5": "mx-sm-5", "mb-sm-5": "mb-sm-5", "ml-sm-5": "ml-sm-5", "p-sm-0": "p-sm-0", "pt-sm-0": "pt-sm-0", "py-sm-0": "py-sm-0", "pr-sm-0": "pr-sm-0", "px-sm-0": "px-sm-0", "pb-sm-0": "pb-sm-0", "pl-sm-0": "pl-sm-0", "p-sm-1": "p-sm-1", "pt-sm-1": "pt-sm-1", "py-sm-1": "py-sm-1", "pr-sm-1": "pr-sm-1", "px-sm-1": "px-sm-1", "pb-sm-1": "pb-sm-1", "pl-sm-1": "pl-sm-1", "p-sm-2": "p-sm-2", "pt-sm-2": "pt-sm-2", "py-sm-2": "py-sm-2", "pr-sm-2": "pr-sm-2", "px-sm-2": "px-sm-2", "pb-sm-2": "pb-sm-2", "pl-sm-2": "pl-sm-2", "p-sm-3": "p-sm-3", "pt-sm-3": "pt-sm-3", "py-sm-3": "py-sm-3", "pr-sm-3": "pr-sm-3", "px-sm-3": "px-sm-3", "pb-sm-3": "pb-sm-3", "pl-sm-3": "pl-sm-3", "p-sm-4": "p-sm-4", "pt-sm-4": "pt-sm-4", "py-sm-4": "py-sm-4", "pr-sm-4": "pr-sm-4", "px-sm-4": "px-sm-4", "pb-sm-4": "pb-sm-4", "pl-sm-4": "pl-sm-4", "p-sm-5": "p-sm-5", "pt-sm-5": "pt-sm-5", "py-sm-5": "py-sm-5", "pr-sm-5": "pr-sm-5", "px-sm-5": "px-sm-5", "pb-sm-5": "pb-sm-5", "pl-sm-5": "pl-sm-5", "m-sm-auto": "m-sm-auto", "mt-sm-auto": "mt-sm-auto", "my-sm-auto": "my-sm-auto", "mr-sm-auto": "mr-sm-auto", "mx-sm-auto": "mx-sm-auto", "mb-sm-auto": "mb-sm-auto", "ml-sm-auto": "ml-sm-auto", "m-md-0": "m-md-0", "mt-md-0": "mt-md-0", "my-md-0": "my-md-0", "mr-md-0": "mr-md-0", "mx-md-0": "mx-md-0", "mb-md-0": "mb-md-0", "ml-md-0": "ml-md-0", "m-md-1": "m-md-1", "mt-md-1": "mt-md-1", "my-md-1": "my-md-1", "mr-md-1": "mr-md-1", "mx-md-1": "mx-md-1", "mb-md-1": "mb-md-1", "ml-md-1": "ml-md-1", "m-md-2": "m-md-2", "mt-md-2": "mt-md-2", "my-md-2": "my-md-2", "mr-md-2": "mr-md-2", "mx-md-2": "mx-md-2", "mb-md-2": "mb-md-2", "ml-md-2": "ml-md-2", "m-md-3": "m-md-3", "mt-md-3": "mt-md-3", "my-md-3": "my-md-3", "mr-md-3": "mr-md-3", "mx-md-3": "mx-md-3", "mb-md-3": "mb-md-3", "ml-md-3": "ml-md-3", "m-md-4": "m-md-4", "mt-md-4": "mt-md-4", "my-md-4": "my-md-4", "mr-md-4": "mr-md-4", "mx-md-4": "mx-md-4", "mb-md-4": "mb-md-4", "ml-md-4": "ml-md-4", "m-md-5": "m-md-5", "mt-md-5": "mt-md-5", "my-md-5": "my-md-5", "mr-md-5": "mr-md-5", "mx-md-5": "mx-md-5", "mb-md-5": "mb-md-5", "ml-md-5": "ml-md-5", "p-md-0": "p-md-0", "pt-md-0": "pt-md-0", "py-md-0": "py-md-0", "pr-md-0": "pr-md-0", "px-md-0": "px-md-0", "pb-md-0": "pb-md-0", "pl-md-0": "pl-md-0", "p-md-1": "p-md-1", "pt-md-1": "pt-md-1", "py-md-1": "py-md-1", "pr-md-1": "pr-md-1", "px-md-1": "px-md-1", "pb-md-1": "pb-md-1", "pl-md-1": "pl-md-1", "p-md-2": "p-md-2", "pt-md-2": "pt-md-2", "py-md-2": "py-md-2", "pr-md-2": "pr-md-2", "px-md-2": "px-md-2", "pb-md-2": "pb-md-2", "pl-md-2": "pl-md-2", "p-md-3": "p-md-3", "pt-md-3": "pt-md-3", "py-md-3": "py-md-3", "pr-md-3": "pr-md-3", "px-md-3": "px-md-3", "pb-md-3": "pb-md-3", "pl-md-3": "pl-md-3", "p-md-4": "p-md-4", "pt-md-4": "pt-md-4", "py-md-4": "py-md-4", "pr-md-4": "pr-md-4", "px-md-4": "px-md-4", "pb-md-4": "pb-md-4", "pl-md-4": "pl-md-4", "p-md-5": "p-md-5", "pt-md-5": "pt-md-5", "py-md-5": "py-md-5", "pr-md-5": "pr-md-5", "px-md-5": "px-md-5", "pb-md-5": "pb-md-5", "pl-md-5": "pl-md-5", "m-md-auto": "m-md-auto", "mt-md-auto": "mt-md-auto", "my-md-auto": "my-md-auto", "mr-md-auto": "mr-md-auto", "mx-md-auto": "mx-md-auto", "mb-md-auto": "mb-md-auto", "ml-md-auto": "ml-md-auto", "m-lg-0": "m-lg-0", "mt-lg-0": "mt-lg-0", "my-lg-0": "my-lg-0", "mr-lg-0": "mr-lg-0", "mx-lg-0": "mx-lg-0", "mb-lg-0": "mb-lg-0", "ml-lg-0": "ml-lg-0", "m-lg-1": "m-lg-1", "mt-lg-1": "mt-lg-1", "my-lg-1": "my-lg-1", "mr-lg-1": "mr-lg-1", "mx-lg-1": "mx-lg-1", "mb-lg-1": "mb-lg-1", "ml-lg-1": "ml-lg-1", "m-lg-2": "m-lg-2", "mt-lg-2": "mt-lg-2", "my-lg-2": "my-lg-2", "mr-lg-2": "mr-lg-2", "mx-lg-2": "mx-lg-2", "mb-lg-2": "mb-lg-2", "ml-lg-2": "ml-lg-2", "m-lg-3": "m-lg-3", "mt-lg-3": "mt-lg-3", "my-lg-3": "my-lg-3", "mr-lg-3": "mr-lg-3", "mx-lg-3": "mx-lg-3", "mb-lg-3": "mb-lg-3", "ml-lg-3": "ml-lg-3", "m-lg-4": "m-lg-4", "mt-lg-4": "mt-lg-4", "my-lg-4": "my-lg-4", "mr-lg-4": "mr-lg-4", "mx-lg-4": "mx-lg-4", "mb-lg-4": "mb-lg-4", "ml-lg-4": "ml-lg-4", "m-lg-5": "m-lg-5", "mt-lg-5": "mt-lg-5", "my-lg-5": "my-lg-5", "mr-lg-5": "mr-lg-5", "mx-lg-5": "mx-lg-5", "mb-lg-5": "mb-lg-5", "ml-lg-5": "ml-lg-5", "p-lg-0": "p-lg-0", "pt-lg-0": "pt-lg-0", "py-lg-0": "py-lg-0", "pr-lg-0": "pr-lg-0", "px-lg-0": "px-lg-0", "pb-lg-0": "pb-lg-0", "pl-lg-0": "pl-lg-0", "p-lg-1": "p-lg-1", "pt-lg-1": "pt-lg-1", "py-lg-1": "py-lg-1", "pr-lg-1": "pr-lg-1", "px-lg-1": "px-lg-1", "pb-lg-1": "pb-lg-1", "pl-lg-1": "pl-lg-1", "p-lg-2": "p-lg-2", "pt-lg-2": "pt-lg-2", "py-lg-2": "py-lg-2", "pr-lg-2": "pr-lg-2", "px-lg-2": "px-lg-2", "pb-lg-2": "pb-lg-2", "pl-lg-2": "pl-lg-2", "p-lg-3": "p-lg-3", "pt-lg-3": "pt-lg-3", "py-lg-3": "py-lg-3", "pr-lg-3": "pr-lg-3", "px-lg-3": "px-lg-3", "pb-lg-3": "pb-lg-3", "pl-lg-3": "pl-lg-3", "p-lg-4": "p-lg-4", "pt-lg-4": "pt-lg-4", "py-lg-4": "py-lg-4", "pr-lg-4": "pr-lg-4", "px-lg-4": "px-lg-4", "pb-lg-4": "pb-lg-4", "pl-lg-4": "pl-lg-4", "p-lg-5": "p-lg-5", "pt-lg-5": "pt-lg-5", "py-lg-5": "py-lg-5", "pr-lg-5": "pr-lg-5", "px-lg-5": "px-lg-5", "pb-lg-5": "pb-lg-5", "pl-lg-5": "pl-lg-5", "m-lg-auto": "m-lg-auto", "mt-lg-auto": "mt-lg-auto", "my-lg-auto": "my-lg-auto", "mr-lg-auto": "mr-lg-auto", "mx-lg-auto": "mx-lg-auto", "mb-lg-auto": "mb-lg-auto", "ml-lg-auto": "ml-lg-auto", "m-xl-0": "m-xl-0", "mt-xl-0": "mt-xl-0", "my-xl-0": "my-xl-0", "mr-xl-0": "mr-xl-0", "mx-xl-0": "mx-xl-0", "mb-xl-0": "mb-xl-0", "ml-xl-0": "ml-xl-0", "m-xl-1": "m-xl-1", "mt-xl-1": "mt-xl-1", "my-xl-1": "my-xl-1", "mr-xl-1": "mr-xl-1", "mx-xl-1": "mx-xl-1", "mb-xl-1": "mb-xl-1", "ml-xl-1": "ml-xl-1", "m-xl-2": "m-xl-2", "mt-xl-2": "mt-xl-2", "my-xl-2": "my-xl-2", "mr-xl-2": "mr-xl-2", "mx-xl-2": "mx-xl-2", "mb-xl-2": "mb-xl-2", "ml-xl-2": "ml-xl-2", "m-xl-3": "m-xl-3", "mt-xl-3": "mt-xl-3", "my-xl-3": "my-xl-3", "mr-xl-3": "mr-xl-3", "mx-xl-3": "mx-xl-3", "mb-xl-3": "mb-xl-3", "ml-xl-3": "ml-xl-3", "m-xl-4": "m-xl-4", "mt-xl-4": "mt-xl-4", "my-xl-4": "my-xl-4", "mr-xl-4": "mr-xl-4", "mx-xl-4": "mx-xl-4", "mb-xl-4": "mb-xl-4", "ml-xl-4": "ml-xl-4", "m-xl-5": "m-xl-5", "mt-xl-5": "mt-xl-5", "my-xl-5": "my-xl-5", "mr-xl-5": "mr-xl-5", "mx-xl-5": "mx-xl-5", "mb-xl-5": "mb-xl-5", "ml-xl-5": "ml-xl-5", "p-xl-0": "p-xl-0", "pt-xl-0": "pt-xl-0", "py-xl-0": "py-xl-0", "pr-xl-0": "pr-xl-0", "px-xl-0": "px-xl-0", "pb-xl-0": "pb-xl-0", "pl-xl-0": "pl-xl-0", "p-xl-1": "p-xl-1", "pt-xl-1": "pt-xl-1", "py-xl-1": "py-xl-1", "pr-xl-1": "pr-xl-1", "px-xl-1": "px-xl-1", "pb-xl-1": "pb-xl-1", "pl-xl-1": "pl-xl-1", "p-xl-2": "p-xl-2", "pt-xl-2": "pt-xl-2", "py-xl-2": "py-xl-2", "pr-xl-2": "pr-xl-2", "px-xl-2": "px-xl-2", "pb-xl-2": "pb-xl-2", "pl-xl-2": "pl-xl-2", "p-xl-3": "p-xl-3", "pt-xl-3": "pt-xl-3", "py-xl-3": "py-xl-3", "pr-xl-3": "pr-xl-3", "px-xl-3": "px-xl-3", "pb-xl-3": "pb-xl-3", "pl-xl-3": "pl-xl-3", "p-xl-4": "p-xl-4", "pt-xl-4": "pt-xl-4", "py-xl-4": "py-xl-4", "pr-xl-4": "pr-xl-4", "px-xl-4": "px-xl-4", "pb-xl-4": "pb-xl-4", "pl-xl-4": "pl-xl-4", "p-xl-5": "p-xl-5", "pt-xl-5": "pt-xl-5", "py-xl-5": "py-xl-5", "pr-xl-5": "pr-xl-5", "px-xl-5": "px-xl-5", "pb-xl-5": "pb-xl-5", "pl-xl-5": "pl-xl-5", "m-xl-auto": "m-xl-auto", "mt-xl-auto": "mt-xl-auto", "my-xl-auto": "my-xl-auto", "mr-xl-auto": "mr-xl-auto", "mx-xl-auto": "mx-xl-auto", "mb-xl-auto": "mb-xl-auto", "ml-xl-auto": "ml-xl-auto", "sr-only": "sr-only", "sr-only-focusable": "sr-only-focusable", "form-check": "form-check"
        }
    }
    , 455:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=u(l(2)), s=u(l(1)), a=l(19), p=u(l(164)), r=u(l(15)), d=u(l(454)), o=u(l(453));
        function u(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var n=function(e) {
            var t=e.assetsFilters, l=e.updateFilter, s=e.courseDetails;
            return m.default.createElement("div", {
                role: "group", "aria-labelledby": "filter-label"
            }
            , m.default.createElement(r.default, {
                message: o.default.assetsFiltersSectionLabel
            }
            , function(e) {
                return m.default.createElement("h4", {
                    id: "filter-label", className: d.default["filter-heading"], "data-identifier": "asset-filters-header"
                }
                , e)
            }
            ), m.default.createElement("div", {
                className: d.default["filter-set"], "data-identifier": "asset-filters"
            }
            , m.default.createElement(a.CheckBoxGroup, null, p.default.map(function(e) {
                return m.default.createElement(a.CheckBox, {
                    key:e.key, id:e.key, name:e.key, label:m.default.createElement(r.default, {
                        message: o.default["assetsFilters"+e.displayName]
                    }
                    ), checked:t.assetTypes[e.key], onChange:function(t) {
                        l(e.key, t, s)
                    }
                }
                )
            }
            ))))
        }
        ;
        n.propTypes= {
            assetsFilters:s.default.shape( {
                assetTypes: s.default.object
            }
            ).isRequired, courseDetails:s.default.shape( {
                lang: s.default.string, url_name: s.default.string, name: s.default.string, display_course_number: s.default.string, num: s.default.string, org: s.default.string, id: s.default.string, revision: s.default.string, base_url: s.default.string
            }
            ).isRequired, updateFilter:s.default.func.isRequired
        }
        , t.default=n
    }
    , 456:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(455), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        , r=l(24);
        var d=(0, s.connect)(function(e) {
            return {
                assetsFilters: e.metadata.filters, courseDetails: e.studioDetails.course
            }
        }
        , function(e) {
            return {
                updateFilter:function(t, l, m) {
                    return e((0, r.filterUpdate)(t, l, m))
                }
            }
        }
        )(p.default);
        t.default=d
    }
    , 457:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=(0, l(8).defineMessages)( {
            assetsTablePreviewLabel: {
                id: "assetsTablePreviewLabel", defaultMessage: "Image Preview", description: "Label for column header"
            }
            , assetsTableNameLabel: {
                id: "assetsTableNameLabel", defaultMessage: "Name", description: "Label for column header"
            }
            , assetsTableTypeLable: {
                id: "assetsTableTypeLable", defaultMessage: "Type", description: "Label for column header"
            }
            , assetsTableDateLabel: {
                id: "assetsTableDateLabel", defaultMessage: "Date Added", description: "Label for column header"
            }
            , assetsTableCaption: {
                id: "assetsTableCaption", defaultMessage: "Assets Table", description: "Table specific caption for a high level description"
            }
            , assetsTableCopyLabel: {
                id: "assetsTableCopyLabel", defaultMessage: "Copy URLs", description: "Label for column header"
            }
            , assetsTableDeleteLabel: {
                id: "assetsTableDeleteLabel", defaultMessage: "Delete Asset", description: "Label for column header"
            }
            , assetsTableLockLabel: {
                id: "assetsTableLockLabel", defaultMessage: "Lock Asset", description: "Label for column header"
            }
            , assetsTableNoDescription: {
                id: "assetsTableNoDescription", defaultMessage: "Description not available", description: "Label shown when no description is available"
            }
            , assetsTableNoPreview: {
                id: "assetsTableNoPreview", defaultMessage: "Preview not available", description: "Label shown when no preview is available"
            }
            , assetsTableLockedObject: {
                id:"assetsTableLockedObject", defaultMessage:"Locked {object}", description: "States that an object has just been locked"
            }
            , assetsTableUnlockedObject: {
                id:"assetsTableUnlockedObject", defaultMessage:"Unlocked {object}", description: "States that an object has just been unlocked"
            }
            , assetsTableUpdateLock: {
                id:"assetsTableUpdateLock", defaultMessage:"Updating lock status for {assetName}.", description: "States that the lock status of an item is updating"
            }
            , assetsTableStudioLink: {
                id: "assetsTableStudioLink", defaultMessage: "Studio", description: "Label that indicates a relative (as opposed to an absolute) URL"
            }
            , assetsTableWebLink: {
                id: "assetsTableWebLink", defaultMessage: "Web", description: "Label that indicates an absolute (as opposed to a relative) URL"
            }
            , assetsTableCopiedStatus: {
                id: "assetsTableCopiedStatus", defaultMessage: "Copied", description: "States that a URL was copied"
            }
            , assetsTableDetailedCopyLink: {
                id:"assetsTableDetailedCopyLink", defaultMessage:"{displayName} copy {label} URL", description: "Labels a button that is used to copy the studio/web URL for a given item"
            }
            , assetsTableDeleteObject: {
                id: "assetsTableDeleteObject", defaultMessage: "Delete File", description: "Heading on the modal used to delete a file"
            }
            , assetsTableCancel: {
                id: "assetsTableCancel", defaultMessage: "Cancel", description: 'Labels the "Cancel" button in a modal'
            }
            , assetsTablePermaDelete: {
                id: "assetsTablePermaDelete", defaultMessage: "Permanently delete", description: "Indicates a non-recoverable, permanent delete"
            }
            , assetsTableLearnMore: {
                id: "assetsTableLearnMore", defaultMessage: "Learn more.", description: "Label for a documentation link"
            }
            , assetsTableDeleteWarning: {
                id:"assetsTableDeleteWarning", defaultMessage:"Deleting {displayName} cannot be undone.", description: "Warning that indicates the delete operation is permanent"
            }
            , assetsTableDeleteConsequences: {
                id:"assetsTableDeleteConsequences", defaultMessage:"Any links or references to this file will no longer work. {link}", description: "Warns of the consequences of deleting an item"
            }
        }
        );
        t.default=m
    }
    , 458:function(e, t) {
        e.exports= {
            root: "root", SFE: "SFE", "m-0": "m-0", "mt-0": "mt-0", "my-0": "my-0", "mr-0": "mr-0", "mx-0": "mx-0", "mb-0": "mb-0", "ml-0": "ml-0", "m-1": "m-1", "mt-1": "mt-1", "my-1": "my-1", "mr-1": "mr-1", "mx-1": "mx-1", "mb-1": "mb-1", "ml-1": "ml-1", "m-2": "m-2", "mt-2": "mt-2", "my-2": "my-2", "mr-2": "mr-2", "mx-2": "mx-2", "mb-2": "mb-2", "ml-2": "ml-2", "m-3": "m-3", "mt-3": "mt-3", "my-3": "my-3", "mr-3": "mr-3", "mx-3": "mx-3", "mb-3": "mb-3", "ml-3": "ml-3", "m-4": "m-4", "mt-4": "mt-4", "my-4": "my-4", "mr-4": "mr-4", "mx-4": "mx-4", "mb-4": "mb-4", "ml-4": "ml-4", "m-5": "m-5", "mt-5": "mt-5", "my-5": "my-5", "mr-5": "mr-5", "mx-5": "mx-5", "mb-5": "mb-5", "ml-5": "ml-5", "p-0": "p-0", "pt-0": "pt-0", "py-0": "py-0", "pr-0": "pr-0", "px-0": "px-0", "pb-0": "pb-0", "pl-0": "pl-0", "p-1": "p-1", "pt-1": "pt-1", "py-1": "py-1", "pr-1": "pr-1", "px-1": "px-1", "pb-1": "pb-1", "pl-1": "pl-1", "p-2": "p-2", "pt-2": "pt-2", "py-2": "py-2", "pr-2": "pr-2", "px-2": "px-2", "pb-2": "pb-2", "pl-2": "pl-2", "p-3": "p-3", "pt-3": "pt-3", "py-3": "py-3", "pr-3": "pr-3", "px-3": "px-3", "pb-3": "pb-3", "pl-3": "pl-3", "p-4": "p-4", "pt-4": "pt-4", "py-4": "py-4", "pr-4": "pr-4", "px-4": "px-4", "pb-4": "pb-4", "pl-4": "pl-4", "p-5": "p-5", "pt-5": "pt-5", "py-5": "py-5", "pr-5": "pr-5", "px-5": "px-5", "pb-5": "pb-5", "pl-5": "pl-5", "m-auto": "m-auto", "mt-auto": "mt-auto", "my-auto": "my-auto", "mr-auto": "mr-auto", "mx-auto": "mx-auto", "mb-auto": "mb-auto", "ml-auto": "ml-auto", "m-sm-0": "m-sm-0", "mt-sm-0": "mt-sm-0", "my-sm-0": "my-sm-0", "mr-sm-0": "mr-sm-0", "mx-sm-0": "mx-sm-0", "mb-sm-0": "mb-sm-0", "ml-sm-0": "ml-sm-0", "m-sm-1": "m-sm-1", "mt-sm-1": "mt-sm-1", "my-sm-1": "my-sm-1", "mr-sm-1": "mr-sm-1", "mx-sm-1": "mx-sm-1", "mb-sm-1": "mb-sm-1", "ml-sm-1": "ml-sm-1", "m-sm-2": "m-sm-2", "mt-sm-2": "mt-sm-2", "my-sm-2": "my-sm-2", "mr-sm-2": "mr-sm-2", "mx-sm-2": "mx-sm-2", "mb-sm-2": "mb-sm-2", "ml-sm-2": "ml-sm-2", "m-sm-3": "m-sm-3", "mt-sm-3": "mt-sm-3", "my-sm-3": "my-sm-3", "mr-sm-3": "mr-sm-3", "mx-sm-3": "mx-sm-3", "mb-sm-3": "mb-sm-3", "ml-sm-3": "ml-sm-3", "m-sm-4": "m-sm-4", "mt-sm-4": "mt-sm-4", "my-sm-4": "my-sm-4", "mr-sm-4": "mr-sm-4", "mx-sm-4": "mx-sm-4", "mb-sm-4": "mb-sm-4", "ml-sm-4": "ml-sm-4", "m-sm-5": "m-sm-5", "mt-sm-5": "mt-sm-5", "my-sm-5": "my-sm-5", "mr-sm-5": "mr-sm-5", "mx-sm-5": "mx-sm-5", "mb-sm-5": "mb-sm-5", "ml-sm-5": "ml-sm-5", "p-sm-0": "p-sm-0", "pt-sm-0": "pt-sm-0", "py-sm-0": "py-sm-0", "pr-sm-0": "pr-sm-0", "px-sm-0": "px-sm-0", "pb-sm-0": "pb-sm-0", "pl-sm-0": "pl-sm-0", "p-sm-1": "p-sm-1", "pt-sm-1": "pt-sm-1", "py-sm-1": "py-sm-1", "pr-sm-1": "pr-sm-1", "px-sm-1": "px-sm-1", "pb-sm-1": "pb-sm-1", "pl-sm-1": "pl-sm-1", "p-sm-2": "p-sm-2", "pt-sm-2": "pt-sm-2", "py-sm-2": "py-sm-2", "pr-sm-2": "pr-sm-2", "px-sm-2": "px-sm-2", "pb-sm-2": "pb-sm-2", "pl-sm-2": "pl-sm-2", "p-sm-3": "p-sm-3", "pt-sm-3": "pt-sm-3", "py-sm-3": "py-sm-3", "pr-sm-3": "pr-sm-3", "px-sm-3": "px-sm-3", "pb-sm-3": "pb-sm-3", "pl-sm-3": "pl-sm-3", "p-sm-4": "p-sm-4", "pt-sm-4": "pt-sm-4", "py-sm-4": "py-sm-4", "pr-sm-4": "pr-sm-4", "px-sm-4": "px-sm-4", "pb-sm-4": "pb-sm-4", "pl-sm-4": "pl-sm-4", "p-sm-5": "p-sm-5", "pt-sm-5": "pt-sm-5", "py-sm-5": "py-sm-5", "pr-sm-5": "pr-sm-5", "px-sm-5": "px-sm-5", "pb-sm-5": "pb-sm-5", "pl-sm-5": "pl-sm-5", "m-sm-auto": "m-sm-auto", "mt-sm-auto": "mt-sm-auto", "my-sm-auto": "my-sm-auto", "mr-sm-auto": "mr-sm-auto", "mx-sm-auto": "mx-sm-auto", "mb-sm-auto": "mb-sm-auto", "ml-sm-auto": "ml-sm-auto", "m-md-0": "m-md-0", "mt-md-0": "mt-md-0", "my-md-0": "my-md-0", "mr-md-0": "mr-md-0", "mx-md-0": "mx-md-0", "mb-md-0": "mb-md-0", "ml-md-0": "ml-md-0", "m-md-1": "m-md-1", "mt-md-1": "mt-md-1", "my-md-1": "my-md-1", "mr-md-1": "mr-md-1", "mx-md-1": "mx-md-1", "mb-md-1": "mb-md-1", "ml-md-1": "ml-md-1", "m-md-2": "m-md-2", "mt-md-2": "mt-md-2", "my-md-2": "my-md-2", "mr-md-2": "mr-md-2", "mx-md-2": "mx-md-2", "mb-md-2": "mb-md-2", "ml-md-2": "ml-md-2", "m-md-3": "m-md-3", "mt-md-3": "mt-md-3", "my-md-3": "my-md-3", "mr-md-3": "mr-md-3", "mx-md-3": "mx-md-3", "mb-md-3": "mb-md-3", "ml-md-3": "ml-md-3", "m-md-4": "m-md-4", "mt-md-4": "mt-md-4", "my-md-4": "my-md-4", "mr-md-4": "mr-md-4", "mx-md-4": "mx-md-4", "mb-md-4": "mb-md-4", "ml-md-4": "ml-md-4", "m-md-5": "m-md-5", "mt-md-5": "mt-md-5", "my-md-5": "my-md-5", "mr-md-5": "mr-md-5", "mx-md-5": "mx-md-5", "mb-md-5": "mb-md-5", "ml-md-5": "ml-md-5", "p-md-0": "p-md-0", "pt-md-0": "pt-md-0", "py-md-0": "py-md-0", "pr-md-0": "pr-md-0", "px-md-0": "px-md-0", "pb-md-0": "pb-md-0", "pl-md-0": "pl-md-0", "p-md-1": "p-md-1", "pt-md-1": "pt-md-1", "py-md-1": "py-md-1", "pr-md-1": "pr-md-1", "px-md-1": "px-md-1", "pb-md-1": "pb-md-1", "pl-md-1": "pl-md-1", "p-md-2": "p-md-2", "pt-md-2": "pt-md-2", "py-md-2": "py-md-2", "pr-md-2": "pr-md-2", "px-md-2": "px-md-2", "pb-md-2": "pb-md-2", "pl-md-2": "pl-md-2", "p-md-3": "p-md-3", "pt-md-3": "pt-md-3", "py-md-3": "py-md-3", "pr-md-3": "pr-md-3", "px-md-3": "px-md-3", "pb-md-3": "pb-md-3", "pl-md-3": "pl-md-3", "p-md-4": "p-md-4", "pt-md-4": "pt-md-4", "py-md-4": "py-md-4", "pr-md-4": "pr-md-4", "px-md-4": "px-md-4", "pb-md-4": "pb-md-4", "pl-md-4": "pl-md-4", "p-md-5": "p-md-5", "pt-md-5": "pt-md-5", "py-md-5": "py-md-5", "pr-md-5": "pr-md-5", "px-md-5": "px-md-5", "pb-md-5": "pb-md-5", "pl-md-5": "pl-md-5", "m-md-auto": "m-md-auto", "mt-md-auto": "mt-md-auto", "my-md-auto": "my-md-auto", "mr-md-auto": "mr-md-auto", "mx-md-auto": "mx-md-auto", "mb-md-auto": "mb-md-auto", "ml-md-auto": "ml-md-auto", "m-lg-0": "m-lg-0", "mt-lg-0": "mt-lg-0", "my-lg-0": "my-lg-0", "mr-lg-0": "mr-lg-0", "mx-lg-0": "mx-lg-0", "mb-lg-0": "mb-lg-0", "ml-lg-0": "ml-lg-0", "m-lg-1": "m-lg-1", "mt-lg-1": "mt-lg-1", "my-lg-1": "my-lg-1", "mr-lg-1": "mr-lg-1", "mx-lg-1": "mx-lg-1", "mb-lg-1": "mb-lg-1", "ml-lg-1": "ml-lg-1", "m-lg-2": "m-lg-2", "mt-lg-2": "mt-lg-2", "my-lg-2": "my-lg-2", "mr-lg-2": "mr-lg-2", "mx-lg-2": "mx-lg-2", "mb-lg-2": "mb-lg-2", "ml-lg-2": "ml-lg-2", "m-lg-3": "m-lg-3", "mt-lg-3": "mt-lg-3", "my-lg-3": "my-lg-3", "mr-lg-3": "mr-lg-3", "mx-lg-3": "mx-lg-3", "mb-lg-3": "mb-lg-3", "ml-lg-3": "ml-lg-3", "m-lg-4": "m-lg-4", "mt-lg-4": "mt-lg-4", "my-lg-4": "my-lg-4", "mr-lg-4": "mr-lg-4", "mx-lg-4": "mx-lg-4", "mb-lg-4": "mb-lg-4", "ml-lg-4": "ml-lg-4", "m-lg-5": "m-lg-5", "mt-lg-5": "mt-lg-5", "my-lg-5": "my-lg-5", "mr-lg-5": "mr-lg-5", "mx-lg-5": "mx-lg-5", "mb-lg-5": "mb-lg-5", "ml-lg-5": "ml-lg-5", "p-lg-0": "p-lg-0", "pt-lg-0": "pt-lg-0", "py-lg-0": "py-lg-0", "pr-lg-0": "pr-lg-0", "px-lg-0": "px-lg-0", "pb-lg-0": "pb-lg-0", "pl-lg-0": "pl-lg-0", "p-lg-1": "p-lg-1", "pt-lg-1": "pt-lg-1", "py-lg-1": "py-lg-1", "pr-lg-1": "pr-lg-1", "px-lg-1": "px-lg-1", "pb-lg-1": "pb-lg-1", "pl-lg-1": "pl-lg-1", "p-lg-2": "p-lg-2", "pt-lg-2": "pt-lg-2", "py-lg-2": "py-lg-2", "pr-lg-2": "pr-lg-2", "px-lg-2": "px-lg-2", "pb-lg-2": "pb-lg-2", "pl-lg-2": "pl-lg-2", "p-lg-3": "p-lg-3", "pt-lg-3": "pt-lg-3", "py-lg-3": "py-lg-3", "pr-lg-3": "pr-lg-3", "px-lg-3": "px-lg-3", "pb-lg-3": "pb-lg-3", "pl-lg-3": "pl-lg-3", "p-lg-4": "p-lg-4", "pt-lg-4": "pt-lg-4", "py-lg-4": "py-lg-4", "pr-lg-4": "pr-lg-4", "px-lg-4": "px-lg-4", "pb-lg-4": "pb-lg-4", "pl-lg-4": "pl-lg-4", "p-lg-5": "p-lg-5", "pt-lg-5": "pt-lg-5", "py-lg-5": "py-lg-5", "pr-lg-5": "pr-lg-5", "px-lg-5": "px-lg-5", "pb-lg-5": "pb-lg-5", "pl-lg-5": "pl-lg-5", "m-lg-auto": "m-lg-auto", "mt-lg-auto": "mt-lg-auto", "my-lg-auto": "my-lg-auto", "mr-lg-auto": "mr-lg-auto", "mx-lg-auto": "mx-lg-auto", "mb-lg-auto": "mb-lg-auto", "ml-lg-auto": "ml-lg-auto", "m-xl-0": "m-xl-0", "mt-xl-0": "mt-xl-0", "my-xl-0": "my-xl-0", "mr-xl-0": "mr-xl-0", "mx-xl-0": "mx-xl-0", "mb-xl-0": "mb-xl-0", "ml-xl-0": "ml-xl-0", "m-xl-1": "m-xl-1", "mt-xl-1": "mt-xl-1", "my-xl-1": "my-xl-1", "mr-xl-1": "mr-xl-1", "mx-xl-1": "mx-xl-1", "mb-xl-1": "mb-xl-1", "ml-xl-1": "ml-xl-1", "m-xl-2": "m-xl-2", "mt-xl-2": "mt-xl-2", "my-xl-2": "my-xl-2", "mr-xl-2": "mr-xl-2", "mx-xl-2": "mx-xl-2", "mb-xl-2": "mb-xl-2", "ml-xl-2": "ml-xl-2", "m-xl-3": "m-xl-3", "mt-xl-3": "mt-xl-3", "my-xl-3": "my-xl-3", "mr-xl-3": "mr-xl-3", "mx-xl-3": "mx-xl-3", "mb-xl-3": "mb-xl-3", "ml-xl-3": "ml-xl-3", "m-xl-4": "m-xl-4", "mt-xl-4": "mt-xl-4", "my-xl-4": "my-xl-4", "mr-xl-4": "mr-xl-4", "mx-xl-4": "mx-xl-4", "mb-xl-4": "mb-xl-4", "ml-xl-4": "ml-xl-4", "m-xl-5": "m-xl-5", "mt-xl-5": "mt-xl-5", "my-xl-5": "my-xl-5", "mr-xl-5": "mr-xl-5", "mx-xl-5": "mx-xl-5", "mb-xl-5": "mb-xl-5", "ml-xl-5": "ml-xl-5", "p-xl-0": "p-xl-0", "pt-xl-0": "pt-xl-0", "py-xl-0": "py-xl-0", "pr-xl-0": "pr-xl-0", "px-xl-0": "px-xl-0", "pb-xl-0": "pb-xl-0", "pl-xl-0": "pl-xl-0", "p-xl-1": "p-xl-1", "pt-xl-1": "pt-xl-1", "py-xl-1": "py-xl-1", "pr-xl-1": "pr-xl-1", "px-xl-1": "px-xl-1", "pb-xl-1": "pb-xl-1", "pl-xl-1": "pl-xl-1", "p-xl-2": "p-xl-2", "pt-xl-2": "pt-xl-2", "py-xl-2": "py-xl-2", "pr-xl-2": "pr-xl-2", "px-xl-2": "px-xl-2", "pb-xl-2": "pb-xl-2", "pl-xl-2": "pl-xl-2", "p-xl-3": "p-xl-3", "pt-xl-3": "pt-xl-3", "py-xl-3": "py-xl-3", "pr-xl-3": "pr-xl-3", "px-xl-3": "px-xl-3", "pb-xl-3": "pb-xl-3", "pl-xl-3": "pl-xl-3", "p-xl-4": "p-xl-4", "pt-xl-4": "pt-xl-4", "py-xl-4": "py-xl-4", "pr-xl-4": "pr-xl-4", "px-xl-4": "px-xl-4", "pb-xl-4": "pb-xl-4", "pl-xl-4": "pl-xl-4", "p-xl-5": "p-xl-5", "pt-xl-5": "pt-xl-5", "py-xl-5": "py-xl-5", "pr-xl-5": "pr-xl-5", "px-xl-5": "px-xl-5", "pb-xl-5": "pb-xl-5", "pl-xl-5": "pl-xl-5", "m-xl-auto": "m-xl-auto", "mt-xl-auto": "mt-xl-auto", "my-xl-auto": "my-xl-auto", "mr-xl-auto": "mr-xl-auto", "mx-xl-auto": "mx-xl-auto", "mb-xl-auto": "mb-xl-auto", "ml-xl-auto": "ml-xl-auto", "sr-only": "sr-only", "sr-only-focusable": "sr-only-focusable", "copy-button": "copy-button"
        }
    }
    , 459:function(e, t) {
        e.exports=function() {
            var e=document.getSelection();
            if(!e.rangeCount)return function() {}
            ;
            for(var t=document.activeElement, l=[], m=0;
            m<e.rangeCount;
            m++)l.push(e.getRangeAt(m));
            switch(t.tagName.toUpperCase()) {
                case"INPUT": case"TEXTAREA": t.blur();
                break;
                default: t=null
            }
            return e.removeAllRanges(), function() {
                "Caret"===e.type&&e.removeAllRanges(), e.rangeCount||l.forEach(function(t) {
                    e.addRange(t)
                }
                ), t&&t.focus()
            }
        }
    }
    , 460:function(e, t, l) {
        "use strict";
        var m=l(459), s="Copy to clipboard: #{key}, Enter";
        e.exports=function(e, t) {
            var l, a, p, r, d, o, u=!1;
            t||(t= {}
            ), l=t.debug||!1;
            try {
                if(p=m(), r=document.createRange(), d=document.getSelection(), (o=document.createElement("span")).textContent=e, o.style.all="unset", o.style.position="fixed", o.style.top=0, o.style.clip="rect(0, 0, 0, 0)", o.style.whiteSpace="pre", o.style.webkitUserSelect="text", o.style.MozUserSelect="text", o.style.msUserSelect="text", o.style.userSelect="text", document.body.appendChild(o), r.selectNode(o), d.addRange(r), !document.execCommand("copy"))throw new Error("copy command was unsuccessful");
                u=!0
            }
            catch(m) {
                l&&console.error("unable to copy using execCommand: ", m), l&&console.warn("trying IE specific stuff");
                try {
                    window.clipboardData.setData("text", e), u=!0
                }
                catch(m) {
                    l&&console.error("unable to copy using clipboardData: ", m), l&&console.error("falling back to prompt"), a=function(e) {
                        var t=(/mac os x/i.test(navigator.userAgent)?"⌘": "Ctrl")+"+C";
                        return e.replace(/#{\s*key\s*}/g, t)
                    }
                    ("message"in t?t.message:s), window.prompt(a, e)
                }
            }
            finally {
                d&&("function"==typeof d.removeRange?d.removeRange(r): d.removeAllRanges()), o&&document.body.removeChild(o), p()
            }
            return u
        }
    }
    , 461:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=function() {
            function e(e, t) {
                for(var l=0;
                l<t.length;
                l++) {
                    var m=t[l];
                    m.enumerable=m.enumerable||!1, m.configurable=!0, "value"in m&&(m.writable=!0), Object.defineProperty(e, m.key, m)
                }
            }
            return function(t, l, m) {
                return l&&e(t.prototype, l), m&&e(t, m), t
            }
        }
        (), s=o(l(2)), a=o(l(1)), p=l(19), r=o(l(460)), d=o(l(458));
        function o(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var u=function(e) {
            function t(e) {
                !function(e, t) {
                    if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")
                }
                (this, t);
                var l=function(e, t) {
                    if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return!t||"object"!=typeof t&&"function"!=typeof t?e: t
                }
                (this, (t.__proto__||Object.getPrototypeOf(t)).call(this, e));
                return l.state= {
                    wasClicked: !1
                }
                , l.onClick=l.onClick.bind(l), l.onBlur=l.onBlur.bind(l), l.setButtonRef=l.setButtonRef.bind(l), l
            }
            return function(e, t) {
                if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);
                e.prototype=Object.create(t&&t.prototype, {
                    constructor: {
                        value: e, enumerable: !1, writable: !0, configurable: !0
                    }
                }
                ), t&&(Object.setPrototypeOf?Object.setPrototypeOf(e, t):e.__proto__=t)
            }
            (t, s.default.Component), m(t, [ {
                key:"onClick", value:function() {
                    var url = window.location.href;
                    var arr = url.split("/");
                    var link = arr[0] + "//" + this.props.textToCopy; 
                    this.setState( {
                        wasClicked: !0
                    }
                    ), (0, r.default)(""+link /*this.props.textToCopy*/), this.buttonRef.focus(), this.props.onCopyButtonClick(!0)
                }
            }
            , {
                key:"onBlur", value:function() {
                    this.setState( {
                        wasClicked: !1
                    }
                    ), this.props.onCopyButtonClick(!1)
                }
            }
            , {
                key:"setButtonRef", value:function(e) {
                    this.buttonRef=e
                }
            }
            , {
                key:"render", value:function() {
                    var e=this.state.wasClicked?this.props.onCopyLabel: this.props.label;
                    return s.default.createElement(p.Button, {
                        className:[].concat(function(e) {
                            if(Array.isArray(e)) {
                                for(var t=0, l=Array(e.length);
                                t<e.length;
                                t++)l[t]=e[t];
                                return l
                            }
                            return Array.from(e)
                        }
                        (this.props.className), [d.default["copy-button"], "btn-outline-primary"]), "aria-label":this.props.ariaLabel, label:e, inputRef:this.setButtonRef, onClick:this.onClick, onBlur:this.onBlur, onMouseUp:this.onMouseUp, onMouseDown:this.onMouseDown, "data-identifier":this.props["data-identifier"]
                    }
                    )
                }
            }
            ]), t
        }
        ();
        t.default=u, u.propTypes= {
            ariaLabel: a.default.string, label: a.default.oneOfType([a.default.string, a.default.element]).isRequired, onCopyLabel: a.default.oneOfType([a.default.string, a.default.element]).isRequired, textToCopy: a.default.string.isRequired, onCopyButtonClick: a.default.func, className: a.default.arrayOf(a.default.string), "data-identifier": a.default.string
        }
        , u.defaultProps= {
            onCopyButtonClick:function() {}
            , ariaLabel: "Copy", className: [], "data-identifier": void 0
        }
    }
    , 462:function(e, t) {
        e.exports= {
            root: "root", SFE: "SFE", "m-0": "m-0", "mt-0": "mt-0", "my-0": "my-0", "mr-0": "mr-0", "mx-0": "mx-0", "mb-0": "mb-0", "ml-0": "ml-0", "m-1": "m-1", "mt-1": "mt-1", "my-1": "my-1", "mr-1": "mr-1", "mx-1": "mx-1", "mb-1": "mb-1", "ml-1": "ml-1", "m-2": "m-2", "mt-2": "mt-2", "my-2": "my-2", "mr-2": "mr-2", "mx-2": "mx-2", "mb-2": "mb-2", "ml-2": "ml-2", "m-3": "m-3", "mt-3": "mt-3", "my-3": "my-3", "mr-3": "mr-3", "mx-3": "mx-3", "mb-3": "mb-3", "ml-3": "ml-3", "m-4": "m-4", "mt-4": "mt-4", "my-4": "my-4", "mr-4": "mr-4", "mx-4": "mx-4", "mb-4": "mb-4", "ml-4": "ml-4", "m-5": "m-5", "mt-5": "mt-5", "my-5": "my-5", "mr-5": "mr-5", "mx-5": "mx-5", "mb-5": "mb-5", "ml-5": "ml-5", "p-0": "p-0", "pt-0": "pt-0", "py-0": "py-0", "pr-0": "pr-0", "px-0": "px-0", "pb-0": "pb-0", "pl-0": "pl-0", "p-1": "p-1", "pt-1": "pt-1", "py-1": "py-1", "pr-1": "pr-1", "px-1": "px-1", "pb-1": "pb-1", "pl-1": "pl-1", "p-2": "p-2", "pt-2": "pt-2", "py-2": "py-2", "pr-2": "pr-2", "px-2": "px-2", "pb-2": "pb-2", "pl-2": "pl-2", "p-3": "p-3", "no-image-preview": "no-image-preview", "pt-3": "pt-3", "py-3": "py-3", "pr-3": "pr-3", "px-3": "px-3", "pb-3": "pb-3", "pl-3": "pl-3", "p-4": "p-4", "pt-4": "pt-4", "py-4": "py-4", "pr-4": "pr-4", "px-4": "px-4", "pb-4": "pb-4", "pl-4": "pl-4", "p-5": "p-5", "pt-5": "pt-5", "py-5": "py-5", "pr-5": "pr-5", "px-5": "px-5", "pb-5": "pb-5", "pl-5": "pl-5", "m-auto": "m-auto", "mt-auto": "mt-auto", "my-auto": "my-auto", "mr-auto": "mr-auto", "mx-auto": "mx-auto", "mb-auto": "mb-auto", "ml-auto": "ml-auto", "m-sm-0": "m-sm-0", "mt-sm-0": "mt-sm-0", "my-sm-0": "my-sm-0", "mr-sm-0": "mr-sm-0", "mx-sm-0": "mx-sm-0", "mb-sm-0": "mb-sm-0", "ml-sm-0": "ml-sm-0", "m-sm-1": "m-sm-1", "mt-sm-1": "mt-sm-1", "my-sm-1": "my-sm-1", "mr-sm-1": "mr-sm-1", "mx-sm-1": "mx-sm-1", "mb-sm-1": "mb-sm-1", "ml-sm-1": "ml-sm-1", "m-sm-2": "m-sm-2", "mt-sm-2": "mt-sm-2", "my-sm-2": "my-sm-2", "mr-sm-2": "mr-sm-2", "mx-sm-2": "mx-sm-2", "mb-sm-2": "mb-sm-2", "ml-sm-2": "ml-sm-2", "m-sm-3": "m-sm-3", "mt-sm-3": "mt-sm-3", "my-sm-3": "my-sm-3", "mr-sm-3": "mr-sm-3", "mx-sm-3": "mx-sm-3", "mb-sm-3": "mb-sm-3", "ml-sm-3": "ml-sm-3", "m-sm-4": "m-sm-4", "mt-sm-4": "mt-sm-4", "my-sm-4": "my-sm-4", "mr-sm-4": "mr-sm-4", "mx-sm-4": "mx-sm-4", "mb-sm-4": "mb-sm-4", "ml-sm-4": "ml-sm-4", "m-sm-5": "m-sm-5", "mt-sm-5": "mt-sm-5", "my-sm-5": "my-sm-5", "mr-sm-5": "mr-sm-5", "mx-sm-5": "mx-sm-5", "mb-sm-5": "mb-sm-5", "ml-sm-5": "ml-sm-5", "p-sm-0": "p-sm-0", "pt-sm-0": "pt-sm-0", "py-sm-0": "py-sm-0", "pr-sm-0": "pr-sm-0", "px-sm-0": "px-sm-0", "pb-sm-0": "pb-sm-0", "pl-sm-0": "pl-sm-0", "p-sm-1": "p-sm-1", "pt-sm-1": "pt-sm-1", "py-sm-1": "py-sm-1", "pr-sm-1": "pr-sm-1", "px-sm-1": "px-sm-1", "pb-sm-1": "pb-sm-1", "pl-sm-1": "pl-sm-1", "p-sm-2": "p-sm-2", "pt-sm-2": "pt-sm-2", "py-sm-2": "py-sm-2", "pr-sm-2": "pr-sm-2", "px-sm-2": "px-sm-2", "pb-sm-2": "pb-sm-2", "pl-sm-2": "pl-sm-2", "p-sm-3": "p-sm-3", "pt-sm-3": "pt-sm-3", "py-sm-3": "py-sm-3", "pr-sm-3": "pr-sm-3", "px-sm-3": "px-sm-3", "pb-sm-3": "pb-sm-3", "pl-sm-3": "pl-sm-3", "p-sm-4": "p-sm-4", "pt-sm-4": "pt-sm-4", "py-sm-4": "py-sm-4", "pr-sm-4": "pr-sm-4", "px-sm-4": "px-sm-4", "pb-sm-4": "pb-sm-4", "pl-sm-4": "pl-sm-4", "p-sm-5": "p-sm-5", "pt-sm-5": "pt-sm-5", "py-sm-5": "py-sm-5", "pr-sm-5": "pr-sm-5", "px-sm-5": "px-sm-5", "pb-sm-5": "pb-sm-5", "pl-sm-5": "pl-sm-5", "m-sm-auto": "m-sm-auto", "mt-sm-auto": "mt-sm-auto", "my-sm-auto": "my-sm-auto", "mr-sm-auto": "mr-sm-auto", "mx-sm-auto": "mx-sm-auto", "mb-sm-auto": "mb-sm-auto", "ml-sm-auto": "ml-sm-auto", "m-md-0": "m-md-0", "mt-md-0": "mt-md-0", "my-md-0": "my-md-0", "mr-md-0": "mr-md-0", "mx-md-0": "mx-md-0", "mb-md-0": "mb-md-0", "ml-md-0": "ml-md-0", "m-md-1": "m-md-1", "mt-md-1": "mt-md-1", "my-md-1": "my-md-1", "mr-md-1": "mr-md-1", "mx-md-1": "mx-md-1", "mb-md-1": "mb-md-1", "ml-md-1": "ml-md-1", "m-md-2": "m-md-2", "mt-md-2": "mt-md-2", "my-md-2": "my-md-2", "mr-md-2": "mr-md-2", "mx-md-2": "mx-md-2", "mb-md-2": "mb-md-2", "ml-md-2": "ml-md-2", "m-md-3": "m-md-3", "mt-md-3": "mt-md-3", "my-md-3": "my-md-3", "mr-md-3": "mr-md-3", "mx-md-3": "mx-md-3", "mb-md-3": "mb-md-3", "ml-md-3": "ml-md-3", "m-md-4": "m-md-4", "mt-md-4": "mt-md-4", "my-md-4": "my-md-4", "mr-md-4": "mr-md-4", "mx-md-4": "mx-md-4", "mb-md-4": "mb-md-4", "ml-md-4": "ml-md-4", "m-md-5": "m-md-5", "mt-md-5": "mt-md-5", "my-md-5": "my-md-5", "mr-md-5": "mr-md-5", "mx-md-5": "mx-md-5", "mb-md-5": "mb-md-5", "ml-md-5": "ml-md-5", "p-md-0": "p-md-0", "pt-md-0": "pt-md-0", "py-md-0": "py-md-0", "pr-md-0": "pr-md-0", "px-md-0": "px-md-0", "pb-md-0": "pb-md-0", "pl-md-0": "pl-md-0", "p-md-1": "p-md-1", "pt-md-1": "pt-md-1", "py-md-1": "py-md-1", "pr-md-1": "pr-md-1", "px-md-1": "px-md-1", "pb-md-1": "pb-md-1", "pl-md-1": "pl-md-1", "p-md-2": "p-md-2", "pt-md-2": "pt-md-2", "py-md-2": "py-md-2", "pr-md-2": "pr-md-2", "px-md-2": "px-md-2", "pb-md-2": "pb-md-2", "pl-md-2": "pl-md-2", "p-md-3": "p-md-3", "pt-md-3": "pt-md-3", "py-md-3": "py-md-3", "pr-md-3": "pr-md-3", "px-md-3": "px-md-3", "pb-md-3": "pb-md-3", "pl-md-3": "pl-md-3", "p-md-4": "p-md-4", "pt-md-4": "pt-md-4", "py-md-4": "py-md-4", "pr-md-4": "pr-md-4", "px-md-4": "px-md-4", "pb-md-4": "pb-md-4", "pl-md-4": "pl-md-4", "p-md-5": "p-md-5", "pt-md-5": "pt-md-5", "py-md-5": "py-md-5", "pr-md-5": "pr-md-5", "px-md-5": "px-md-5", "pb-md-5": "pb-md-5", "pl-md-5": "pl-md-5", "m-md-auto": "m-md-auto", "mt-md-auto": "mt-md-auto", "my-md-auto": "my-md-auto", "mr-md-auto": "mr-md-auto", "mx-md-auto": "mx-md-auto", "mb-md-auto": "mb-md-auto", "ml-md-auto": "ml-md-auto", "m-lg-0": "m-lg-0", "mt-lg-0": "mt-lg-0", "my-lg-0": "my-lg-0", "mr-lg-0": "mr-lg-0", "mx-lg-0": "mx-lg-0", "mb-lg-0": "mb-lg-0", "ml-lg-0": "ml-lg-0", "m-lg-1": "m-lg-1", "mt-lg-1": "mt-lg-1", "my-lg-1": "my-lg-1", "mr-lg-1": "mr-lg-1", "mx-lg-1": "mx-lg-1", "mb-lg-1": "mb-lg-1", "ml-lg-1": "ml-lg-1", "m-lg-2": "m-lg-2", "mt-lg-2": "mt-lg-2", "my-lg-2": "my-lg-2", "mr-lg-2": "mr-lg-2", "mx-lg-2": "mx-lg-2", "mb-lg-2": "mb-lg-2", "ml-lg-2": "ml-lg-2", "m-lg-3": "m-lg-3", "mt-lg-3": "mt-lg-3", "my-lg-3": "my-lg-3", "mr-lg-3": "mr-lg-3", "mx-lg-3": "mx-lg-3", "mb-lg-3": "mb-lg-3", "ml-lg-3": "ml-lg-3", "m-lg-4": "m-lg-4", "mt-lg-4": "mt-lg-4", "my-lg-4": "my-lg-4", "mr-lg-4": "mr-lg-4", "mx-lg-4": "mx-lg-4", "mb-lg-4": "mb-lg-4", "ml-lg-4": "ml-lg-4", "m-lg-5": "m-lg-5", "mt-lg-5": "mt-lg-5", "my-lg-5": "my-lg-5", "mr-lg-5": "mr-lg-5", "mx-lg-5": "mx-lg-5", "mb-lg-5": "mb-lg-5", "ml-lg-5": "ml-lg-5", "p-lg-0": "p-lg-0", "pt-lg-0": "pt-lg-0", "py-lg-0": "py-lg-0", "pr-lg-0": "pr-lg-0", "px-lg-0": "px-lg-0", "pb-lg-0": "pb-lg-0", "pl-lg-0": "pl-lg-0", "p-lg-1": "p-lg-1", "pt-lg-1": "pt-lg-1", "py-lg-1": "py-lg-1", "pr-lg-1": "pr-lg-1", "px-lg-1": "px-lg-1", "pb-lg-1": "pb-lg-1", "pl-lg-1": "pl-lg-1", "p-lg-2": "p-lg-2", "pt-lg-2": "pt-lg-2", "py-lg-2": "py-lg-2", "pr-lg-2": "pr-lg-2", "px-lg-2": "px-lg-2", "pb-lg-2": "pb-lg-2", "pl-lg-2": "pl-lg-2", "p-lg-3": "p-lg-3", "pt-lg-3": "pt-lg-3", "py-lg-3": "py-lg-3", "pr-lg-3": "pr-lg-3", "px-lg-3": "px-lg-3", "pb-lg-3": "pb-lg-3", "pl-lg-3": "pl-lg-3", "p-lg-4": "p-lg-4", "pt-lg-4": "pt-lg-4", "py-lg-4": "py-lg-4", "pr-lg-4": "pr-lg-4", "px-lg-4": "px-lg-4", "pb-lg-4": "pb-lg-4", "pl-lg-4": "pl-lg-4", "p-lg-5": "p-lg-5", "pt-lg-5": "pt-lg-5", "py-lg-5": "py-lg-5", "pr-lg-5": "pr-lg-5", "px-lg-5": "px-lg-5", "pb-lg-5": "pb-lg-5", "pl-lg-5": "pl-lg-5", "m-lg-auto": "m-lg-auto", "mt-lg-auto": "mt-lg-auto", "my-lg-auto": "my-lg-auto", "mr-lg-auto": "mr-lg-auto", "mx-lg-auto": "mx-lg-auto", "mb-lg-auto": "mb-lg-auto", "ml-lg-auto": "ml-lg-auto", "m-xl-0": "m-xl-0", "mt-xl-0": "mt-xl-0", "my-xl-0": "my-xl-0", "mr-xl-0": "mr-xl-0", "mx-xl-0": "mx-xl-0", "mb-xl-0": "mb-xl-0", "ml-xl-0": "ml-xl-0", "m-xl-1": "m-xl-1", "mt-xl-1": "mt-xl-1", "my-xl-1": "my-xl-1", "mr-xl-1": "mr-xl-1", "mx-xl-1": "mx-xl-1", "mb-xl-1": "mb-xl-1", "ml-xl-1": "ml-xl-1", "m-xl-2": "m-xl-2", "mt-xl-2": "mt-xl-2", "my-xl-2": "my-xl-2", "mr-xl-2": "mr-xl-2", "mx-xl-2": "mx-xl-2", "mb-xl-2": "mb-xl-2", "ml-xl-2": "ml-xl-2", "m-xl-3": "m-xl-3", "mt-xl-3": "mt-xl-3", "my-xl-3": "my-xl-3", "mr-xl-3": "mr-xl-3", "mx-xl-3": "mx-xl-3", "mb-xl-3": "mb-xl-3", "ml-xl-3": "ml-xl-3", "m-xl-4": "m-xl-4", "mt-xl-4": "mt-xl-4", "my-xl-4": "my-xl-4", "mr-xl-4": "mr-xl-4", "mx-xl-4": "mx-xl-4", "mb-xl-4": "mb-xl-4", "ml-xl-4": "ml-xl-4", "m-xl-5": "m-xl-5", "mt-xl-5": "mt-xl-5", "my-xl-5": "my-xl-5", "mr-xl-5": "mr-xl-5", "mx-xl-5": "mx-xl-5", "mb-xl-5": "mb-xl-5", "ml-xl-5": "ml-xl-5", "p-xl-0": "p-xl-0", "pt-xl-0": "pt-xl-0", "py-xl-0": "py-xl-0", "pr-xl-0": "pr-xl-0", "px-xl-0": "px-xl-0", "pb-xl-0": "pb-xl-0", "pl-xl-0": "pl-xl-0", "p-xl-1": "p-xl-1", "pt-xl-1": "pt-xl-1", "py-xl-1": "py-xl-1", "pr-xl-1": "pr-xl-1", "px-xl-1": "px-xl-1", "pb-xl-1": "pb-xl-1", "pl-xl-1": "pl-xl-1", "p-xl-2": "p-xl-2", "pt-xl-2": "pt-xl-2", "py-xl-2": "py-xl-2", "pr-xl-2": "pr-xl-2", "px-xl-2": "px-xl-2", "pb-xl-2": "pb-xl-2", "pl-xl-2": "pl-xl-2", "p-xl-3": "p-xl-3", "pt-xl-3": "pt-xl-3", "py-xl-3": "py-xl-3", "pr-xl-3": "pr-xl-3", "px-xl-3": "px-xl-3", "pb-xl-3": "pb-xl-3", "pl-xl-3": "pl-xl-3", "p-xl-4": "p-xl-4", "pt-xl-4": "pt-xl-4", "py-xl-4": "py-xl-4", "pr-xl-4": "pr-xl-4", "px-xl-4": "px-xl-4", "pb-xl-4": "pb-xl-4", "pl-xl-4": "pl-xl-4", "p-xl-5": "p-xl-5", "pt-xl-5": "pt-xl-5", "py-xl-5": "py-xl-5", "pr-xl-5": "pr-xl-5", "px-xl-5": "px-xl-5", "pb-xl-5": "pb-xl-5", "pl-xl-5": "pl-xl-5", "m-xl-auto": "m-xl-auto", "mt-xl-auto": "mt-xl-auto", "my-xl-auto": "my-xl-auto", "mr-xl-auto": "mr-xl-auto", "mx-xl-auto": "mx-xl-auto", "mb-xl-auto": "mb-xl-auto", "ml-xl-auto": "ml-xl-auto", "sr-only": "sr-only", "sr-only-focusable": "sr-only-focusable", "studio-copy-button": "studio-copy-button", "copy-icon": "copy-icon", table: "table", "wrap-text": "wrap-text"
        }
    }
    , 463:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m=Object.assign||function(e) {
            for(var t=1;
            t<arguments.length;
            t++) {
                var l=arguments[t];
                for(var m in l)Object.prototype.hasOwnProperty.call(l, m)&&(e[m]=l[m])
            }
            return e
        }
        , s=function() {
            function e(e, t) {
                for(var l=0;
                l<t.length;
                l++) {
                    var m=t[l];
                    m.enumerable=m.enumerable||!1, m.configurable=!0, "value"in m&&(m.writable=!0), Object.defineProperty(e, m.key, m)
                }
            }
            return function(t, l, m) {
                return l&&e(t.prototype, l), m&&e(t, m), t
            }
        }
        (), a=c(l(2)), p=c(l(1)), r=l(19), d=c(l(35)), o=c(l(69)), u=c(l(462)), n=l(160), i=c(l(461)), x=c(l(15)), g=c(l(457));
        function c(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var f=!!window.MSInputMethodContext&&!!document.documentMode, b=function(e) {
            function t(e) {
                !function(e, t) {
                    if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")
                }
                (this, t);
                var l=function(e, t) {
                    if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return!t||"object"!=typeof t&&"function"!=typeof t?e: t
                }
                (this, (t.__proto__||Object.getPrototypeOf(t)).call(this, e));
                return l.onLockClick=function(e) {
                    var t=e.currentTarget.getAttribute("data-asset-id"), m=l.props.assetsList.find(function(e) {
                        return e.id===t
                    }
                    );
                    l.props.toggleLockAsset(m, l.props.courseDetails)
                }
                , l.state= {
                    copyButtonIsClicked:!1, elementToFocusOnModalClose: {}
                    , modalOpen: !1
                }
                , l.columns= {
                    image_preview: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTablePreviewLabel
                        }
                        ), key:"image_preview", columnSortable:!1, hideHeader:!0, width:"col-2"
                    }
                    , display_name: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableNameLabel
                        }
                        ), key:"display_name", columnSortable:!0, width:"col-3"
                    }
                    , content_type: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableTypeLable
                        }
                        ), key:"content_type", columnSortable:!0, width:"col-2"
                    }
                    , date_added: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableDateLabel
                        }
                        ), key:"date_added", columnSortable:!0, width:"col-2"
                    }
                    , urls: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableCopyLabel
                        }
                        ), key:"urls", columnSortable:!1, width:"col"
                    }
                    , delete_asset: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableDeleteLabel
                        }
                        ), key:"delete_asset", columnSortable:!1, hideHeader:!0, width:"col"
                    }
                    , lock_asset: {
                        label:a.default.createElement(x.default, {
                            message: g.default.assetsTableLockLabel
                        }
                        ), key:"lock_asset", columnSortable:!1, hideHeader:!0, width:"col"
                    }
                }
                , l.trashcanRefs= {}
                , l.addSupplementalTableElements=l.addSupplementalTableElements.bind(l), l.closeModal=l.closeModal.bind(l), l.deleteAsset=l.deleteAsset.bind(l), l.getAssetDeleteButtonRef=l.getAssetDeleteButtonRef.bind(l), l.onCopyButtonClick=l.onCopyButtonClick.bind(l), l.onDeleteClick=l.onDeleteClick.bind(l), l
            }
            return function(e, t) {
                if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);
                e.prototype=Object.create(t&&t.prototype, {
                    constructor: {
                        value: e, enumerable: !1, writable: !0, configurable: !0
                    }
                }
                ), t&&(Object.setPrototypeOf?Object.setPrototypeOf(e, t):e.__proto__=t)
            }
            (t, a.default.Component), s(t, [ {
                key:"onSortClick", value:function(e) {
                    var t=this.props.assetsSortMetadata.sort, l=this.props.assetsSortMetadata.direction, m="desc";
                    t===e&&(m="desc"===l?"asc": "desc"), this.props.updateSort(e, m, this.props.courseDetails)
                }
            }
            , {
                key:"onDeleteClick", value:function(e) {
                    var t=this.props.assetsList[e];
                    this.props.stageAssetDeletion(t, e), this.setState( {
                        elementToFocusOnModalClose: this.trashcanRefs[t.id], modalOpen: !0
                    }
                    )
                }
            }
            , {
                key:"onCopyButtonClick", value:function(e) {
                    this.setState( {
                        copyButtonIsClicked: e
                    }
                    )
                }
            }
            , {
                key:"getImageThumbnail", value:function(e) {
                    var t=this.props.courseDetails.base_url||"";
                    return e?a.default.createElement(x.default, {
                        message: g.default.assetsTableNoDescription
                    }
                    , function(l) {
                        return a.default.createElement("img", {
                            src: ""+t+e, alt: l, "data-identifier": "asset-image-thumbnail"
                        }
                        )
                    }
                    ):a.default.createElement(x.default, {
                        message: g.default.assetsTableNoPreview
                    }
                    , function(e) {
                        return a.default.createElement("div", {
                            className: u.default["no-image-preview"], "data-identifier": "asset-image-thumbnail"
                        }
                        , e)
                    }
                    )
                }
            }
            , {
                key:"getLockButton", value:function(e) {
                    var t=this, l=[o.default.fa, "btn-outline-primary"], m=void 0;
                    return e.locked?(m=g.default.assetsTableUnlockedObject, l.push(o.default["fa-lock"])):(m=g.default.assetsTableUnlockedObject, l.push(o.default["fa-unlock"])), a.default.createElement(x.default, {
                        message:m, values: {
                            object: e.display_name
                        }
                    }
                    , function(m) {
                        return a.default.createElement(r.Button, {
                            className: l, label: "", "data-asset-id": e.id, "aria-label": m, onClick: t.onLockClick, "data-identifier": "asset-lock-button"
                        }
                        )
                    }
                    )
                }
            }
            , {
                key:"getLoadingLockButton", value:function(e) {
                    var t=[o.default.fa, o.default["fa-spinner"], o.default["fa-spin"]];
                    return a.default.createElement(x.default, {
                        message:g.default.assetsTableUpdateLock, values: {
                            assetName: e.display_name
                        }
                    }
                    , function(e) {
                        return a.default.createElement(r.Button, {
                            className:["btn-outline-primary"], label:a.default.createElement("span", {
                                className: d.default.apply(void 0, t)
                            }
                            ), "aria-label":e, "data-identifier":"asset-locking-button"
                        }
                        )
                    }
                    )
                }
            }
            , {
                key:"getCopyUrlButtons", value:function(e, t, l) {
                    return a.default.createElement("span", null, t&&this.getCopyUrlButton(e, t, g.default.assetsTableStudioLink, [u.default["studio-copy-button"]]), l&&this.getCopyUrlButton(e, l, g.default.assetsTableWebLink))
                }
            }
            , {
                key:"getCopyUrlButton", value:function(e, t, l) {
                    var m=this, s=arguments.length>3&&void 0!==arguments[3]?arguments[3]:[], p=a.default.createElement("span", null, a.default.createElement("span", {
                        className: (0, d.default)(o.default.fa, o.default["fa-files-o"], u.default["copy-icon"]), "aria-hidden": !0
                    }
                    ), a.default.createElement(x.default, {
                        message: l
                    }
                    )), r=a.default.createElement(x.default, {
                        message: g.default.assetsTableCopiedStatus, "data-identifier": "asset-copy-"+l+"-url-button-copy-label"
                    }
                    );
                    return a.default.createElement(x.default, {
                        message:g.default.assetsTableDetailedCopyLink, values: {
                            displayName:e, label:a.default.createElement(x.default, {
                                message: l
                            }
                            )
                        }
                    }
                    , function(e) {
                        return a.default.createElement(i.default, {
                            label: p, className: s, textToCopy: t, onCopyButtonClick: m.onCopyButtonClick, ariaLabel: e, onCopyLabel: r, "data-identifier": "asset-copy-"+l.defaultMessage.toLowerCase()+"-url-button"
                        }
                        )
                    }
                    )
                }
            }
            , {
                key:"getAssetDeleteButtonRef", value:function(e, t) {
                    this.trashcanRefs[t.id]=e, this.props.deleteButtonRefs(e, t)
                }
            }
            , {
                key:"getTableCaption", value:function() {
                    return a.default.createElement("span", {
                        className: "sr-only"
                    }
                    , a.default.createElement(x.default, {
                        message: g.default.assetsTableCaption
                    }
                    ))
                }
            }
            , {
                key:"getTableColumns", value:function() {
                    var e=this, t=Object.keys(this.columns), l= {}
                    ;
                    return this.props.isImagePreviewEnabled||(t=t.filter(function(e) {
                        return"image_preview"!==e
                    }
                    ), l= {
                        content_type: {
                            width: "col-3"
                        }
                        , date_added: {
                            width: "col-3"
                        }
                    }
                    ), t.map(function(t) {
                        return m( {}
                        , e.columns[t], l[t], {
                            onSort:function() {
                                return e.onSortClick(t)
                            }
                        }
                        )
                    }
                    )
                }
            }
            , {
                key:"addSupplementalTableElements", value:function() {
                    var e=this;
                    return this.props.assetsList.map(function(t, l) {
                        var m=Object.assign( {}
                        , t), s=a.default.createElement(x.default, {
                            message:g.default.assetsTableDeleteObject, values: {
                                displayName: m.display_name
                            }
                        }
                        , function(t) {
                            return a.default.createElement(r.Button, {
                                key:m.id, className:[o.default.fa, o.default["fa-trash"], "btn-outline-primary"], label:"", "aria-label":t, onClick:function() {
                                    e.onDeleteClick(l)
                                }
                                , inputRef:function(t) {
                                    return e.getAssetDeleteButtonRef(t, m)
                                }
                                , "data-identifier":"asset-delete-button"
                            }
                            )
                        }
                        ), p=m.loadingFields&&m.loadingFields.includes(n.assetLoading.LOCK);
                        return m.delete_asset=s, m.lock_asset=p?e.getLoadingLockButton(m):e.getLockButton(m), m.image_preview=e.getImageThumbnail(m.thumbnail), m.urls=e.getCopyUrlButtons(m.display_name, m.portable_url, m.external_url), m.display_name=a.default.createElement("span", {
                            "data-identifier": "asset-file-name"
                        }
                        , m.display_name), m.content_type=a.default.createElement("span", {
                            "data-identifier": "asset-content-type"
                        }
                        , m.content_type), m.date_added=a.default.createElement("span", {
                            "data-identifier": "asset-date-added"
                        }
                        , m.date_added), m
                    }
                    )
                }
            }
            , {
                key:"closeModal", value:function() {
                    this.state.elementToFocusOnModalClose.focus(), this.props.unstageAssetDeletion(), this.setState( {
                        elementToFocusOnModalClose: {}
                        , modalOpen: !1
                    }
                    )
                }
            }
            , {
                key:"deleteAsset", value:function() {
                    this.props.deleteAsset(this.props.assetToDelete, this.props.courseDetails), this.setState( {
                        elementToFocusOnModalClose: {}
                        , modalOpen: !1
                    }
                    )
                }
            }
            , {
                key:"renderModal", value:function() {
                    return a.default.createElement("div", {
                        id: "modalWrapper"
                    }
                    , a.default.createElement(r.Modal, {
                        open:this.state.modalOpen, title:a.default.createElement(x.default, {
                            message:g.default.assetsTableDeleteObject, values: {
                                displayName: this.props.assetToDelete.display_name
                            }
                        }
                        ), body:this.renderModalBody(), closeText:a.default.createElement(x.default, {
                            message: g.default.assetsTableCancel
                        }
                        ), onClose:this.closeModal, buttons:[a.default.createElement(r.Button, {
                            label:a.default.createElement(x.default, {
                                message: g.default.assetsTablePermaDelete
                            }
                            ), buttonType:"primary", onClick:this.deleteAsset, "data-identifier":"asset-confirm-delete-button"
                        }
                        )], variant: {
                            status: r.Variant.status.WARNING
                        }
                        , parentSelector:"#modalWrapper"
                    }
                    ))
                }
            }
            , {
                key:"renderModalBody", value:function() {
                    var e=this, t=a.default.createElement(x.default, {
                        tagName: "a", message: g.default.assetsTableLearnMore
                    }
                    , function(t) {
                        return a.default.createElement("a", {
                            target: "_blank", rel: "noopener noreferrer", href: e.props.courseFilesDocs
                        }
                        , t)
                    }
                    );
                    return a.default.createElement(a.default.Fragment, null, a.default.createElement(x.default, {
                        message:g.default.assetsTableDeleteWarning, tagName:"p", values: {
                            displayName:a.default.createElement("b", {
                                className: u.default["wrap-text"]
                            }
                            , this.props.assetToDelete.display_name)
                        }
                    }
                    ), a.default.createElement(x.default, {
                        message:g.default.assetsTableDeleteConsequences, tagName:"p", values: {
                            link: t
                        }
                    }
                    ))
                }
            }
            , {
                key:"render", value:function() {
                    let options = {
                        caption: this.getTableCaption(), 
                        className: ["table-responsive"], 
                        columns: this.getTableColumns(), 
                        data: this.addSupplementalTableElements(this.props.assetsList), 
                        tableSortable: !0, defaultSortedColumn: "date_added", 
                        defaultSortDirection: "desc", 
                        hasFixedColumnWidths: !f
                    }

                    let _datas = options.data;
                    _datas.forEach((val, key) => {

                        let __link = a.default.createElement(
                            "a", 
                            {
                                "href": options.data[key].url,
                                "target": "_blank",
                                'className': "assets-link"
                            }, 
                            options.data[key].display_name.props.children
                        )
                        options.data[key].display_name.props.children = __link;
                    });

                    return a.default.createElement(a.default.Fragment, null, a.default.createElement(r.Table, options), this.renderModal(), a.default.createElement("span", {
                        className: "sr", "aria-live": "assertive", id: "copy-status"
                    }
                    , this.state.copyButtonIsClicked?a.default.createElement(x.default, {
                        message: g.default.assetsTableCopiedStatus
                    }
                    ):""))
                }
            }
            ]), t
        }
        ();
        t.default=b, b.propTypes= {
            assetsList:p.default.arrayOf(p.default.object).isRequired, assetsSortMetadata:p.default.shape( {
                sort: p.default.string, direction: p.default.string
            }
            ).isRequired, courseDetails:p.default.shape( {
                lang: p.default.string, url_name: p.default.string, name: p.default.string, display_course_number: p.default.string, num: p.default.string, org: p.default.string, id: p.default.string, revision: p.default.string, base_url: p.default.string
            }
            ).isRequired, courseFilesDocs:p.default.string.isRequired, deleteAsset:p.default.func.isRequired, deleteButtonRefs:p.default.func, isImagePreviewEnabled:p.default.bool.isRequired, updateSort:p.default.func.isRequired, toggleLockAsset:p.default.func.isRequired, stageAssetDeletion:p.default.func.isRequired, unstageAssetDeletion:p.default.func.isRequired, assetToDelete:p.default.shape( {
                display_name: p.default.string, content_type: p.default.string, url: p.default.string, date_added: p.default.string, id: p.default.string, portable_url: p.default.string, thumbnail: p.default.string, external_url: p.default.string
            }
            ).isRequired
        }
        , b.defaultProps= {
            deleteButtonRefs:function() {}
        }
    }
    , 464:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(463), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        , r=l(24);
        var d=(0, s.connect)(function(e) {
            return {
                assetsList: e.assets, assetToDelete: e.metadata.deletion.assetToDelete, assetsSortMetadata: e.metadata.sort, assetsStatus: e.metadata.status, courseDetails: e.studioDetails.course, courseFilesDocs: e.studioDetails.help_tokens.files, upload: e.assets.upload, isImagePreviewEnabled: e.metadata.imagePreview.enabled
            }
        }
        , function(e) {
            return {
                clearAssetsStatus:function() {
                    return e((0, r.clearAssetsStatus)())
                }
                , deleteAsset:function(t, l) {
                    return e((0, r.deleteAsset)(t, l))
                }
                , stageAssetDeletion:function(t, l) {
                    return e((0, r.stageAssetDeletion)(t, l))
                }
                , toggleLockAsset:function(t, l) {
                    return e((0, r.toggleLockAsset)(t, l))
                }
                , updateSort:function(t, l, m) {
                    return e((0, r.sortUpdate)(t, l, m))
                }
                , unstageAssetDeletion:function() {
                    return e((0, r.unstageAssetDeletion)())
                }
            }
        }
        )(p.default);
        t.default=d
    }
    , 473:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        ), t.TABLE_CONTENTS_ID=void 0;
        var m=function() {
            function e(e, t) {
                for(var l=0;
                l<t.length;
                l++) {
                    var m=t[l];
                    m.enumerable=m.enumerable||!1, m.configurable=!0, "value"in m&&(m.writable=!0), Object.defineProperty(e, m.key, m)
                }
            }
            return function(t, l, m) {
                return l&&e(t.prototype, l), m&&e(t, m), t
            }
        }
        (), s=E(l(35)), a=E(l(2)), p=E(l(1)), r=l(165), d=l(70), o=E(l(163)), u=E(l(464)), n=E(l(456)), i=E(l(452)), x=E(l(159)), g=E(l(158)), c=E(l(439)), f=E(l(436)), b=E(l(433)), y=E(l(15)), h=E(l(430)), v=E(l(429));
        function E(e) {
            return e&&e.__esModule?e: {
                default: e
            }
        }
        var _=t.TABLE_CONTENTS_ID="table-contents", S=function(e) {
            function t(e) {
                !function(e, t) {
                    if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")
                }
                (this, t);
                var l=function(e, t) {
                    if(!e)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
                    return!t||"object"!=typeof t&&"function"!=typeof t?e: t
                }
                (this, (t.__proto__||Object.getPrototypeOf(t)).call(this, e));
                return l.onDeleteStatusAlertClose=function() {
                    (l.props.clearAssetDeletion(), l.props.assetsList.length>0)&&l.getNextFocusElementOnDelete().focus()
                }
                , l.getPage=function(e) {
                    switch(e) {
                        case r.pageTypes.NORMAL: return l.renderAssetsPage();
                        case r.pageTypes.NO_ASSETS: return l.renderNoAssetsPage();
                        case r.pageTypes.NO_RESULTS: return l.renderNoResultsPage();
                        case r.pageTypes.SKELETON: return l.renderSkeletonPage();
                        default: throw new Error("Unknown pageType "+e+".")
                    }
                }
                , l.renderAssetsDropZone=function() {
                    return a.default.createElement(o.default, {
                        maxFileSizeMB: l.props.uploadSettings.max_file_size_in_mbs
                    }
                    )
                }
                , l.renderAssetsFilters=function() {
                    return a.default.createElement(n.default, null)
                }
                , l.renderAssetsPage=function() {
                    return a.default.createElement(a.default.Fragment, null, a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-sm-3 cold-md-3 offset-2"
                    }
                    , a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-sm-9 col-md-9"
                    }
                    , a.default.createElement("div", {
                        "aria-hidden": !0
                    }
                    , a.default.createElement(f.default, null))), a.default.createElement("div", {
                        className: "col-md-4 text-right"
                    }
                    , (0, d.hasSearchOrFilterApplied)(l.props.filtersMetadata.assetTypes, l.props.searchMetadata.search)&&a.default.createElement(b.default, {
                        className: "p-3"
                    }
                    ))))), a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-sm-3 col-md-3"
                    }
                    , a.default.createElement("a", {
                        className: (0, s.default)("sr-only", "sr-only-focusable", v.default["skip-link"]), href: "#"+_
                    }
                    , a.default.createElement(y.default, {
                        message: h.default.assetsPageSkipLink
                    }
                    )), l.renderAssetsDropZone(), a.default.createElement("div", {
                        className: "page-header"
                    }
                    , a.default.createElement(i.default, null)), l.renderAssetsFilters()), a.default.createElement("div", {
                        className: "col-sm-9 col-md-9", id: _, tabIndex: "-1"
                    }
                    , a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement(u.default, {
                        deleteButtonRefs:function(e, t) {
                            l.deleteButtonRefs[t.id]=e
                        }
                    }
                    )))), a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-sm-9 col-md-9 offset-2"
                    }
                    , a.default.createElement(x.default, null))))
                }
                , l.renderNoAssetsBody=function() {
                    return a.default.createElement(y.default, {
                        message: h.default.assetsPageNoAssetsMessage, tagName: "h3"
                    }
                    )
                }
                , l.renderNoAssetsPage=function() {
                    return a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col"
                    }
                    , l.renderAssetsDropZone()), a.default.createElement("div", {
                        className: "col-sm-9 col-md-9"
                    }
                    , l.renderNoAssetsBody()))
                }
                , l.renderNoResultsBody=function() {
                    return a.default.createElement(a.default.Fragment, null, a.default.createElement(y.default, {
                        message: h.default.assetsPageNoResultsMessage, tagName: "h3"
                    }
                    ), a.default.createElement(b.default, null))
                }
                , l.renderNoResultsPage=function() {
                    return a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col"
                    }
                    , l.renderAssetsDropZone(), l.renderAssetsFilters()), a.default.createElement("div", {
                        className: "col-sm-9 col-md-9"
                    }
                    , l.renderNoResultsBody()))
                }
                , l.renderSkeletonPage=function() {
                    return a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-2"
                    }
                    , l.renderAssetsDropZone(), l.renderAssetsFilters()))
                }
                , l.state= {
                    pageType: r.pageTypes.SKELETON
                }
                , l.statusAlertRef=null, l.deleteButtonRefs= {}
                , l.getNextFocusElementOnDelete=l.getNextFocusElementOnDelete.bind(l), l.onDeleteStatusAlertClose=l.onDeleteStatusAlertClose.bind(l), l
            }
            return function(e, t) {
                if("function"!=typeof t&&null!==t)throw new TypeError("Super expression must either be null or a function, not "+typeof t);
                e.prototype=Object.create(t&&t.prototype, {
                    constructor: {
                        value: e, enumerable: !1, writable: !0, configurable: !0
                    }
                }
                ), t&&(Object.setPrototypeOf?Object.setPrototypeOf(e, t):e.__proto__=t)
            }
            (t, a.default.Component), m(t, [ {
                key:"componentDidMount", value:function() {
                    0===this.props.assetsList.length&&this.props.getAssets( {}
                    , this.props.courseDetails)
                }
            }
            , {
                key:"componentWillReceiveProps", value:function(e) {
                    this.setState(function(t) {
                        return {
                            pageType: (0, r.getPageType)(e, t.pageType)
                        }
                    }
                    )
                }
            }
            , {
                key:"getNextFocusElementOnDelete", value:function() {
                    var e=this.props, t=e.deletedAssetIndex, l=e.assetsList[t];
                    return this.deleteButtonRefs[l.id]
                }
            }
            , {
                key:"render", value:function() {
                    var e=this;
                    return a.default.createElement(a.default.Fragment, null, a.default.createElement("div", {
                        "aria-atomic": !0, "aria-live": "polite", "aria-relevant": "text", className: "sr-only"
                    }
                    , a.default.createElement(f.default, null)), a.default.createElement("div", {
                        className: "container"
                    }
                    , a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-12"
                    }
                    , a.default.createElement(c.default, {
                        statusAlertRef:function(t) {
                            e.statusAlertRef=t
                        }
                        , onDeleteStatusAlertClose:this.onDeleteStatusAlertClose, onClose:this.onStatusAlertClose
                    }
                    ))), (this.state.pageType===r.pageTypes.NORMAL||this.state.pageType===r.pageTypes.NO_RESULTS)&&a.default.createElement("div", {
                        className: "row"
                    }
                    , a.default.createElement("div", {
                        className: "col-12 p-0"
                    }
                    , a.default.createElement(g.default, null))), this.getPage(this.state.pageType)))
                }
            }
            ]), t
        }
        ();
        t.default=S, S.propTypes= {
            assetsList:p.default.arrayOf(p.default.object).isRequired, courseDetails:p.default.shape( {
                lang: p.default.string, url_name: p.default.string, name: p.default.string, display_course_number: p.default.string, num: p.default.string, org: p.default.string, id: p.default.string, revision: p.default.string
            }
            ).isRequired, deletedAssetIndex:p.default.oneOfType([p.default.number]), filtersMetadata:p.default.shape( {
                assetTypes: p.default.object
            }
            ).isRequired, searchMetadata:p.default.shape( {
                search: p.default.string
            }
            ).isRequired, getAssets:p.default.func.isRequired, status:p.default.shape( {
                type: p.default.string, response: p.default.object
            }
            ).isRequired, uploadSettings:p.default.shape( {
                max_file_size_in_mbs: p.default.number
            }
            ).isRequired, clearAssetDeletion:p.default.func.isRequired
        }
        , S.defaultProps= {
            deletedAssetIndex: null
        }
    }
    , 474:function(e, t, l) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }
        );
        var m, s=l(9), a=l(473), p=(m=a)&&m.__esModule?m: {
            default: m
        }
        , r=l(24);
        var d=(0, s.connect)(function(e) {
            return {
                assetsList: e.assets, assetsStatus: e.metadata.status, assetToDelete: e.metadata.deletion.assetToDelete, courseDetails: e.studioDetails.course, deletedAsset: e.metadata.deletion.deletedAsset, deletedAssetIndex: e.metadata.deletion.deletedAssetIndex, filtersMetadata: e.metadata.filters, uploadSettings: e.studioDetails.upload_settings, searchMetadata: e.metadata.search, status: e.metadata.status
            }
        }
        , function(e) {
            return {
                clearAssetDeletion:function() {
                    return e((0, r.clearAssetDeletion)())
                }
                , getAssets:function(t, l) {
                    return e((0, r.getAssets)(t, l))
                }
            }
        }
        )(p.default);
        t.default=d
    }
    , 502:function(e, t, l) {
        "use strict";
        (function(e) {
            var t=u(l(2)), m=u(l(60)), s=l(9), a=l(8);
            l(59);
            var p=u(l(474)), r=u(l(428)), d=u(l(68)), o=u(l(155));
            function u(e) {
                return e&&e.__esModule?e: {
                    default: e
                }
            }
            ("undefined"!=typeof window&&!window._babelPolyfill||void 0!==e&&!e._babelPolyfill)&&l(66);
            var n=(0, o.default)();
            m.default.render(t.default.createElement(function() {
                return t.default.createElement(a.IntlProvider, {
                    locale: n.locale, messages: n.messages
                }
                , t.default.createElement(s.Provider, {
                    store: d.default
                }
                , t.default.createElement("div", {
                    className: "SFE-wrapper"
                }
                , t.default.createElement(r.default, null), t.default.createElement(p.default, null))))
            }
            , null), document.getElementById("root"))
        }
        ).call(this, l(38))
    }
}

, [[502, 1, 0]]]);
//# sourceMappingURL=assets.min.js.map
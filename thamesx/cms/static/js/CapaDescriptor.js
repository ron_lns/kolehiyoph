define([
	'jquery', 
    '/static/studio/k_thamesx_cms/js/xml_to_json.js'
],function(jquery, X2JS){
	return function(){
		this.$ = jquery;
		this.initCount = 0;

	    this.init = function (){
	        this.xmlBox = $(".problem-editor .xml-box");
	        if(this.xmlBox.length){
	    		this.x2js = new X2JS();
			    this._json = {};
			    this.htmlString = '';
	    		this._type = '';
	    		this._choices = '';
		        this._xml = this.xmlBox.val();
		        this.convertXml2JSon();
		        this.renderEditor();
	        } else {
	        	var _self = this;
	        	this.initCount++;
	        	if(_self.initCount < 500) {
		        	setTimeout(function(){
		        		_self.init();
		        	},100);
	        	}
	        }
	    }

        this.convertXml2JSon = function() {
	        this._json = this.x2js.xml_str2json(this._xml);
	    }

		this.renderEditor = function(){
		    $(".problem-editor .CodeMirror").addClass('hidden');
		    var _o;
		    var _field = '';
	    	if (typeof this._json.problem.choiceresponse != "undefined") {
	    		_o = this._json.problem.choiceresponse;
	    		this._type = 'choiceresponse';
	    		this._choices = 'checkboxgroup';
		    }
	    	if (typeof this._json.problem.multiplechoiceresponse != "undefined") {
	    		_o = this._json.problem.multiplechoiceresponse;
	    		this._type = 'multiplechoiceresponse';
	    		this._choices = 'choicegroup';
		    }
	    	if (typeof this._json.problem.optionresponse != "undefined") {
	    		_o = this._json.problem.optionresponse;
	    		this._type = 'optionresponse';
	    		this._choices = 'optioninput';
		    }
    		_field = '<div class="choice_container">';
				_field += this.renderChoices();
			_field += '</div>';

	    	if (typeof this._json.problem.numericalresponse != "undefined") {
	    		_o = this._json.problem.numericalresponse;
	    		this._type = 'numericalresponse';
	    		this._choices = 'responseparam';
    			_field = this.renderNumerical();
		    }

	    	if (typeof this._json.problem.stringresponse != "undefined") {
	    		_o = this._json.problem.stringresponse;
	    		this._type = 'stringresponse';
	    		this._choices = '';
    			_field = this.renderText();
		    }
		    
		    var editorContainer = $(".problem-editor > .row");
			this.htmlString +='<div id="new-problem-editor">';
				this.htmlString += '<div class="container" style="padding: 0px !important;">';
					this.htmlString += '<div class="row">';
					    this.htmlString += '<div class="col-sm-12 col-sm-12">';
					        this.htmlString += '<label>Description:</label>';
					        this.htmlString += '<textarea class="xml-box-p" rows="3" cols="40" onfocusout="window.__des.updateText(this, \'p\')">';
					            this.htmlString += _o.p;
					        this.htmlString += '</textarea>';

					        this.htmlString += '<label>Question:</label>';
					        this.htmlString += '<textarea class="xml-box-label" rows="3" cols="40" onfocusout="window.__des.updateText(this, \'label\')">';
					            this.htmlString += _o.label;
					        this.htmlString += '</textarea>';

					        this.htmlString += '<label>Tips:</label>';
					        this.htmlString += '<input type="text" class="xml-box-description" value="'+_o.description+'" onfocusout="window.__des.updateText(this, \'description\')">';

					    this.htmlString += '</div>';
					this.htmlString += '</div>';

			        this.htmlString += _field;
					
				this.htmlString += '</div>';
		    this.htmlString += '</div>';

		    $(".problem-editor > .row #new-problem-editor").remove();
		    editorContainer.append(this.htmlString);
		}

		this.renderText = function(){
			var	_answer = this._json.problem[this._type]._answer;
			var	string = '';
			string += '<div class="row m-t-15">';
	            string += '<div class="col-sm-3 col-sm-3 pull-left">'
	            	string += '<label>Correct Answer:</label>';
					string += '<input onfocusout="window.__des.updateText(this, \'_answer\')" type="text" name="" value="'+_answer+'" maxlength="50" />';
	            string += '</div>';
            string += '</div>';
            return string;
		}

		this.renderNumerical = function(){
			var	_answer = this._json.problem[this._type]._answer;
			var	_default = this._json.problem[this._type].responseparam._default;
			var	string = '';
			string += '<div class="row m-t-15">';
	            string += '<div class="col-sm-3 col-sm-3 pull-left">'
	            	string += '<label>Correct Answer:</label>';
					string += '<input onfocusout="window.__des.updateText(this, \'_answer\')" type="text" name="" value="'+_answer+'"  />';
	            string += '</div>';
				string += '<div class="col-sm-3 col-sm-3 pull-left">'
	            	string += '<label>Tolerance +-:</label>';
					string += '<input onfocusout="window.__des.updateToleranceValue(this)" type="text" name="" value="'+_default+'"  />';
		            // string += '<input onfocusout="window.__des.updateChoiceText(this, '+index+')" type="text" name="" value="'+value.__text+'"  />';
	            string += '</div>';
            string += '</div>';
            return string;
		}

		this.renderChoices = function(){
			var	choicesString = '';
			var isChecked = '';

			if(this._type === 'choiceresponse' || this._type === 'multiplechoiceresponse'){
				var choices = this._json.problem[this._type][this._choices].choice;
    		}
    		else if(this._type === 'optionresponse') {
				var choices = this._json.problem[this._type][this._choices].option;
    		}

    		choicesString += '<div class="row">';
				choicesString += '<div class="col-sm-3 col-sm-3 pull-left">';
					choicesString += '<label>';
						choicesString += 'Value:';
					choicesString += '</label>';
				choicesString += '</div>';
				choicesString += '<div class="col-sm-9 col-sm-9 pull-left">';
					choicesString += '<label>';
						choicesString += 'Label:';
					choicesString += '</label>';
				choicesString += '</div>';
			choicesString += '</div>';

			$.each(choices, function( index, value ) {
				if (typeof value != "undefined") {
					choicesString += '<div class="row m-t-15 choice_number_'+index+' clearfix">';
			            choicesString += '<div class="col-sm-3 col-sm-3 pull-left">'
				            choicesString += '<select class="choices_values" name="" onchange="window.__des.updateChoiceValue(this, '+index+')">';
				            	choicesString += '<option value="true" ';
				            	if(value._correct == 'true' || value._correct == 'True'){
				            		choicesString += 'selected="selected"';
				            	}
				            	choicesString += '>Correct</option>';
				            	choicesString += '<option value="false" ';
								if(value._correct == 'false' || value._correct == 'False'){
				            		choicesString += 'selected="selected"';
				            	}
				            	choicesString += '>In-correct</option>';
				            choicesString += '</select>';
			            choicesString += '</div>';
						choicesString += '<div class="col-sm-8 col-sm-8 pull-left">'
				            choicesString += '<input onfocusout="window.__des.updateChoiceText(this, '+index+')" type="text" name="" value="'+value.__text+'"  />';
			            choicesString += '</div>';
						choicesString += '<div class="col-sm-1 col-sm-1 pull-left removechoice">'
				            choicesString += '<a class="remove_choice" href="javascript:void(0)" onclick="window.__des.removeChoice(this, '+index+')"><span class="icon fa fa-trash-o" aria-hidden="true"></span></a>';
			            choicesString += '</div>';
		            choicesString += '</div>';
				}
			});



			choicesString += '<div class="row">';
				choicesString += '<div class="col-sm-12 col-sm-12">';
					choicesString += '<a class="add_new_choice" href="javascript:void(0)" onclick="window.__des.addNewChoice()">';
						choicesString += 'Add new choice';
					choicesString += '</a>';
				choicesString += '</div>';
			choicesString += '</div>';
			return choicesString;
		}

		this.addNewChoice = function(){
			if(this._type === 'choiceresponse' || this._type === 'multiplechoiceresponse'){
				var choices = this._json.problem[this._type][this._choices].choice;
    		}
    		else if(this._type === 'optionresponse') {
				var choices = this._json.problem[this._type][this._choices].option;
    		}

    		if(choices.length <= 5){
    			choices.push({
    				_correct: 'false',
    				__text: 'an incorrect answer'
    			}); 
    		}

    		$('.choice_container').empty();
    		$('.choice_container').append(this.renderChoices());
		}

		this.removeChoice = function(event, index){
			if(this._type === 'choiceresponse' || this._type === 'multiplechoiceresponse'){
				var _c = this._json.problem[this._type][this._choices].choice;
			}
			if(this._type === 'optionresponse'){
				var _c = this._json.problem[this._type][this._choices].option;
			}

			var	newChoices = [];
			$.each(_c, function( _i, _v ) {
				if(index != _i){
    				newChoices.push(_v);
				}
			});

			if(this._type === 'choiceresponse' || this._type === 'multiplechoiceresponse'){
				this._json.problem[this._type][this._choices].choice = newChoices;
			}
			if(this._type === 'optionresponse'){
				this._json.problem[this._type][this._choices].option = newChoices;
			}
    		$('.choice_container').empty();
    		$('.choice_container').append(this.renderChoices());
		}

		this.updateText = function(event, type) {
			this._json.problem[this._type][type] = event.value;
		}

		this.updateToleranceValue = function(event){
			this._json.problem[this._type].responseparam._default = event.value;
		}

		this.updateChoiceValue = function(event, index){
			var selectedVal = event.value;
			var _a = this;
			var choices_values = document.getElementsByClassName("choices_values");

			if(this._type === 'choiceresponse'){
				this._json.problem[this._type][this._choices].choice[index]._correct = event.value;
			}

			if(this._type === 'multiplechoiceresponse'){
				$.each(this._json.problem[this._type][this._choices].choice, function( _i, _v ) {
					if(_i == index){
						newVal = selectedVal;
					} else {
						newVal = 'false';
					}
					_a._json.problem[_a._type][_a._choices].choice[_i]._correct = newVal;
					choices_values[_i].value = newVal;
				});
				/*this._json.problem[this._type][this._choices].option[index]._correct = event.value;*/
			}

			if(this._type === 'optionresponse'){ 
				$.each(this._json.problem[this._type][this._choices].option, function( _i, _v ) {
					if(_i == index){
						newVal = selectedVal;
					} else {
						newVal = 'false';
					}
					_a._json.problem[_a._type][_a._choices].option[_i]._correct = newVal;
					choices_values[_i].value = newVal;
				});
			}
		} 

		this.updateChoiceText = function(event, index){
			if(this._type === 'choiceresponse' || this._type === 'multiplechoiceresponse'){
				this._json.problem[this._type][this._choices].choice[index].__text = event.value;
			}

			if(this._type === 'optionresponse'){
				this._json.problem[this._type][this._choices].option[index].__text = event.value;
			}
		}

	    this.convertJSon2XML = function() {
	    	var x2js = new X2JS();
	        this._xml = x2js.json2xml_str(this._json);
	        return this._xml;
	    }

	    this.createMarkDown = function(){
	    	var type = this._json.problem[this._type];

	    	var md = type.p + "\n\n";
	    	md += ">>" + type.label + "||" + type.description + "<<\n\n";

	    	var	_type = this._type;
	    	if(_type === 'choiceresponse'){
		    	$.each(type[this._choices].choice, function( index, value ) {
					if(value._correct === "true"){
						md += "[x] ";
					} 
					else {
						md += "[ ] ";
					}
					md += value.__text + "\n";
				});
    		}
    		if(_type === 'multiplechoiceresponse') {
		    	$.each(type[this._choices].choice, function( index, value ) {
					if(value._correct === "true"){
						md += "(x) ";
					} 
					else {
						md += "( ) ";
					}
					md += value.__text + "\n";
				});
		    }
    		if(_type === 'optionresponse') {
    			md += "[[\n";
		    	$.each(type[this._choices].option, function( index, value ) {
					if(value._correct === "True" || value._correct === "true"){
						md += "("+value.__text+")" + "\n";
					} 
					else {
						md += value.__text + "\n";
					}
				});
				md += "]]\n";
		    }
		    if(_type === 'numericalresponse') {
				var	_default = this._json.problem.numericalresponse.responseparam._default;
				md += '= ';
				md += this._json.problem.numericalresponse._answer;
				if(_default.length > 0) {
					md += ' +-' + _default;
				}
				md += "\n";
			}
		    if(_type === 'stringresponse') {
				md += '= ';
				md += this._json.problem.stringresponse._answer;
				md += "\n";
			}
	    	return md;
	    }
	}
});

define(['jquery', 'jquery.cookie'], function($) {
    'use strict';

    window.reg = {
        validateField: function(fieldId){
            var fieldVal = $("#register_form #" + fieldId);
            var val = fieldVal.val();
            if(val.length > 2){
                return true;
            } else {
                return false;
            }
        }
    }

    return function() {
        $('form :input')
            .focus(function() {
                $('label[for="' + this.id + '"]').addClass('is-focused');
            })
            .blur(function() {
                $('label').removeClass('is-focused');
            });

        $('form#register_form').submit(function(event) {
            event.preventDefault();
            var submit_data = $('#register_form').serialize();

            var password = $('#password').val();
            var verifypassword = $('#verifypassword').val();

            if(password != verifypassword){
                $('#register_form .error_message').text('');
                $('#register_form .error_message').addClass('hidden');
                $('#password_error').text('Password does not match the verify password.').removeClass('hidden');
                $('#verifypassword_error').text('Password does not match the verify password.').removeClass('hidden');
            } else {
                $.ajax({
                    url: '/create_account',
                    type: 'POST',
                    dataType: 'json',
                    headers: {'X-CSRFToken': $.cookie('csrftoken')},
                    notifyOnError: false,
                    data: submit_data,
                    beforeSend: function(){
                        $('#register_form .error_message').text('');
                        $('#register_form .error_message').addClass('hidden');
                    },
                    success: function(json) {
                        location.href = '/course/';
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        var json = $.parseJSON(jqXHR.responseText);
                        var fieldError = $('#'+json.field+'_error');
                        fieldError.text(json.value);
                        fieldError.removeClass('hidden');
                    }
                });
            }
        });

        $('input#password').blur(function() {
            var $formErrors = $('#password_error'),
                data = {
                    password: $('#password').val()
                };

            // Uninitialize the errors on blur
            $formErrors.empty();
            $formErrors.addClass('hidden');

            $.ajax({
                url: '/api/user/v1/validation/registration',
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(json) {
                    _.each(json.validation_decisions, function(value, key) {
                        if (key === 'password' && value) {
                            $formErrors.html(value);
                            $formErrors.removeClass('hidden');
                        }
                    });
                }
            });
        });
    };
});

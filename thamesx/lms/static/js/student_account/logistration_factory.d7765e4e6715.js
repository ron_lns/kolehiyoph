(function(e) {
    "use strict";
    RequireJS.define("js/student_account/models/LoginModel", ["jquery", "backbone", "jquery.url"], function(e, t) {
        return t.Model.extend({
            defaults: {
                email: "",
                password: "",
                remember: !1
            },
            ajaxType: "",
            urlRoot: "",
            initialize: function(e, t) {
                this.ajaxType = t.method, this.urlRoot = t.url
            },
            sync: function(t, r) {
                var i, s = {
                        "X-CSRFToken": e.cookie("csrftoken")
                    },
                    n = {},
                    a = e.url("?course_id");
                a && (i = JSON.stringify({
                    enroll_course_id: decodeURIComponent(a)
                })), e.extend(n, r.attributes, {
                    analytics: i
                }), e.ajax({
                    url: r.urlRoot,
                    type: r.ajaxType,
                    data: n,
                    headers: s,
                    success: function() {
                        r.trigger("sync")
                    },
                    error: function(e) {
                        r.trigger("error", e)
                    }
                })
            }
        })
    })
}).call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/models/PasswordResetModel", ["jquery", "backbone"], function(e, t) {
            return t.Model.extend({
                defaults: {
                    email: ""
                },
                ajaxType: "",
                urlRoot: "",
                initialize: function(e, t) {
                    this.ajaxType = t.method, this.urlRoot = t.url
                },
                sync: function(t, r) {
                    var i = {
                        "X-CSRFToken": e.cookie("csrftoken")
                    };
                    e.ajax({
                        url: r.urlRoot,
                        type: r.ajaxType,
                        data: r.attributes,
                        headers: i,
                        success: function() {
                            r.trigger("sync")
                        },
                        error: function(e) {
                            r.trigger("error", e)
                        }
                    })
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/models/RegisterModel", ["jquery", "backbone", "jquery.url"], function(e, t) {
            return t.Model.extend({
                defaults: {
                    email: "",
                    name: "",
                    username: "",
                    password: "",
                    level_of_education: "",
                    gender: "",
                    year_of_birth: "",
                    mailing_address: "",
                    goals: ""
                },
                ajaxType: "",
                urlRoot: "",
                initialize: function(e, t) {
                    this.ajaxType = t.method, this.urlRoot = t.url
                },
                sync: function(t, r) {
                    var i = {
                            "X-CSRFToken": e.cookie("csrftoken")
                        },
                        s = {},
                        n = e.url("?course_id");
                    n && (s.course_id = decodeURIComponent(n)), e.extend(s, r.attributes), e.ajax({
                        url: r.urlRoot,
                        type: r.ajaxType,
                        data: s,
                        headers: i,
                        success: function() {
                            r.trigger("sync")
                        },
                        error: function(e) {
                            r.trigger("error", e)
                        }
                    })
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("common/js/utils/edx.utils.validate", ["jquery", "underscore", "underscore.string", "gettext"], function(e, t, r, i) {
            var s;
            return t.mixin(r.exports()), s = function() {
                var r = {
                    validate: {
                        template: t.template("<li><%- content %></li>"),
                        msg: {
                            email: i("The email address you've provided isn't formatted correctly."),
                            min: i("%(field)s must have at least %(count)d characters."),
                            max: i("%(field)s can only contain up to %(count)d characters."),
                            required: i("Please enter your %(field)s.")
                        },
                        field: function(t) {
                            var i = e(t),
                                s = !0,
                                n = !0,
                                a = !0,
                                o = !0,
                                l = {},
                                u = r.validate.isBlank(i);
                            return r.validate.isRequired(i) ? u ? s = !1 : (n = r.validate.str.minlength(i), a = r.validate.str.maxlength(i), o = r.validate.email.valid(i)) : u || (n = r.validate.str.minlength(i), a = r.validate.str.maxlength(i), o = r.validate.email.valid(i)), l.isValid = s && n && a && o, l.isValid || (r.validate.removeDefault(i), l.message = r.validate.getMessage(i, {
                                required: s,
                                min: n,
                                max: a,
                                email: o
                            })), l
                        },
                        str: {
                            minlength: function(e) {
                                var t = e.attr("minlength") || 0;
                                return t <= e.val().length
                            },
                            maxlength: function(e) {
                                var t = e.attr("maxlength") || !1;
                                return t ? t >= e.val().length : !0
                            }
                        },
                        isRequired: function(e) {
                            return e.attr("required")
                        },
                        isBlank: function(e) {
                            var t, r = e.attr("type");
                            return t = "checkbox" === r ? !e.prop("checked") : "select" === r ? e.data("isdefault") === !0 : !e.val()
                        },
                        email: {
                            regex: new RegExp(["(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*", '|^"([\\001-\\010\\013\\014\\016-\\037!#-\\[\\]-\\177]|\\\\[\\001-\\011\\013\\014\\016-\\177])*"', ")@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\\.)+)(?:[A-Z0-9-]{2,63})", "|\\[(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}\\]$"].join(""), "i"),
                            valid: function(e) {
                                return "email" === e.attr("type") ? r.validate.email.format(e.val()) : !0
                            },
                            format: function(e) {
                                return r.validate.email.regex.test(e)
                            }
                        },
                        getLabel: function(t) {
                            return e("label[for=" + t + "] > span.label-text").text().split("*")[0].trim()
                        },
                        getMessage: function(i, s) {
                            var n, a, o, l, u, d = [];
                            return t.each(s, function(s, c) {
                                s || (n = r.validate.getLabel(i.attr("id")), l = i.data("errormsg-" + c) || !1, u = e("#" + i.attr("id") + "-validation-error-msg").text() || !1, l ? o = l : u ? o = u : (a = {
                                    field: n
                                }, "min" === c ? a.count = parseInt(i.attr("minlength"), 10) : "max" === c && (a.count = parseInt(i.attr("maxlength"), 10)), o = t.sprintf(r.validate.msg[c], a)), d.push(r.validate.template({
                                    content: o
                                })))
                            }), d.join(" ")
                        },
                        removeDefault: function(e) {
                            e.setCustomValidity && e.setCustomValidity(" ")
                        }
                    }
                };
                return {
                    validate: r.validate.field
                }
            }()
        })
    }.call(this, define || RequireJS.define), RequireJS.define("text", {
        load: function(e) {
            throw new Error("Dynamic load not allowed: " + e)
        }
    }), RequireJS.define("text!templates/student_account/form_errors.underscore", [], function() {
        return '<div class="<%- jsHook %> status submission-error">\n    <h4 class="message-title"><%- title %></h4>\n    <ul class="message-copy">\n        <%= HtmlUtils.ensureHtml(messagesHtml) %>\n    </ul>\n</div>\n'
    }),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/FormView", ["jquery", "underscore", "backbone", "common/js/utils/edx.utils.validate", "edx-ui-toolkit/js/utils/html-utils", "edx-ui-toolkit/js/utils/string-utils", "text!templates/student_account/form_errors.underscore"], function(e, t, r, i, s, n, a) {
            return r.View.extend({
                tagName: "form",
                el: "",
                tpl: "",
                fieldTpl: "#form_field-tpl",
                formErrorsTpl: a,
                formErrorsJsHook: "js-form-errors",
                defaultFormErrorsTitle: gettext("An error occurred."),
                events: {},
                errors: [],
                formType: "",
                $form: {},
                fields: [],
                liveValidationFields: [],
                requiredStr: "",
                optionalStr: gettext("(optional)"),
                submitButton: "",
                initialize: function(t) {
                    this.model = t.model, this.preRender(t), this.tpl = e(this.tpl).html(), this.fieldTpl = e(this.fieldTpl).html(), this.buildForm(t.fields), this.listenTo(this.model, "error", this.saveError)
                },
                preRender: function(e) {
                    return e
                },
                render: function(r) {
                    var i = r || "";
                    return e(this.el).html(t.template(this.tpl)({
                        fields: i
                    })), this.postRender(), this
                },
                postRender: function() {
                    var t = e(this.el);
                    this.$form = t.find("form"), this.$formFeedback = t.find(".js-form-feedback"), this.$submitButton = t.find(this.submitButton)
                },
                buildForm: function(t) {
                    var r, i = [],
                        n = t.length,
                        a = this.fieldTpl;
                    for (this.fields = t, r = 0; n > r; r++) t[r].errorMessages && (t[r].errorMessages = this.escapeStrings(t[r].errorMessages)), i.push(s.template(a)(e.extend(t[r], {
                        form: this.formType,
                        requiredStr: this.requiredStr,
                        optionalStr: this.optionalStr,
                        supplementalText: t[r].supplementalText || "",
                        supplementalLink: t[r].supplementalLink || ""
                    })));
                    this.render(i.join(""))
                },
                element: {
                    hide: function(e) {
                        e && e.addClass("hidden")
                    },
                    scrollTop: function(t) {
                        e("html,body").animate({
                            scrollTop: t.offset().top
                        }, "slow")
                    },
                    show: function(e) {
                        e && e.removeClass("hidden")
                    }
                },
                escapeStrings: function(e) {
                    return t.each(e, function(r, i) {
                        e[i] = t.escape(r)
                    }), e
                },
                forgotPassword: function(e) {
                    e.preventDefault(), this.trigger("password-help")
                },
                getFormData: function() {
                    var r, i, s, n = {},
                        a = this.$form,
                        o = a[0].elements,
                        l = o.length,
                        u = "",
                        d = [],
                        c = {};
                    for (r = 0; l > r; r++) i = e(o[r]), s = a.find("label[for=" + i.attr("id") + "]"), u = i.attr("name") || !1, "email" === u && i.val(i.val().trim()), u && (c = this.validate(o[r]), c.isValid ? (n[u] = "checkbox" === i.attr("type") ? i.is(":checked") : i.val(), i.removeClass("error"), s.removeClass("error")) : (d.push(c.message), i.addClass("error"), s.addClass("error")));
                    return this.errors = t.uniq(d), n
                },
                saveError: function(e) {
                    this.errors = [n.interpolate("<li>{error}</li>", {
                        error: e.responseText
                    })], this.renderErrors(this.defaultFormErrorsTitle, this.errors), this.scrollToFormFeedback(), this.toggleDisableButton(!1)
                },
                renderErrors: function(e, t) {
                    this.clearFormErrors(), this.renderFormFeedback(this.formErrorsTpl, {
                        jsHook: this.formErrorsJsHook,
                        title: e,
                        messagesHtml: s.HTML(t.join(""))
                    })
                },
                renderFormFeedback: function(e, t) {
                    var r = s.template(e);
                    s.prepend(this.$formFeedback, r(t))
                },
                setExtraData: function(e) {
                    return e
                },
                submitForm: function(e) {
                    var r = this.getFormData();
                    t.isUndefined(e) || e.preventDefault(), this.toggleDisableButton(!0), t.compact(this.errors).length ? (this.renderErrors(this.defaultFormErrorsTitle, this.errors), this.scrollToFormFeedback(), this.toggleDisableButton(!1)) : (r = this.setExtraData(r), this.model.set(r), this.model.save(), this.clearFormErrors()), this.postFormSubmission()
                },
                postFormSubmission: function() {
                    return !0
                },
                resetValidationVariables: function() {
                    return !0
                },
                clearFormErrors: function() {
                    var e = "." + this.formErrorsJsHook;
                    this.clearFormFeedbackItems(e)
                },
                clearFormFeedbackItems: function(e) {
                    var t = this.$formFeedback.find(e);
                    t.length > 0 && t.remove()
                },
                toggleDisableButton: function(e) {
                    this.$submitButton && this.$submitButton.attr("disabled", e)
                },
                scrollToFormFeedback: function() {
                    var t = this;
                    console.log(this.$formFeedback.offset().top);
                    e("html,body").animate({
                        // scrollTop: this.$formFeedback.offset().top
                        scrollTop: 0
                    }, "slow", function() {
                        t.resetValidationVariables()
                    }), this.$formFeedback.focus()
                },
                validate: function(e) {
                    return i.validate(e)
                },
                liveValidate: function(t, r, i, s, n, a) {
                    e.ajax({
                        url: r,
                        dataType: i,
                        data: s,
                        method: n,
                        success: function(e) {
                            a.trigger("validation", t, e)
                        }
                    })
                },
                inLiveValidationFields: function(e) {
                    var t, r = e.attr("name") || !1;
                    for (t = 0; t < this.liveValidationFields.length; ++t)
                        if (this.liveValidationFields[t] === r) return !0;
                    return !1
                }
            })
        })
    }.call(this, define || RequireJS.define), RequireJS.define("text!templates/student_account/form_success.underscore", [], function() {
        return '<div class="<%- jsHook %> status submission-success">\n    <h4 class="message-title"><%- title %></h4>\n    <div class="message-copy">\n        <%= HtmlUtils.ensureHtml(messageHtml) %>\n    </div>\n</div>\n'
    }), RequireJS.define("text!templates/student_account/form_status.underscore", [], function() {
        return '<div class="<%- jsHook %> status">\n    <div class="message-copy">\n        <%= HtmlUtils.ensureHtml(message) %>\n    </p>\n</div>\n\n'
    }),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/LoginView", ["jquery", "underscore", "gettext", "edx-ui-toolkit/js/utils/html-utils", "edx-ui-toolkit/js/utils/string-utils", "js/student_account/views/FormView", "text!templates/student_account/form_success.underscore", "text!templates/student_account/form_status.underscore"], function(e, t, r, i, s, n, a, o) {
            return n.extend({
                el: "#login-form",
                tpl: "#login-tpl",
                events: {
                    "click .js-login": "submitForm",
                    "click .forgot-password": "forgotPassword",
                    "click .login-provider": "thirdPartyAuth"
                },
                formType: "login",
                requiredStr: "",
                optionalStr: "",
                submitButton: ".js-login",
                formSuccessTpl: a,
                formStatusTpl: o,
                authWarningJsHook: "js-auth-warning",
                passwordResetSuccessJsHook: "js-password-reset-success",
                defaultFormErrorsTitle: r("We couldn't sign you in."),
                preRender: function(e) {
                    this.providers = e.thirdPartyAuth.providers || [], this.hasSecondaryProviders = e.thirdPartyAuth.secondaryProviders && e.thirdPartyAuth.secondaryProviders.length, this.currentProvider = e.thirdPartyAuth.currentProvider || "", this.syncLearnerProfileData = e.thirdPartyAuth.syncLearnerProfileData || !1, this.errorMessage = e.thirdPartyAuth.errorMessage || "", this.platformName = e.platformName, this.resetModel = e.resetModel, this.supportURL = e.supportURL, this.passwordResetSupportUrl = e.passwordResetSupportUrl, this.createAccountOption = e.createAccountOption, this.accountActivationMessages = e.accountActivationMessages, this.hideAuthWarnings = e.hideAuthWarnings, this.pipelineUserDetails = e.pipelineUserDetails, this.enterpriseName = e.enterpriseName, this.listenTo(this.model, "sync", this.saveSuccess), this.listenTo(this.resetModel, "sync", this.resetEmail)
                },
                render: function(r) {
                    var i = r || "";
                    return e(this.el).html(t.template(this.tpl)({
                        context: {
                            fields: i,
                            currentProvider: this.currentProvider,
                            syncLearnerProfileData: this.syncLearnerProfileData,
                            providers: this.providers,
                            hasSecondaryProviders: this.hasSecondaryProviders,
                            platformName: this.platformName,
                            createAccountOption: this.createAccountOption,
                            pipelineUserDetails: this.pipelineUserDetails,
                            enterpriseName: this.enterpriseName
                        }
                    })), this.postRender(), this
                },
                postRender: function() {
                    var i;
                    this.$container = e(this.el), this.$form = this.$container.find("form"), this.$formFeedback = this.$container.find(".js-form-feedback"), this.$submitButton = this.$container.find(this.submitButton), this.errorMessage ? (i = t.sprintf(r("An error occurred when signing you in to %s."), this.platformName), this.renderErrors(i, [this.errorMessage])) : this.currentProvider && this.model.save(), this.renderAccountActivationMessages()
                },
                renderAccountActivationMessages: function() {
                    t.each(this.accountActivationMessages, this.renderAccountActivationMessage, this)
                },
                renderAccountActivationMessage: function(e) {
                    this.renderFormFeedback(this.formStatusTpl, {
                        jsHook: e.tags,
                        message: i.HTML(e.message)
                    })
                },
                forgotPassword: function(e) {
                    e.preventDefault(), this.trigger("password-help"), this.clearPasswordResetSuccess()
                },
                postFormSubmission: function() {
                    this.clearPasswordResetSuccess()
                },
                resetEmail: function() {
                    var t = e("#password-reset-email").val(),
                        n = r("Check Your Email"),
                        a = i.interpolateHtml(r("{paragraphStart}You entered {boldStart}{email}{boldEnd}. If this email address is associated with your {platform_name} account, we will send a message with password reset instructions to this email address.{paragraphEnd}{paragraphStart}If you do not receive a password reset message, verify that you entered the correct email address, or check your spam folder.{paragraphEnd}{paragraphStart}If you need further assistance, {anchorStart}contact technical support{anchorEnd}.{paragraphEnd}"), {
                            boldStart: i.HTML("<b>"),
                            boldEnd: i.HTML("</b>"),
                            paragraphStart: i.HTML("<p>"),
                            paragraphEnd: i.HTML("</p>"),
                            email: t,
                            platform_name: this.platformName,
                            anchorStart: i.HTML(s.interpolate('<a href="{passwordResetSupportUrl}">', {
                                passwordResetSupportUrl: this.passwordResetSupportUrl
                            })),
                            anchorEnd: i.HTML("</a>")
                        });
                    this.clearFormErrors(), this.clearPasswordResetSuccess(), this.renderFormFeedback(this.formSuccessTpl, {
                        jsHook: this.passwordResetSuccessJsHook,
                        title: n,
                        messageHtml: a
                    })
                },
                thirdPartyAuth: function(t) {
                    var r = e(t.currentTarget).data("provider-url") || "";
                    r && (window.location.href = r)
                },
                saveSuccess: function() {
                    this.trigger("auth-complete"), this.clearPasswordResetSuccess()
                },
                saveError: function(e) {
                    var t = e.responseText;
                    0 === e.status ? t = r("An error has occurred. Check your Internet connection and try again.") : 500 === e.status && (t = r("An error has occurred. Try refreshing the page, or check your Internet connection.")), this.errors = [s.interpolate("<li>{msg}</li>", {
                        msg: t
                    })], this.clearPasswordResetSuccess(), 403 === e.status && "third-party-auth" === e.responseText && this.currentProvider ? this.hideAuthWarnings || (this.clearFormErrors(), this.renderAuthWarning()) : this.renderErrors(this.defaultFormErrorsTitle, this.errors), this.toggleDisableButton(!1)
                },
                renderAuthWarning: function() {
                    var e = t.sprintf(r("You have successfully signed into %(currentProvider)s, but your %(currentProvider)s account does not have a linked %(platformName)s account. To link your accounts, sign in now using your %(platformName)s password."), {
                        currentProvider: this.currentProvider,
                        platformName: this.platformName
                    });
                    this.clearAuthWarning(), this.renderFormFeedback(this.formStatusTpl, {
                        jsHook: this.authWarningJsHook,
                        message: e
                    })
                },
                clearPasswordResetSuccess: function() {
                    var e = "." + this.passwordResetSuccessJsHook;
                    this.clearFormFeedbackItems(e)
                },
                clearAuthWarning: function() {
                    var e = "." + this.authWarningJsHook;
                    this.clearFormFeedbackItems(e)
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/PasswordResetView", ["jquery", "js/student_account/views/FormView"], function(e, t) {
            return t.extend({
                el: "#password-reset-form",
                tpl: "#password_reset-tpl",
                events: {
                    "click .js-reset": "submitForm"
                },
                formType: "password-reset",
                requiredStr: "",
                optionalStr: "",
                submitButton: ".js-reset",
                preRender: function() {
                    this.element.show(e(this.el)), this.element.show(e(this.el).parent()), this.listenTo(this.model, "sync", this.saveSuccess)
                },
                saveSuccess: function() {
                    this.trigger("password-email-sent"), this.$el.empty().off(), this.stopListening()
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/RegisterView", ["jquery", "underscore", "gettext", "edx-ui-toolkit/js/utils/string-utils", "edx-ui-toolkit/js/utils/html-utils", "js/student_account/views/FormView", "text!templates/student_account/form_status.underscore"], function(e, t, r, i, s, n, a) {
            return n.extend({
                el: "#register-form",
                tpl: "#register-tpl",
                validationUrl: "/api/user/v1/validation/registration",
                events: {
                    "click .js-register": "submitForm",
                    "click .login-provider": "thirdPartyAuth",
                    'click input[required][type="checkbox"]': "liveValidateHandler",
                    "blur input[required], textarea[required], select[required]": "liveValidateHandler",
                    "focus input[required], textarea[required], select[required]": "handleRequiredInputFocus"
                },
                liveValidationFields: ["name", "username", "password", "email", "confirm_email", "country", "honor_code", "terms_of_service"],
                formType: "register",
                formStatusTpl: a,
                authWarningJsHook: "js-auth-warning",
                defaultFormErrorsTitle: r("We couldn't create your account."),
                submitButton: ".js-register",
                positiveValidationIcon: "fa-check",
                negativeValidationIcon: "fa-exclamation",
                successfulValidationDisplaySeconds: 3,
                positiveValidationEnabled: !0,
                negativeValidationEnabled: !0,
                preRender: function(e) {
                    this.providers = e.thirdPartyAuth.providers || [], this.hasSecondaryProviders = e.thirdPartyAuth.secondaryProviders && e.thirdPartyAuth.secondaryProviders.length, this.currentProvider = e.thirdPartyAuth.currentProvider || "", this.syncLearnerProfileData = e.thirdPartyAuth.syncLearnerProfileData || !1, this.errorMessage = e.thirdPartyAuth.errorMessage || "", this.platformName = e.platformName, this.autoSubmit = e.thirdPartyAuth.autoSubmitRegForm, this.hideAuthWarnings = e.hideAuthWarnings, this.autoRegisterWelcomeMessage = e.thirdPartyAuth.autoRegisterWelcomeMessage || "", this.registerFormSubmitButtonText = e.thirdPartyAuth.registerFormSubmitButtonText || t("Create Account"), this.listenTo(this.model, "sync", this.saveSuccess), this.listenTo(this.model, "validation", this.renderLiveValidations)
                },
                renderFields: function(t, r) {
                    var i, n = [],
                        a = this.fieldTpl;
                    for (n.push(s.joinHtml(s.HTML('<div class="'), r, s.HTML('">'))), i = 0; i < t.length; i++) n.push(s.template(a)(e.extend(t[i], {
                        form: this.formType,
                        requiredStr: this.requiredStr,
                        optionalStr: this.optionalStr,
                        supplementalText: t[i].supplementalText || "",
                        supplementalLink: t[i].supplementalLink || ""
                    })));
                    return n.push("</div>"), n
                },
                buildForm: function(e) {
                    var t, r, i = [],
                        s = e.length,
                        n = [],
                        a = [];
                    for (this.fields = e, this.hasOptionalFields = !1, t = 0; s > t; t++) r = e[t], r.errorMessages && (r.errorMessages = this.escapeStrings(r.errorMessages)), r.required ? n.push(r) : ("hidden" !== r.type && (this.hasOptionalFields = !0), a.push(r));
                    i = this.renderFields(n, "required-fields"), i.push.apply(i, this.renderFields(a, "optional-fields")), this.render(i.join(""))
                },
                render: function(i) {
                    i = '<div class="required-fields">\
                            <div class="form-field email-email mB-30">\
                                <label for="register-email">\
                                    <span class="label-text">Email</span>\
                                    <span id="register-email-required-label" class="label-required hidden">\
                                                            </span>\
                                    <span class="icon fa" id="register-email-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <input id="register-email" type="email" name="email" class="input-block " aria-describedby="register-email-desc register-email-validation-error" minlength="3" maxlength="254" required value="" />\
                                <p>\
                                    <span id="register-email-validation-error" class="tip error" aria-live="assertive">\
                                                                <span class="sr-only">ERROR: </span>\
                                    <span id="register-email-validation-error-msg"></span>\
                                    </span>\
                                    <span class="tip tip-input" id="register-email-desc">This is what you will use to login.</span>\
                                </p>\
                            </div>\
                            <div class="form-field text-name mB-30">\
                                <label for="register-name">\
                                    <span class="label-text">Full Name</span>\
                                    <span id="register-name-required-label" class="label-required hidden">\
                                                                </span>\
                                    <span class="icon fa" id="register-name-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <input id="register-name" type="text" name="name" class="input-block " aria-describedby="register-name-desc register-name-validation-error" maxlength="255" required value="" />\
                                <p>\
                                    <span id="register-name-validation-error" class="tip error" aria-live="assertive">\
                                                <span class="sr-only">ERROR: </span>\
                                    <span id="register-name-validation-error-msg"></span>\
                                    </span>\
                                    <span class="tip tip-input" id="register-name-desc">This name will be used on any certificates that you earn.</span>\
                                </p>\
                            </div>\
                            <div class="form-field text-student-id mB-30">\
                                <label for="register-student-id">\
                                    <span class="label-text">Student ID</span>\
                                    <span id="register-student-id-required-label" class="label-required hidden">\
                                    </span>\
                                    <span class="icon fa" id="register-student-id-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <input id="teacherid" type="text" name="school_id" class="input-block "  aria-describedby="register-student-id-desc register-student-id-validation-error" maxlength="255" required value="" >\
                                <p>\
                                    <span id="register-student-id-validation-error" class="tip error" aria-live="assertive">\
                                        <span class="sr-only">ERROR: </span>\
                                        <span id="register-student-id-validation-error-msg"></span>\
                                    </span>\
                                </p>\
                            </div>\
                            <div class="form-field text-username mB-30">\
                                <label for="register-username">\
                                    <span class="label-text">Public Username</span>\
                                    <span id="register-username-required-label" class="label-required hidden"></span>\
                                    <span class="icon fa" id="register-username-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <input id="register-username" type="text" name="username" class="input-block " aria-describedby="register-username-desc register-username-validation-error" minlength="2" maxlength="30" required value="" />\
                                <p>\
                                    <span id="register-username-validation-error" class="tip error" aria-live="assertive">\
                                                <span class="sr-only">ERROR: </span>\
                                    <span id="register-username-validation-error-msg"></span>\
                                    </span>\
                                    <span class="tip tip-input" id="register-username-desc">Must not contain space.</span>\
                                    <span class="tip tip-input" id="register-username-desc">The name that will identify you in your courses. It cannot be changed later.</span>\
                                </p>\
                            </div>\
                            <div class="form-field password-password mB-30">\
                                <label for="register-password">\
                                    <span class="label-text">Password</span>\
                                    <span id="register-password-required-label" class="label-required hidden"></span>\
                                    <span class="icon fa" id="register-password-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <input id="register-password" type="password" name="password" class="input-block " aria-describedby="register-password-desc register-password-validation-error" minlength="2" maxlength="75" required value="" />\
                                <p>\
                                    <span id="register-password-validation-error" class="tip error" aria-live="assertive">\
                                                <span class="sr-only">ERROR: </span>\
                                    <span id="register-password-validation-error-msg"></span>\
                                    </span>\
                                    <span class="tip tip-input" id="register-password-desc">Your password must contain at least 2 characters.</span>\
                                </p>\
                            </div>\
                            <div class="form-field select-country mB-30">\
                                <label for="register-country">\
                                    <span class="label-text">Country or Region of Residence</span>\
                                    <span id="register-country-required-label" class="label-required hidden"></span>\
                                    <span class="icon fa" id="register-country-validation-icon" aria-hidden="true"></span>\
                                </label>\
                                <select id="register-country" name="country" class="input-inline" aria-describedby="register-country-desc register-country-validation-error" data-errormsg-required="Select your country or region of residence." aria-required="true" required>\
                                    <option value="" data-isdefault="true" selected>--</option>\
                                    <option value="AF">Afghanistan</option>\
                                    <option value="AX">Åland Islands</option>\
                                    <option value="AL">Albania</option>\
                                    <option value="DZ">Algeria</option>\
                                    <option value="AS">American Samoa</option>\
                                    <option value="AD">Andorra</option>\
                                    <option value="AO">Angola</option>\
                                    <option value="AI">Anguilla</option>\
                                    <option value="AQ">Antarctica</option>\
                                    <option value="AG">Antigua and Barbuda</option>\
                                    <option value="AR">Argentina</option>\
                                    <option value="AM">Armenia</option>\
                                    <option value="AW">Aruba</option>\
                                    <option value="AU">Australia</option>\
                                    <option value="AT">Austria</option>\
                                    <option value="AZ">Azerbaijan</option>\
                                    <option value="BS">Bahamas</option>\
                                    <option value="BH">Bahrain</option>\
                                    <option value="BD">Bangladesh</option>\
                                    <option value="BB">Barbados</option>\
                                    <option value="BY">Belarus</option>\
                                    <option value="BE">Belgium</option>\
                                    <option value="BZ">Belize</option>\
                                    <option value="BJ">Benin</option>\
                                    <option value="BM">Bermuda</option>\
                                    <option value="BT">Bhutan</option>\
                                    <option value="BO">Bolivia</option>\
                                    <option value="BQ">Bonaire, Sint Eustatius and Saba</option>\
                                    <option value="BA">Bosnia and Herzegovina</option>\
                                    <option value="BW">Botswana</option>\
                                    <option value="BV">Bouvet Island</option>\
                                    <option value="BR">Brazil</option>\
                                    <option value="IO">British Indian Ocean Territory</option>\
                                    <option value="BN">Brunei</option>\
                                    <option value="BG">Bulgaria</option>\
                                    <option value="BF">Burkina Faso</option>\
                                    <option value="BI">Burundi</option>\
                                    <option value="CV">Cabo Verde</option>\
                                    <option value="KH">Cambodia</option>\
                                    <option value="CM">Cameroon</option>\
                                    <option value="CA">Canada</option>\
                                    <option value="KY">Cayman Islands</option>\
                                    <option value="CF">Central African Republic</option>\
                                    <option value="TD">Chad</option>\
                                    <option value="CL">Chile</option>\
                                    <option value="CN">China</option>\
                                    <option value="CX">Christmas Island</option>\
                                    <option value="CC">Cocos (Keeling) Islands</option>\
                                    <option value="CO">Colombia</option>\
                                    <option value="KM">Comoros</option>\
                                    <option value="CG">Congo</option>\
                                    <option value="CD">Congo (the Democratic Republic of the)</option>\
                                    <option value="CK">Cook Islands</option>\
                                    <option value="CR">Costa Rica</option>\
                                    <option value="CI">Côte d&#x27;Ivoire</option>\
                                    <option value="HR">Croatia</option>\
                                    <option value="CU">Cuba</option>\
                                    <option value="CW">Curaçao</option>\
                                    <option value="CY">Cyprus</option>\
                                    <option value="CZ">Czechia</option>\
                                    <option value="DK">Denmark</option>\
                                    <option value="DJ">Djibouti</option>\
                                    <option value="DM">Dominica</option>\
                                    <option value="DO">Dominican Republic</option>\
                                    <option value="EC">Ecuador</option>\
                                    <option value="EG">Egypt</option>\
                                    <option value="SV">El Salvador</option>\
                                    <option value="GQ">Equatorial Guinea</option>\
                                    <option value="ER">Eritrea</option>\
                                    <option value="EE">Estonia</option>\
                                    <option value="ET">Ethiopia</option>\
                                    <option value="FK">Falkland Islands [Malvinas]</option>\
                                    <option value="FO">Faroe Islands</option>\
                                    <option value="FJ">Fiji</option>\
                                    <option value="FI">Finland</option>\
                                    <option value="FR">France</option>\
                                    <option value="GF">French Guiana</option>\
                                    <option value="PF">French Polynesia</option>\
                                    <option value="TF">French Southern Territories</option>\
                                    <option value="GA">Gabon</option>\
                                    <option value="GM">Gambia</option>\
                                    <option value="GE">Georgia</option>\
                                    <option value="DE">Germany</option>\
                                    <option value="GH">Ghana</option>\
                                    <option value="GI">Gibraltar</option>\
                                    <option value="GR">Greece</option>\
                                    <option value="GL">Greenland</option>\
                                    <option value="GD">Grenada</option>\
                                    <option value="GP">Guadeloupe</option>\
                                    <option value="GU">Guam</option>\
                                    <option value="GT">Guatemala</option>\
                                    <option value="GG">Guernsey</option>\
                                    <option value="GN">Guinea</option>\
                                    <option value="GW">Guinea-Bissau</option>\
                                    <option value="GY">Guyana</option>\
                                    <option value="HT">Haiti</option>\
                                    <option value="HM">Heard Island and McDonald Islands</option>\
                                    <option value="VA">Holy See</option>\
                                    <option value="HN">Honduras</option>\
                                    <option value="HK">Hong Kong</option>\
                                    <option value="HU">Hungary</option>\
                                    <option value="IS">Iceland</option>\
                                    <option value="IN">India</option>\
                                    <option value="ID">Indonesia</option>\
                                    <option value="IR">Iran</option>\
                                    <option value="IQ">Iraq</option>\
                                    <option value="IE">Ireland</option>\
                                    <option value="IM">Isle of Man</option>\
                                    <option value="IL">Israel</option>\
                                    <option value="IT">Italy</option>\
                                    <option value="JM">Jamaica</option>\
                                    <option value="JP">Japan</option>\
                                    <option value="JE">Jersey</option>\
                                    <option value="JO">Jordan</option>\
                                    <option value="KZ">Kazakhstan</option>\
                                    <option value="KE">Kenya</option>\
                                    <option value="KI">Kiribati</option>\
                                    <option value="XK">Kosovo</option>\
                                    <option value="KW">Kuwait</option>\
                                    <option value="KG">Kyrgyzstan</option>\
                                    <option value="LA">Laos</option>\
                                    <option value="LV">Latvia</option>\
                                    <option value="LB">Lebanon</option>\
                                    <option value="LS">Lesotho</option>\
                                    <option value="LR">Liberia</option>\
                                    <option value="LY">Libya</option>\
                                    <option value="LI">Liechtenstein</option>\
                                    <option value="LT">Lithuania</option>\
                                    <option value="LU">Luxembourg</option>\
                                    <option value="MO">Macao</option>\
                                    <option value="MK">Macedonia</option>\
                                    <option value="MG">Madagascar</option>\
                                    <option value="MW">Malawi</option>\
                                    <option value="MY">Malaysia</option>\
                                    <option value="MV">Maldives</option>\
                                    <option value="ML">Mali</option>\
                                    <option value="MT">Malta</option>\
                                    <option value="MH">Marshall Islands</option>\
                                    <option value="MQ">Martinique</option>\
                                    <option value="MR">Mauritania</option>\
                                    <option value="MU">Mauritius</option>\
                                    <option value="YT">Mayotte</option>\
                                    <option value="MX">Mexico</option>\
                                    <option value="FM">Micronesia (Federated States of)</option>\
                                    <option value="MD">Moldova</option>\
                                    <option value="MC">Monaco</option>\
                                    <option value="MN">Mongolia</option>\
                                    <option value="ME">Montenegro</option>\
                                    <option value="MS">Montserrat</option>\
                                    <option value="MA">Morocco</option>\
                                    <option value="MZ">Mozambique</option>\
                                    <option value="MM">Myanmar</option>\
                                    <option value="NA">Namibia</option>\
                                    <option value="NR">Nauru</option>\
                                    <option value="NP">Nepal</option>\
                                    <option value="NL">Netherlands</option>\
                                    <option value="NC">New Caledonia</option>\
                                    <option value="NZ">New Zealand</option>\
                                    <option value="NI">Nicaragua</option>\
                                    <option value="NE">Niger</option>\
                                    <option value="NG">Nigeria</option>\
                                    <option value="NU">Niue</option>\
                                    <option value="NF">Norfolk Island</option>\
                                    <option value="KP">North Korea</option>\
                                    <option value="MP">Northern Mariana Islands</option>\
                                    <option value="NO">Norway</option>\
                                    <option value="OM">Oman</option>\
                                    <option value="PK">Pakistan</option>\
                                    <option value="PW">Palau</option>\
                                    <option value="PS">Palestine, State of</option>\
                                    <option value="PA">Panama</option>\
                                    <option value="PG">Papua New Guinea</option>\
                                    <option value="PY">Paraguay</option>\
                                    <option value="PE">Peru</option>\
                                    <option value="PH">Philippines</option>\
                                    <option value="PN">Pitcairn</option>\
                                    <option value="PL">Poland</option>\
                                    <option value="PT">Portugal</option>\
                                    <option value="PR">Puerto Rico</option>\
                                    <option value="QA">Qatar</option>\
                                    <option value="RE">Réunion</option>\
                                    <option value="RO">Romania</option>\
                                    <option value="RU">Russia</option>\
                                    <option value="RW">Rwanda</option>\
                                    <option value="BL">Saint Barthélemy</option>\
                                    <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>\
                                    <option value="KN">Saint Kitts and Nevis</option>\
                                    <option value="LC">Saint Lucia</option>\
                                    <option value="MF">Saint Martin (French part)</option>\
                                    <option value="PM">Saint Pierre and Miquelon</option>\
                                    <option value="VC">Saint Vincent and the Grenadines</option>\
                                    <option value="WS">Samoa</option>\
                                    <option value="SM">San Marino</option>\
                                    <option value="ST">Sao Tome and Principe</option>\
                                    <option value="SA">Saudi Arabia</option>\
                                    <option value="SN">Senegal</option>\
                                    <option value="RS">Serbia</option>\
                                    <option value="SC">Seychelles</option>\
                                    <option value="SL">Sierra Leone</option>\
                                    <option value="SG">Singapore</option>\
                                    <option value="SX">Sint Maarten (Dutch part)</option>\
                                    <option value="SK">Slovakia</option>\
                                    <option value="SI">Slovenia</option>\
                                    <option value="SB">Solomon Islands</option>\
                                    <option value="SO">Somalia</option>\
                                    <option value="ZA">South Africa</option>\
                                    <option value="GS">South Georgia and the South Sandwich Islands</option>\
                                    <option value="KR">South Korea</option>\
                                    <option value="SS">South Sudan</option>\
                                    <option value="ES">Spain</option>\
                                    <option value="LK">Sri Lanka</option>\
                                    <option value="SD">Sudan</option>\
                                    <option value="SR">Suriname</option>\
                                    <option value="SJ">Svalbard and Jan Mayen</option>\
                                    <option value="SZ">Swaziland</option>\
                                    <option value="SE">Sweden</option>\
                                    <option value="CH">Switzerland</option>\
                                    <option value="SY">Syria</option>\
                                    <option value="TW">Taiwan</option>\
                                    <option value="TJ">Tajikistan</option>\
                                    <option value="TZ">Tanzania</option>\
                                    <option value="TH">Thailand</option>\
                                    <option value="TL">Timor-Leste</option>\
                                    <option value="TG">Togo</option>\
                                    <option value="TK">Tokelau</option>\
                                    <option value="TO">Tonga</option>\
                                    <option value="TT">Trinidad and Tobago</option>\
                                    <option value="TN">Tunisia</option>\
                                    <option value="TR">Turkey</option>\
                                    <option value="TM">Turkmenistan</option>\
                                    <option value="TC">Turks and Caicos Islands</option>\
                                    <option value="TV">Tuvalu</option>\
                                    <option value="UG">Uganda</option>\
                                    <option value="UA">Ukraine</option>\
                                    <option value="AE">United Arab Emirates</option>\
                                    <option value="GB">United Kingdom</option>\
                                    <option value="UM">United States Minor Outlying Islands</option>\
                                    <option value="US">United States of America</option>\
                                    <option value="UY">Uruguay</option>\
                                    <option value="UZ">Uzbekistan</option>\
                                    <option value="VU">Vanuatu</option>\
                                    <option value="VE">Venezuela</option>\
                                    <option value="VN">Vietnam</option>\
                                    <option value="VG">Virgin Islands (British)</option>\
                                    <option value="VI">Virgin Islands (U.S.)</option>\
                                    <option value="WF">Wallis and Futuna</option>\
                                    <option value="EH">Western Sahara</option>\
                                    <option value="YE">Yemen</option>\
                                    <option value="ZM">Zambia</option>\
                                    <option value="ZW">Zimbabwe</option>\
                                </select>\
                                <p>\
                                    <span id="register-country-validation-error" class="tip error" aria-live="assertive">\
                                    <span class="sr-only">ERROR: </span>\
                                    <span id="register-country-validation-error-msg"></span>\
                                    </span>\
                                    <span class="tip tip-input" id="register-country-desc">The country or region where you live.</span>\
                                </p>\
                            </div>\
                            <div class="form-field plaintext-honor_code">\
                                <span class="plaintext-field">By creating an account with Thames International, you agree to abide by our Thames International                   <a href="/honor" target="_blank">Terms of Service and Honor Code</a>                   and agree to our <a href="/privacy" target="_blank">Privacy Policy</a>.</span>\
                                <input id="register-honor_code" type="hidden" name="honor_code" class="input-block" value="true" />\
                            </div>\
                        </div>\
                        <div class="optional-fields">\
                            <div class="form-field select-gender">\
                                <label for="register-gender">\
                                    <span class="label-text">Gender</span>\
                                    <span class="label-optional" id="register-gender-optional-label">(optional)</span>\
                                </label>\
                                <select id="register-gender" name="gender" class="input-inline">\
                                    <option value="" data-isdefault="true" selected>--</option>\
                                    <option value="m">Male</option>\
                                    <option value="f">Female</option>\
                                    <option value="o">Other/Prefer Not to Say</option>\
                                </select>\
                                <span id="register-gender-validation-error" class="tip error" aria-live="assertive">\
                                            <span class="sr-only">ERROR: </span>\
                                <span id="register-gender-validation-error-msg"></span>\
                                </span>\
                            </div>\
                            <div class="form-field select-year_of_birth">\
                                <label for="register-year_of_birth">\
                                    <span class="label-text">Year of birth</span>\
                                    <span class="label-optional" id="register-year_of_birth-optional-label">(optional)</span>\
                                </label>\
                                <select id="register-year_of_birth" name="year_of_birth" class="input-inline">\
                                    <option value="" data-isdefault="true" selected>--</option>\
                                        <option value="2018">2018</option>\
                                        <option value="2017">2017</option>\
                                        <option value="2016">2016</option>\
                                        <option value="2015">2015</option>\
                                        <option value="2014">2014</option>\
                                        <option value="2013">2013</option>\
                                        <option value="2012">2012</option>\
                                        <option value="2011">2011</option>\
                                        <option value="2010">2010</option>\
                                        <option value="2009">2009</option>\
                                        <option value="2008">2008</option>\
                                        <option value="2007">2007</option>\
                                        <option value="2006">2006</option>\
                                        <option value="2005">2005</option>\
                                        <option value="2004">2004</option>\
                                        <option value="2003">2003</option>\
                                        <option value="2002">2002</option>\
                                        <option value="2001">2001</option>\
                                        <option value="2000">2000</option>\
                                        <option value="1999">1999</option>\
                                        <option value="1998">1998</option>\
                                        <option value="1997">1997</option>\
                                        <option value="1996">1996</option>\
                                        <option value="1995">1995</option>\
                                        <option value="1994">1994</option>\
                                        <option value="1993">1993</option>\
                                        <option value="1992">1992</option>\
                                        <option value="1991">1991</option>\
                                        <option value="1990">1990</option>\
                                        <option value="1989">1989</option>\
                                        <option value="1988">1988</option>\
                                        <option value="1987">1987</option>\
                                        <option value="1986">1986</option>\
                                        <option value="1985">1985</option>\
                                        <option value="1984">1984</option>\
                                        <option value="1983">1983</option>\
                                        <option value="1982">1982</option>\
                                        <option value="1981">1981</option>\
                                        <option value="1980">1980</option>\
                                        <option value="1979">1979</option>\
                                        <option value="1978">1978</option>\
                                        <option value="1977">1977</option>\
                                        <option value="1976">1976</option>\
                                        <option value="1975">1975</option>\
                                        <option value="1974">1974</option>\
                                        <option value="1973">1973</option>\
                                        <option value="1972">1972</option>\
                                        <option value="1971">1971</option>\
                                        <option value="1970">1970</option>\
                                        <option value="1969">1969</option>\
                                        <option value="1968">1968</option>\
                                        <option value="1967">1967</option>\
                                        <option value="1966">1966</option>\
                                        <option value="1965">1965</option>\
                                        <option value="1964">1964</option>\
                                        <option value="1963">1963</option>\
                                        <option value="1962">1962</option>\
                                        <option value="1961">1961</option>\
                                        <option value="1960">1960</option>\
                                        <option value="1959">1959</option>\
                                        <option value="1958">1958</option>\
                                        <option value="1957">1957</option>\
                                        <option value="1956">1956</option>\
                                        <option value="1955">1955</option>\
                                        <option value="1954">1954</option>\
                                        <option value="1953">1953</option>\
                                        <option value="1952">1952</option>\
                                        <option value="1951">1951</option>\
                                        <option value="1950">1950</option>\
                                        <option value="1949">1949</option>\
                                        <option value="1948">1948</option>\
                                        <option value="1947">1947</option>\
                                        <option value="1946">1946</option>\
                                        <option value="1945">1945</option>\
                                        <option value="1944">1944</option>\
                                        <option value="1943">1943</option>\
                                        <option value="1942">1942</option>\
                                        <option value="1941">1941</option>\
                                        <option value="1940">1940</option>\
                                        <option value="1939">1939</option>\
                                        <option value="1938">1938</option>\
                                        <option value="1937">1937</option>\
                                        <option value="1936">1936</option>\
                                        <option value="1935">1935</option>\
                                        <option value="1934">1934</option>\
                                        <option value="1933">1933</option>\
                                        <option value="1932">1932</option>\
                                        <option value="1931">1931</option>\
                                        <option value="1930">1930</option>\
                                        <option value="1929">1929</option>\
                                        <option value="1928">1928</option>\
                                        <option value="1927">1927</option>\
                                        <option value="1926">1926</option>\
                                        <option value="1925">1925</option>\
                                        <option value="1924">1924</option>\
                                        <option value="1923">1923</option>\
                                        <option value="1922">1922</option>\
                                        <option value="1921">1921</option>\
                                        <option value="1920">1920</option>\
                                        <option value="1919">1919</option>\
                                        <option value="1918">1918</option>\
                                        <option value="1917">1917</option>\
                                        <option value="1916">1916</option>\
                                        <option value="1915">1915</option>\
                                        <option value="1914">1914</option>\
                                        <option value="1913">1913</option>\
                                        <option value="1912">1912</option>\
                                        <option value="1911">1911</option>\
                                        <option value="1910">1910</option>\
                                        <option value="1909">1909</option>\
                                        <option value="1908">1908</option>\
                                        <option value="1907">1907</option>\
                                        <option value="1906">1906</option>\
                                        <option value="1905">1905</option>\
                                        <option value="1904">1904</option>\
                                        <option value="1903">1903</option>\
                                        <option value="1902">1902</option>\
                                        <option value="1901">1901</option>\
                                        <option value="1900">1900</option>\
                                        <option value="1899">1899</option>\
                                </select>\
                                <span id="register-year_of_birth-validation-error" class="tip error" aria-live="assertive">\
                                    <span class="sr-only">ERROR: </span>\
                                    <span id="register-year_of_birth-validation-error-msg"></span>\
                                </span>\
                            </div>\
                            <div class="form-field select-level_of_education">\
                                <label for="register-level_of_education">\
                                    <span class="label-text">Highest level of education completed</span>\
                                    <span class="label-optional" id="register-level_of_education-optional-label">(optional)</span>\
                                </label>\
                                <select id="register-level_of_education" name="level_of_education" class="input-inline" data-errormsg-required="Select the highest level of education you have completed.">\
                                    <option value="" data-isdefault="true" selected>--</option>\
                                    <option value="p">Doctorate</option>\
                                    <option value="m">Master&#x27;s or professional degree</option>\
                                    <option value="b">Bachelor&#x27;s degree</option>\
                                    <option value="a">Associate degree</option>\
                                    <option value="hs">Secondary/high school</option>\
                                    <option value="jhs">Junior secondary/junior high/middle school</option>\
                                    <option value="el">Elementary/primary school</option>\
                                    <option value="none">No formal education</option>\
                                    <option value="other">Other education</option>\
                                </select>\
                                <span id="register-level_of_education-validation-error" class="tip error" aria-live="assertive">\
                                    <span class="sr-only">ERROR: </span>\
                                    <span id="register-level_of_education-validation-error-msg"></span>\
                                </span>\
                            </div>\
                            <div class="form-field textarea-goals">\
                                <label for="register-goals">\
                                    <span class="label-text">Tell us why you&#x27;re interested in Thames International</span>\
                                    <span class="label-optional" id="register-goals-optional-label">(optional)</span>\
                                </label>\
                                <textarea id="register-goals" type="textarea" name="goals" class="input-block" data-errormsg-required="Tell us your goals.">\</textarea>\
                                <span id="register-goals-validation-error" class="tip error" aria-live="assertive">\
                                    <span class="sr-only">ERROR: </span>\
                                    <span id="register-goals-validation-error-msg"></span>\
                                </span>\
                            </div>\
                        </div>';
                    var s = i || "",
                        n = r("An error occurred.");
                    return e(this.el).html(t.template(this.tpl)({
                        context: {
                            fields: s,
                            currentProvider: this.currentProvider,
                            syncLearnerProfileData: this.syncLearnerProfileData,
                            providers: this.providers,
                            hasSecondaryProviders: this.hasSecondaryProviders,
                            platformName: this.platformName,
                            autoRegisterWelcomeMessage: this.autoRegisterWelcomeMessage,
                            registerFormSubmitButtonText: this.registerFormSubmitButtonText
                        }
                    })), this.postRender(), this.errorMessage ? this.renderErrors(n, [this.errorMessage]) : this.currentProvider && !this.hideAuthWarnings && this.renderAuthWarning(), this.autoSubmit && (e(this.el).hide(), e("#register-honor_code, #register-terms_of_service").prop("checked", !0), this.submitForm()), this
                },
                postRender: function() {
                    var t = this.$(".form-field"),
                        r = "input, select, textarea",
                        i = ["tip error", "tip tip-input"],
                        s = ["tip error hidden", "tip tip-input hidden"],
                        a = function() {
                            e(this).find("label").addClass("focus-in").removeClass("focus-out"), e(this).children().each(function() {
                                s.indexOf(e(this).attr("class")) >= 0 && e(this).removeClass("hidden")
                            })
                        },
                        o = function() {
                            0 === e(this).find(r).val().length && e(this).find("label").addClass("focus-out").removeClass("focus-in"), e(this).children().each(function() {
                                i.indexOf(e(this).attr("class")) >= 0 && e(this).addClass("hidden")
                            })
                        },
                        l = function(t) {
                            0 === t.find(r).val().length && t.find("label").addClass("focus-out").removeClass("focus-in"), t.children().each(function() {
                                i.indexOf(e(this).attr("class")) >= 0 && e(this).addClass("hidden")
                            }), t.focusin(a), t.focusout(o)
                        },
                        u = function() {
                            e(t).each(function() {
                                var t = e(this),
                                    i = -1 !== t.attr("class").indexOf("checkbox");
                                i || (0 !== t.find(r).val().length || t.is(":-webkit-autofill") ? t.find("label").addClass("focus-in").removeClass("focus-out") : t.find("label").addClass("focus-out").removeClass("focus-in"))
                            })
                        };
                    n.prototype.postRender.call(this), e(".optional-fields").addClass("hidden"), e("#toggle_optional_fields").change(function() {
                        window.analytics.track("edx.bi.user.register.optional_fields_selected"), e(".optional-fields").toggleClass("hidden")
                    }), e(".checkbox-optional_fields_toggle").insertAfter(".required-fields"), this.hasOptionalFields || e(".checkbox-optional_fields_toggle").addClass("hidden"), e(".checkbox-honor_code").insertAfter(".optional-fields"), e(".checkbox-terms_of_service").insertAfter(".optional-fields"), e("label a").click(function(t) {
                        t.stopPropagation(), t.preventDefault(), window.open(e(this).attr("href"), e(this).attr("target"))
                    }), e(".form-field").each(function() {
                        e(this).find("option:first").html("")
                    }), e(t).each(function() {
                        var t = e(this),
                            r = -1 !== t.attr("class").indexOf("checkbox");
                        t.length > 0 && !r && l(t)
                    }), setTimeout(u, 1e3)
                },
                hideRequiredMessageExceptOnError: function(e) {
                    e.hasClass("error") || this.hideRequiredMessage(e)
                },
                hideRequiredMessage: function(e) {
                    this.doOnInputLabel(e, function(e) {
                        e.addClass("hidden")
                    })
                },
                doOnInputLabel: function(e, t) {
                    var r = this.getRequiredTextLabel(e);
                    t(r)
                },
                handleRequiredInputFocus: function(t) {
                    var r = e(t.currentTarget);
                    "checkbox" !== r.attr("type") && this.renderRequiredMessage(r), r.hasClass("error") && this.doOnInputLabel(r, function(e) {
                        e.addClass("error")
                    })
                },
                renderRequiredMessage: function(e) {
                    this.doOnInputLabel(e, function(e) {
                        e.removeClass("hidden").text(r("(required)"))
                    })
                },
                getRequiredTextLabel: function(t) {
                    return e("#" + t.attr("id") + "-required-label")
                },
                renderLiveValidations: function(e, t) {
                    var r = this.getLabel(e),
                        i = this.getRequiredTextLabel(e),
                        s = this.getIcon(e),
                        n = this.getErrorTip(e),
                        a = e.attr("name"),
                        o = e.attr("type"),
                        l = "checkbox" === o,
                        u = "" !== t.validation_decisions[a],
                        d = l ? "" : t.validation_decisions[a];
                    u && this.negativeValidationEnabled ? this.renderLiveValidationError(e, r, i, s, n, d) : this.positiveValidationEnabled && this.renderLiveValidationSuccess(e, r, i, s, n)
                },
                getLabel: function(e) {
                    return this.$form.find("label[for=" + e.attr("id") + "]")
                },
                getIcon: function(t) {
                    return e("#" + t.attr("id") + "-validation-icon")
                },
                getErrorTip: function(t) {
                    return e("#" + t.attr("id") + "-validation-error-msg")
                },
                getFieldTimeout: function(t) {
                    return e("#" + t.attr("id")).attr("timeout-id") || null
                },
                setFieldTimeout: function(e, t, r) {
                    e.attr("timeout-id", setTimeout(r, t))
                },
                clearFieldTimeout: function(e) {
                    var t = this.getFieldTimeout(e);
                    t && (clearTimeout(this.getFieldTimeout(e)), e.removeAttr("timeout-id"))
                },
                renderLiveValidationError: function(e, t, r, i, s, n) {
                    this.removeLiveValidationIndicators(e, t, r, i, "success", this.positiveValidationIcon), this.addLiveValidationIndicators(e, t, r, i, s, "error", this.negativeValidationIcon, n), this.renderRequiredMessage(e)
                },
                renderLiveValidationSuccess: function(e, t, r, i, s) {
                    var n = this,
                        a = 1e3 * this.successfulValidationDisplaySeconds;
                    this.removeLiveValidationIndicators(e, t, r, i, "error", this.negativeValidationIcon), this.addLiveValidationIndicators(e, t, r, i, s, "success", this.positiveValidationIcon, ""), this.hideRequiredMessage(e), this.clearFieldTimeout(e), this.setFieldTimeout(e, a, function() {
                        n.removeLiveValidationIndicators(e, t, r, i, "success", n.positiveValidationIcon), n.clearFieldTimeout(e)
                    })
                },
                addLiveValidationIndicators: function(e, t, r, i, s, n, a, o) {
                    e.addClass(n), t.addClass(n), r.addClass(n), i.addClass(n + " " + a), s.text(o)
                },
                removeLiveValidationIndicators: function(e, t, r, i, s, n) {
                    e.removeClass(s), t.removeClass(s), r.removeClass(s), i.removeClass(s + " " + n)
                },
                thirdPartyAuth: function(t) {
                    var r = e(t.currentTarget).data("provider-url") || "";
                    r && (window.location.href = r)
                },
                saveSuccess: function() {
                    this.trigger("auth-complete")
                },
                saveError: function(r) {
                    e(this.el).show(), this.errors = t.flatten(t.map(JSON.parse(r.responseText || "[]"), function(e) {
                        return t.map(e, function(e) {
                            return i.interpolate("<li>{error}</li>", {
                                error: e.user_message
                            })
                        })
                    })), this.renderErrors(this.defaultFormErrorsTitle, this.errors), this.scrollToFormFeedback(), this.toggleDisableButton(!1)
                },
                postFormSubmission: function() {
                    t.compact(this.errors).length && e(this.el).show()
                },
                resetValidationVariables: function() {
                    this.positiveValidationEnabled = !0, this.negativeValidationEnabled = !0
                },
                renderAuthWarning: function() {
                    var e = r("You've successfully signed into %(currentProvider)s."),
                        i = r("We just need a little more information before you start learning with %(platformName)s."),
                        s = t.sprintf(e + " " + i, {
                            currentProvider: this.currentProvider,
                            platformName: this.platformName
                        });
                    this.renderFormFeedback(this.formStatusTpl, {
                        jsHook: this.authWarningJsHook,
                        message: s
                    })
                },
                submitForm: function(t) {
                    var r, i, s = this.$form[0].elements;
                    for (this.positiveValidationEnabled = !1, i = 0; i < s.length; i++) r = e(s[i]), r.attr("required") && r.blur();
                    n.prototype.submitForm.apply(this, arguments)
                },
                getFormData: function() {
                    var e = n.prototype.getFormData.apply(this, arguments),
                        t = this.$form.find("input[name=email]"),
                        r = this.$form.find("input[name=confirm_email]");
                    return r.length && (r.val() && t.val() === r.val() || this.errors.push(i.interpolate("<li>{error}</li>", {
                        error: r.data("errormsg-required")
                    })), e.confirm_email = r.val()), e
                },
                liveValidateHandler: function(t) {
                    var r = e(t.currentTarget);
                    this.inLiveValidationFields(r) ? "checkbox" === r.attr("type") ? this.liveValidateCheckbox(r) : this.liveValidate(r) : this.genericLiveValidateHandler(r), this.hideRequiredMessageExceptOnError(r)
                },
                liveValidate: function(t) {
                    var r, i, s = {};
                    for (i = 0; i < this.liveValidationFields.length; ++i) r = this.liveValidationFields[i], s[r] = e("#register-" + r).val();
                    n.prototype.liveValidate(t, this.validationUrl, "json", s, "POST", this.model)
                },
                liveValidateCheckbox: function(e) {
                    var t = {
                            validation_decisions: {}
                        },
                        r = t.validation_decisions,
                        i = e.attr("name"),
                        s = e.is(":checked"),
                        n = e.data("errormsg-required");
                    r[i] = s ? "" : n, this.renderLiveValidations(e, t)
                },
                genericLiveValidateHandler: function(e) {
                    var t = e.attr("type");
                    "checkbox" === t ? this.liveValidateCheckbox(e) : this.genericLiveValidate(e)
                },
                genericLiveValidate: function(e) {
                    var t = {
                            validation_decisions: {}
                        },
                        r = t.validation_decisions,
                        i = e.attr("name"),
                        s = e.data("errormsg-required");
                    r[i] = e.val() ? "" : s, this.renderLiveValidations(e, t)
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/InstitutionLoginView", ["jquery", "underscore", "backbone"], function(e, t, r) {
            return r.View.extend({
                el: "#institution_login-form",
                initialize: function(t) {
                    var r = "register" == t.mode ? "#institution_register-tpl" : "#institution_login-tpl";
                    this.tpl = e(r).html(), this.providers = t.thirdPartyAuth.secondaryProviders || [], this.platformName = t.platformName
                },
                render: function() {
                    return e(this.el).html(t.template(this.tpl)({
                        providers: this.providers,
                        platformName: this.platformName
                    })), this
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/HintedLoginView", ["jquery", "underscore", "backbone"], function(e, t, r) {
            return r.View.extend({
                el: "#hinted-login-form",
                tpl: "#hinted_login-tpl",
                events: {
                    "click .proceed-button": "proceedWithHintedAuth"
                },
                formType: "hinted-login",
                initialize: function(r) {
                    this.tpl = e(this.tpl).html(), this.hintedProvider = t.findWhere(r.thirdPartyAuth.providers, {
                        id: r.hintedProvider
                    }) || t.findWhere(r.thirdPartyAuth.secondaryProviders, {
                        id: r.hintedProvider
                    })
                },
                render: function() {
                    return e(this.el).html(t.template(this.tpl)({
                        hintedProvider: this.hintedProvider
                    })), this
                },
                proceedWithHintedAuth: function(e) {
                    this.redirect(this.hintedProvider.loginUrl)
                },
                redirect: function(e) {
                    window.location.href = e
                }
            })
        })
    }.call(this, define || RequireJS.define), "object" != typeof JSON && (JSON = {}),
    function() {
        "use strict";
        function f(e) {
            return 10 > e ? "0" + e : e
        }
        function quote(e) {
            return escapable.lastIndex = 0, escapable.test(e) ? '"' + e.replace(escapable, function(e) {
                var t = meta[e];
                return "string" == typeof t ? t : "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
            }) + '"' : '"' + e + '"'
        }
        function str(e, t) {
            var r, i, s, n, a, o = gap,
                l = t[e];
            switch (l && "object" == typeof l && "function" == typeof l.toJSON && (l = l.toJSON(e)), "function" == typeof rep && (l = rep.call(t, e, l)), typeof l) {
                case "string":
                    return quote(l);
                case "number":
                    return isFinite(l) ? String(l) : "null";
                case "boolean":
                case "null":
                    return String(l);
                case "object":
                    if (!l) return "null";
                    if (gap += indent, a = [], "[object Array]" === Object.prototype.toString.apply(l)) {
                        for (n = l.length, r = 0; n > r; r += 1) a[r] = str(r, l) || "null";
                        return s = 0 === a.length ? "[]" : gap ? "[\n" + gap + a.join(",\n" + gap) + "\n" + o + "]" : "[" + a.join(",") + "]", gap = o, s
                    }
                    if (rep && "object" == typeof rep)
                        for (n = rep.length, r = 0; n > r; r += 1) "string" == typeof rep[r] && (i = rep[r], s = str(i, l), s && a.push(quote(i) + (gap ? ": " : ":") + s));
                    else
                        for (i in l) Object.prototype.hasOwnProperty.call(l, i) && (s = str(i, l), s && a.push(quote(i) + (gap ? ": " : ":") + s));
                    return s = 0 === a.length ? "{}" : gap ? "{\n" + gap + a.join(",\n" + gap) + "\n" + o + "}" : "{" + a.join(",") + "}", gap = o, s
            }
        }
        "function" != typeof Date.prototype.toJSON && (Date.prototype.toJSON = function(e) {
            return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + f(this.getUTCMonth() + 1) + "-" + f(this.getUTCDate()) + "T" + f(this.getUTCHours()) + ":" + f(this.getUTCMinutes()) + ":" + f(this.getUTCSeconds()) + "Z" : null
        }, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function(e) {
            return this.valueOf()
        });
        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
            gap, indent, meta = {
                "\b": "\\b",
                "  ": "\\t",
                "\n": "\\n",
                "\f": "\\f",
                "\r": "\\r",
                '"': '\\"',
                "\\": "\\\\"
            },
            rep;
        "function" != typeof JSON.stringify && (JSON.stringify = function(e, t, r) {
            var i;
            if (gap = "", indent = "", "number" == typeof r)
                for (i = 0; r > i; i += 1) indent += " ";
            else "string" == typeof r && (indent = r);
            if (rep = t, !t || "function" == typeof t || "object" == typeof t && "number" == typeof t.length) return str("", {
                "": e
            });
            throw new Error("JSON.stringify")
        }), "function" != typeof JSON.parse && (JSON.parse = function(text, reviver) {
            function walk(e, t) {
                var r, i, s = e[t];
                if (s && "object" == typeof s)
                    for (r in s) Object.prototype.hasOwnProperty.call(s, r) && (i = walk(s, r), void 0 !== i ? s[r] = i : delete s[r]);
                return reviver.call(e, t, s)
            }
            var j;
            if (text = String(text), cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function(e) {
                    return "\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
                })), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) return j = eval("(" + text + ")"), "function" == typeof reviver ? walk({
                "": j
            }, "") : j;
            throw new SyntaxError("JSON.parse")
        })
    }(),
    function(e, t) {
        "use strict";
        var r = e.History = e.History || {},
            i = e.jQuery;
        if ("undefined" != typeof r.Adapter) throw new Error("History.js Adapter has already been loaded...");
        r.Adapter = {
            bind: function(e, t, r) {
                i(e).bind(t, r)
            },
            trigger: function(e, t, r) {
                i(e).trigger(t, r)
            },
            extractEventData: function(e, r, i) {
                var s = r && r.originalEvent && r.originalEvent[e] || i && i[e] || t;
                return s
            },
            onDomLoad: function(e) {
                i(e)
            }
        }, "undefined" != typeof r.init && r.init()
    }(window),
    function(e, t) {
        "use strict";
        var r = e.document,
            i = e.setTimeout || i,
            s = e.clearTimeout || s,
            n = e.setInterval || n,
            a = e.History = e.History || {};
        if ("undefined" != typeof a.initHtml4) throw new Error("History.js HTML4 Support has already been loaded...");
        a.initHtml4 = function() {
            return "undefined" != typeof a.initHtml4.initialized ? !1 : (a.initHtml4.initialized = !0, a.enabled = !0, a.savedHashes = [], a.isLastHash = function(e) {
                var t, r = a.getHashByIndex();
                return t = e === r
            }, a.isHashEqual = function(e, t) {
                return e = encodeURIComponent(e).replace(/%25/g, "%"), t = encodeURIComponent(t).replace(/%25/g, "%"), e === t
            }, a.saveHash = function(e) {
                return a.isLastHash(e) ? !1 : (a.savedHashes.push(e), !0)
            }, a.getHashByIndex = function(e) {
                var t = null;
                return t = "undefined" == typeof e ? a.savedHashes[a.savedHashes.length - 1] : 0 > e ? a.savedHashes[a.savedHashes.length + e] : a.savedHashes[e]
            }, a.discardedHashes = {}, a.discardedStates = {}, a.discardState = function(e, t, r) {
                var i, s = a.getHashByState(e);
                return i = {
                    discardedState: e,
                    backState: r,
                    forwardState: t
                }, a.discardedStates[s] = i, !0
            }, a.discardHash = function(e, t, r) {
                var i = {
                    discardedHash: e,
                    backState: r,
                    forwardState: t
                };
                return a.discardedHashes[e] = i, !0
            }, a.discardedState = function(e) {
                var t, r = a.getHashByState(e);
                return t = a.discardedStates[r] || !1
            }, a.discardedHash = function(e) {
                var t = a.discardedHashes[e] || !1;
                return t
            }, a.recycleState = function(e) {
                var t = a.getHashByState(e);
                return a.discardedState(e) && delete a.discardedStates[t], !0
            }, a.emulated.hashChange && (a.hashChangeInit = function() {
                a.checkerFunction = null;
                var t, i, s, o, l = "",
                    u = Boolean(a.getHash());
                return a.isInternetExplorer() ? (t = "historyjs-iframe", i = r.createElement("iframe"), i.setAttribute("id", t), i.setAttribute("src", "#"), i.style.display = "none", r.body.appendChild(i), i.contentWindow.document.open(), i.contentWindow.document.close(), s = "", o = !1, a.checkerFunction = function() {
                    if (o) return !1;
                    o = !0;
                    var t = a.getHash(),
                        r = a.getHash(i.contentWindow.document);
                    return t !== l ? (l = t, r !== t && (s = r = t, i.contentWindow.document.open(), i.contentWindow.document.close(), i.contentWindow.document.location.hash = a.escapeHash(t)), a.Adapter.trigger(e, "hashchange")) : r !== s && (s = r, u && "" === r ? a.back() : a.setHash(r, !1)), o = !1, !0
                }) : a.checkerFunction = function() {
                    var t = a.getHash() || "";
                    return t !== l && (l = t, a.Adapter.trigger(e, "hashchange")), !0
                }, a.intervalList.push(n(a.checkerFunction, a.options.hashChangeInterval)), !0
            }, a.Adapter.onDomLoad(a.hashChangeInit)), a.emulated.pushState && (a.onHashChange = function(t) {
                var r, i = t && t.newURL || a.getLocationHref(),
                    s = a.getHashByUrl(i),
                    n = null,
                    o = null;
                return a.isLastHash(s) ? (a.busy(!1), !1) : (a.doubleCheckComplete(), a.saveHash(s), s && a.isTraditionalAnchor(s) ? (a.Adapter.trigger(e, "anchorchange"), a.busy(!1), !1) : (n = a.extractState(a.getFullUrl(s || a.getLocationHref()), !0), a.isLastSavedState(n) ? (a.busy(!1), !1) : (o = a.getHashByState(n), r = a.discardedState(n), r ? (a.getHashByIndex(-2) === a.getHashByState(r.forwardState) ? a.back(!1) : a.forward(!1), !1) : (a.pushState(n.data, n.title, encodeURI(n.url), !1), !0))))
            }, a.Adapter.bind(e, "hashchange", a.onHashChange), a.pushState = function(t, r, i, s) {
                if (i = encodeURI(i).replace(/%25/g, "%"), a.getHashByUrl(i)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
                if (s !== !1 && a.busy()) return a.pushQueue({
                    scope: a,
                    callback: a.pushState,
                    args: arguments,
                    queue: s
                }), !1;
                a.busy(!0);
                var n = a.createStateObject(t, r, i),
                    o = a.getHashByState(n),
                    l = a.getState(!1),
                    u = a.getHashByState(l),
                    d = a.getHash(),
                    c = a.expectedStateId == n.id;
                return a.storeState(n), a.expectedStateId = n.id, a.recycleState(n), a.setTitle(n), o === u ? (a.busy(!1), !1) : (a.saveState(n), c || a.Adapter.trigger(e, "statechange"), !a.isHashEqual(o, d) && !a.isHashEqual(o, a.getShortUrl(a.getLocationHref())) && a.setHash(o, !1), a.busy(!1), !0)
            }, a.replaceState = function(t, r, i, s) {
                if (i = encodeURI(i).replace(/%25/g, "%"), a.getHashByUrl(i)) throw new Error("History.js does not support states with fragment-identifiers (hashes/anchors).");
                if (s !== !1 && a.busy()) return a.pushQueue({
                    scope: a,
                    callback: a.replaceState,
                    args: arguments,
                    queue: s
                }), !1;
                a.busy(!0);
                var n = a.createStateObject(t, r, i),
                    o = a.getHashByState(n),
                    l = a.getState(!1),
                    u = a.getHashByState(l),
                    d = a.getStateByIndex(-2);
                return a.discardState(l, n, d), o === u ? (a.storeState(n), a.expectedStateId = n.id, a.recycleState(n), a.setTitle(n), a.saveState(n), a.Adapter.trigger(e, "statechange"), a.busy(!1)) : a.pushState(n.data, n.title, n.url, !1), !0
            }), a.emulated.pushState && a.getHash() && !a.emulated.hashChange && a.Adapter.onDomLoad(function() {
                a.Adapter.trigger(e, "hashchange")
            }), void 0)
        }, "undefined" != typeof a.init && a.init()
    }(window),
    function(e, t) {
        "use strict";
        var r = e.console || t,
            i = e.document,
            s = e.navigator,
            n = !1,
            a = e.setTimeout,
            o = e.clearTimeout,
            l = e.setInterval,
            u = e.clearInterval,
            d = e.JSON,
            c = e.alert,
            h = e.History = e.History || {},
            f = e.history;
        try {
            n = e.sessionStorage, n.setItem("TEST", "1"), n.removeItem("TEST")
        } catch (p) {
            n = !1
        }
        if (d.stringify = d.stringify || d.encode, d.parse = d.parse || d.decode, "undefined" != typeof h.init) throw new Error("History.js Core has already been loaded...");
        h.init = function(e) {
            return "undefined" == typeof h.Adapter ? !1 : ("undefined" != typeof h.initCore && h.initCore(), "undefined" != typeof h.initHtml4 && h.initHtml4(), !0)
        }, h.initCore = function(p) {
            if ("undefined" != typeof h.initCore.initialized) return !1;
            if (h.initCore.initialized = !0, h.options = h.options || {}, h.options.hashChangeInterval = h.options.hashChangeInterval || 100, h.options.safariPollInterval = h.options.safariPollInterval || 500, h.options.doubleCheckInterval = h.options.doubleCheckInterval || 500, h.options.disableSuid = h.options.disableSuid || !1, h.options.storeInterval = h.options.storeInterval || 1e3, h.options.busyDelay = h.options.busyDelay || 250, h.options.debug = h.options.debug || !1, h.options.initialTitle = h.options.initialTitle || i.title, h.options.html4Mode = h.options.html4Mode || !1, h.options.delayInit = h.options.delayInit || !1, h.intervalList = [], h.clearAllIntervals = function() {
                    var e, t = h.intervalList;
                    if ("undefined" != typeof t && null !== t) {
                        for (e = 0; e < t.length; e++) u(t[e]);
                        h.intervalList = null
                    }
                }, h.debug = function() {
                    (h.options.debug || !1) && h.log.apply(h, arguments)
                }, h.log = function() {
                    var e, t, s, n, a, o = "undefined" != typeof r && "undefined" != typeof r.log && "undefined" != typeof r.log.apply,
                        l = i.getElementById("log");
                    for (o ? (n = Array.prototype.slice.call(arguments), e = n.shift(), "undefined" != typeof r.debug ? r.debug.apply(r, [e, n]) : r.log.apply(r, [e, n])) : e = "\n" + arguments[0] + "\n", t = 1, s = arguments.length; s > t; ++t) {
                        if (a = arguments[t], "object" == typeof a && "undefined" != typeof d) try {
                            a = d.stringify(a)
                        } catch (u) {}
                        e += "\n" + a + "\n"
                    }
                    return l ? (l.value += e + "\n-----\n", l.scrollTop = l.scrollHeight - l.clientHeight) : o || c(e), !0
                }, h.getInternetExplorerMajorVersion = function() {
                    var e = h.getInternetExplorerMajorVersion.cached = "undefined" != typeof h.getInternetExplorerMajorVersion.cached ? h.getInternetExplorerMajorVersion.cached : function() {
                        for (var e = 3, t = i.createElement("div"), r = t.getElementsByTagName("i");
                            (t.innerHTML = "<!--[if gt IE " + ++e + "]><i></i><![endif]-->") && r[0];);
                        return e > 4 ? e : !1
                    }();
                    return e
                }, h.isInternetExplorer = function() {
                    var e = h.isInternetExplorer.cached = "undefined" != typeof h.isInternetExplorer.cached ? h.isInternetExplorer.cached : Boolean(h.getInternetExplorerMajorVersion());
                    return e
                }, h.options.html4Mode ? h.emulated = {
                    pushState: !0,
                    hashChange: !0
                } : h.emulated = {
                    pushState: !Boolean(e.history && e.history.pushState && e.history.replaceState && !/ Mobile\/([1-7][a-z]|(8([abcde]|f(1[0-8]))))/i.test(s.userAgent) && !/AppleWebKit\/5([0-2]|3[0-2])/i.test(s.userAgent)),
                    hashChange: Boolean(!("onhashchange" in e || "onhashchange" in i) || h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8)
                }, h.enabled = !h.emulated.pushState, h.bugs = {
                    setHash: Boolean(!h.emulated.pushState && "Apple Computer, Inc." === s.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(s.userAgent)),
                    safariPoll: Boolean(!h.emulated.pushState && "Apple Computer, Inc." === s.vendor && /AppleWebKit\/5([0-2]|3[0-3])/.test(s.userAgent)),
                    ieDoubleCheck: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 8),
                    hashEscape: Boolean(h.isInternetExplorer() && h.getInternetExplorerMajorVersion() < 7)
                }, h.isEmptyObject = function(e) {
                    for (var t in e)
                        if (e.hasOwnProperty(t)) return !1;
                    return !0
                }, h.cloneObject = function(e) {
                    var t, r;
                    return e ? (t = d.stringify(e), r = d.parse(t)) : r = {}, r
                }, h.getRootUrl = function() {
                    var e = i.location.protocol + "//" + (i.location.hostname || i.location.host);
                    return i.location.port && (e += ":" + i.location.port), e += "/"
                }, h.getBaseHref = function() {
                    var e = i.getElementsByTagName("base"),
                        t = null,
                        r = "";
                    return 1 === e.length && (t = e[0], r = t.href.replace(/[^\/]+$/, "")), r = r.replace(/\/+$/, ""), r && (r += "/"), r
                }, h.getBaseUrl = function() {
                    var e = h.getBaseHref() || h.getBasePageUrl() || h.getRootUrl();
                    return e
                }, h.getPageUrl = function() {
                    var e, t = h.getState(!1, !1),
                        r = (t || {}).url || h.getLocationHref();
                    return e = r.replace(/\/+$/, "").replace(/[^\/]+$/, function(e, t, r) {
                        return /\./.test(e) ? e : e + "/"
                    })
                }, h.getBasePageUrl = function() {
                    var e = h.getLocationHref().replace(/[#\?].*/, "").replace(/[^\/]+$/, function(e, t, r) {
                        return /[^\/]$/.test(e) ? "" : e
                    }).replace(/\/+$/, "") + "/";
                    return e
                }, h.getFullUrl = function(e, t) {
                    var r = e,
                        i = e.substring(0, 1);
                    return t = "undefined" == typeof t ? !0 : t, /[a-z]+\:\/\//.test(e) || (r = "/" === i ? h.getRootUrl() + e.replace(/^\/+/, "") : "#" === i ? h.getPageUrl().replace(/#.*/, "") + e : "?" === i ? h.getPageUrl().replace(/[\?#].*/, "") + e : t ? h.getBaseUrl() + e.replace(/^(\.\/)+/, "") : h.getBasePageUrl() + e.replace(/^(\.\/)+/, "")), r.replace(/\#$/, "")
                }, h.getShortUrl = function(e) {
                    var t = e,
                        r = h.getBaseUrl(),
                        i = h.getRootUrl();
                    return h.emulated.pushState && (t = t.replace(r, "")), t = t.replace(i, "/"), h.isTraditionalAnchor(t) && (t = "./" + t), t = t.replace(/^(\.\/)+/g, "./").replace(/\#$/, "")
                }, h.getLocationHref = function(e) {
                    return e = e || i, e.URL === e.location.href ? e.location.href : e.location.href === decodeURIComponent(e.URL) ? e.URL : e.location.hash && decodeURIComponent(e.location.href.replace(/^[^#]+/, "")) === e.location.hash ? e.location.href : -1 == e.URL.indexOf("#") && -1 != e.location.href.indexOf("#") ? e.location.href : e.URL || e.location.href
                }, h.store = {}, h.idToState = h.idToState || {}, h.stateToId = h.stateToId || {}, h.urlToId = h.urlToId || {}, h.storedStates = h.storedStates || [], h.savedStates = h.savedStates || [], h.normalizeStore = function() {
                    h.store.idToState = h.store.idToState || {}, h.store.urlToId = h.store.urlToId || {}, h.store.stateToId = h.store.stateToId || {}
                }, h.getState = function(e, t) {
                    "undefined" == typeof e && (e = !0), "undefined" == typeof t && (t = !0);
                    var r = h.getLastSavedState();
                    return !r && t && (r = h.createStateObject()), e && (r = h.cloneObject(r), r.url = r.cleanUrl || r.url), r
                }, h.getIdByState = function(e) {
                    var t, r = h.extractId(e.url);
                    if (!r)
                        if (t = h.getStateString(e), "undefined" != typeof h.stateToId[t]) r = h.stateToId[t];
                        else if ("undefined" != typeof h.store.stateToId[t]) r = h.store.stateToId[t];
                    else {
                        for (; r = (new Date).getTime() + String(Math.random()).replace(/\D/g, ""), "undefined" != typeof h.idToState[r] || "undefined" != typeof h.store.idToState[r];);
                        h.stateToId[t] = r, h.idToState[r] = e
                    }
                    return r
                }, h.normalizeState = function(e) {
                    var t, r;
                    return e && "object" == typeof e || (e = {}), "undefined" != typeof e.normalized ? e : (e.data && "object" == typeof e.data || (e.data = {}), t = {}, t.normalized = !0, t.title = e.title || "", t.url = h.getFullUrl(e.url ? e.url : h.getLocationHref()), t.hash = h.getShortUrl(t.url), t.data = h.cloneObject(e.data), t.id = h.getIdByState(t), t.cleanUrl = t.url.replace(/\??\&_suid.*/, ""), t.url = t.cleanUrl, r = !h.isEmptyObject(t.data), (t.title || r) && h.options.disableSuid !== !0 && (t.hash = h.getShortUrl(t.url).replace(/\??\&_suid.*/, ""), /\?/.test(t.hash) || (t.hash += "?"), t.hash += "&_suid=" + t.id), t.hashedUrl = h.getFullUrl(t.hash), (h.emulated.pushState || h.bugs.safariPoll) && h.hasUrlDuplicate(t) && (t.url = t.hashedUrl), t)
                }, h.createStateObject = function(e, t, r) {
                    var i = {
                        data: e,
                        title: t,
                        url: r
                    };
                    return i = h.normalizeState(i)
                }, h.getStateById = function(e) {
                    e = String(e);
                    var r = h.idToState[e] || h.store.idToState[e] || t;
                    return r
                }, h.getStateString = function(e) {
                    var t, r, i;
                    return t = h.normalizeState(e), r = {
                        data: t.data,
                        title: e.title,
                        url: e.url
                    }, i = d.stringify(r)
                }, h.getStateId = function(e) {
                    var t, r;
                    return t = h.normalizeState(e), r = t.id
                }, h.getHashByState = function(e) {
                    var t, r;
                    return t = h.normalizeState(e), r = t.hash
                }, h.extractId = function(e) {
                    var t, r, i, s;
                    return s = -1 != e.indexOf("#") ? e.split("#")[0] : e, r = /(.*)\&_suid=([0-9]+)$/.exec(s), i = r ? r[1] || e : e, t = r ? String(r[2] || "") : "", t || !1
                }, h.isTraditionalAnchor = function(e) {
                    var t = !/[\/\?\.]/.test(e);
                    return t
                }, h.extractState = function(e, t) {
                    var r, i, s = null;
                    return t = t || !1, r = h.extractId(e), r && (s = h.getStateById(r)), s || (i = h.getFullUrl(e), r = h.getIdByUrl(i) || !1, r && (s = h.getStateById(r)), !s && t && !h.isTraditionalAnchor(e) && (s = h.createStateObject(null, null, i))), s
                }, h.getIdByUrl = function(e) {
                    var r = h.urlToId[e] || h.store.urlToId[e] || t;
                    return r
                }, h.getLastSavedState = function() {
                    return h.savedStates[h.savedStates.length - 1] || t
                }, h.getLastStoredState = function() {
                    return h.storedStates[h.storedStates.length - 1] || t
                }, h.hasUrlDuplicate = function(e) {
                    var t, r = !1;
                    return t = h.extractState(e.url), r = t && t.id !== e.id
                }, h.storeState = function(e) {
                    return h.urlToId[e.url] = e.id, h.storedStates.push(h.cloneObject(e)), e
                }, h.isLastSavedState = function(e) {
                    var t, r, i, s = !1;
                    return h.savedStates.length && (t = e.id, r = h.getLastSavedState(), i = r.id, s = t === i), s
                }, h.saveState = function(e) {
                    return h.isLastSavedState(e) ? !1 : (h.savedStates.push(h.cloneObject(e)), !0)
                }, h.getStateByIndex = function(e) {
                    var t = null;
                    return t = "undefined" == typeof e ? h.savedStates[h.savedStates.length - 1] : 0 > e ? h.savedStates[h.savedStates.length + e] : h.savedStates[e]
                }, h.getCurrentIndex = function() {
                    var e = null;
                    return e = h.savedStates.length < 1 ? 0 : h.savedStates.length - 1
                }, h.getHash = function(e) {
                    var t, r = h.getLocationHref(e);
                    return t = h.getHashByUrl(r)
                }, h.unescapeHash = function(e) {
                    var t = h.normalizeHash(e);
                    return t = decodeURIComponent(t)
                }, h.normalizeHash = function(e) {
                    var t = e.replace(/[^#]*#/, "").replace(/#.*/, "");
                    return t
                }, h.setHash = function(e, t) {
                    var r, s;
                    return t !== !1 && h.busy() ? (h.pushQueue({
                        scope: h,
                        callback: h.setHash,
                        args: arguments,
                        queue: t
                    }), !1) : (h.busy(!0), r = h.extractState(e, !0), r && !h.emulated.pushState ? h.pushState(r.data, r.title, r.url, !1) : h.getHash() !== e && (h.bugs.setHash ? (s = h.getPageUrl(), h.pushState(null, null, s + "#" + e, !1)) : i.location.hash = e), h)
                }, h.escapeHash = function(t) {
                    var r = h.normalizeHash(t);
                    return r = e.encodeURIComponent(r), h.bugs.hashEscape || (r = r.replace(/\%21/g, "!").replace(/\%26/g, "&").replace(/\%3D/g, "=").replace(/\%3F/g, "?")), r
                }, h.getHashByUrl = function(e) {
                    var t = String(e).replace(/([^#]*)#?([^#]*)#?(.*)/, "$2");
                    return t = h.unescapeHash(t)
                }, h.setTitle = function(e) {
                    var t, r = e.title;
                    r || (t = h.getStateByIndex(0), t && t.url === e.url && (r = t.title || h.options.initialTitle));
                    try {
                        i.getElementsByTagName("title")[0].innerHTML = r.replace("<", "&lt;").replace(">", "&gt;").replace(" & ", " &amp; ")
                    } catch (s) {}
                    return i.title = r, h
                }, h.queues = [], h.busy = function(e) {
                    if ("undefined" != typeof e ? h.busy.flag = e : "undefined" == typeof h.busy.flag && (h.busy.flag = !1), !h.busy.flag) {
                        o(h.busy.timeout);
                        var t = function() {
                            var e, r, i;
                            if (!h.busy.flag)
                                for (e = h.queues.length - 1; e >= 0; --e) r = h.queues[e], 0 !== r.length && (i = r.shift(), h.fireQueueItem(i), h.busy.timeout = a(t, h.options.busyDelay))
                        };
                        h.busy.timeout = a(t, h.options.busyDelay)
                    }
                    return h.busy.flag
                }, h.busy.flag = !1, h.fireQueueItem = function(e) {
                    return e.callback.apply(e.scope || h, e.args || [])
                }, h.pushQueue = function(e) {
                    return h.queues[e.queue || 0] = h.queues[e.queue || 0] || [], h.queues[e.queue || 0].push(e), h
                }, h.queue = function(e, t) {
                    return "function" == typeof e && (e = {
                        callback: e
                    }), "undefined" != typeof t && (e.queue = t), h.busy() ? h.pushQueue(e) : h.fireQueueItem(e), h
                }, h.clearQueue = function() {
                    return h.busy.flag = !1, h.queues = [], h
                }, h.stateChanged = !1, h.doubleChecker = !1, h.doubleCheckComplete = function() {
                    return h.stateChanged = !0, h.doubleCheckClear(), h
                }, h.doubleCheckClear = function() {
                    return h.doubleChecker && (o(h.doubleChecker), h.doubleChecker = !1), h
                }, h.doubleCheck = function(e) {
                    return h.stateChanged = !1, h.doubleCheckClear(), h.bugs.ieDoubleCheck && (h.doubleChecker = a(function() {
                        return h.doubleCheckClear(), h.stateChanged || e(), !0
                    }, h.options.doubleCheckInterval)), h
                }, h.safariStatePoll = function() {
                    var t, r = h.extractState(h.getLocationHref());
                    return h.isLastSavedState(r) ? void 0 : (t = r, t || (t = h.createStateObject()), h.Adapter.trigger(e, "popstate"), h)
                }, h.back = function(e) {
                    return e !== !1 && h.busy() ? (h.pushQueue({
                        scope: h,
                        callback: h.back,
                        args: arguments,
                        queue: e
                    }), !1) : (h.busy(!0), h.doubleCheck(function() {
                        h.back(!1)
                    }), f.go(-1), !0)
                }, h.forward = function(e) {
                    return e !== !1 && h.busy() ? (h.pushQueue({
                        scope: h,
                        callback: h.forward,
                        args: arguments,
                        queue: e
                    }), !1) : (h.busy(!0), h.doubleCheck(function() {
                        h.forward(!1)
                    }), f.go(1), !0)
                }, h.go = function(e, t) {
                    var r;
                    if (e > 0)
                        for (r = 1; e >= r; ++r) h.forward(t);
                    else {
                        if (!(0 > e)) throw new Error("History.go: History.go requires a positive or negative integer passed.");
                        for (r = -1; r >= e; --r) h.back(t)
                    }
                    return h
                }, h.emulated.pushState) {
                var g = function() {};
                h.pushState = h.pushState || g, h.replaceState = h.replaceState || g
            } else h.onPopState = function(t, r) {
                var i, s, n = !1,
                    a = !1;
                return h.doubleCheckComplete(), i = h.getHash(), i ? (s = h.extractState(i || h.getLocationHref(), !0), s ? h.replaceState(s.data, s.title, s.url, !1) : (h.Adapter.trigger(e, "anchorchange"), h.busy(!1)), h.expectedStateId = !1, !1) : (n = h.Adapter.extractEventData("state", t, r) || !1, a = n ? h.getStateById(n) : h.expectedStateId ? h.getStateById(h.expectedStateId) : h.extractState(h.getLocationHref()), a || (a = h.createStateObject(null, null, h.getLocationHref())), h.expectedStateId = !1, h.isLastSavedState(a) ? (h.busy(!1), !1) : (h.storeState(a), h.saveState(a), h.setTitle(a), h.Adapter.trigger(e, "statechange"), h.busy(!1), !0))
            }, h.Adapter.bind(e, "popstate", h.onPopState), h.pushState = function(t, r, i, s) {
                if (h.getHashByUrl(i) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (s !== !1 && h.busy()) return h.pushQueue({
                    scope: h,
                    callback: h.pushState,
                    args: arguments,
                    queue: s
                }), !1;
                h.busy(!0);
                var n = h.createStateObject(t, r, i);
                return h.isLastSavedState(n) ? h.busy(!1) : (h.storeState(n), h.expectedStateId = n.id, f.pushState(n.id, n.title, n.url), h.Adapter.trigger(e, "popstate")), !0
            }, h.replaceState = function(t, r, i, s) {
                if (h.getHashByUrl(i) && h.emulated.pushState) throw new Error("History.js does not support states with fragement-identifiers (hashes/anchors).");
                if (s !== !1 && h.busy()) return h.pushQueue({
                    scope: h,
                    callback: h.replaceState,
                    args: arguments,
                    queue: s
                }), !1;
                h.busy(!0);
                var n = h.createStateObject(t, r, i);
                return h.isLastSavedState(n) ? h.busy(!1) : (h.storeState(n), h.expectedStateId = n.id, f.replaceState(n.id, n.title, n.url), h.Adapter.trigger(e, "popstate")), !0
            };
            if (n) {
                try {
                    h.store = d.parse(n.getItem("History.store")) || {}
                } catch (m) {
                    h.store = {}
                }
                h.normalizeStore()
            } else h.store = {}, h.normalizeStore();
            h.Adapter.bind(e, "unload", h.clearAllIntervals), h.saveState(h.storeState(h.extractState(h.getLocationHref(), !0))), n && (h.onUnload = function() {
                var e, t, r;
                try {
                    e = d.parse(n.getItem("History.store")) || {}
                } catch (i) {
                    e = {}
                }
                e.idToState = e.idToState || {}, e.urlToId = e.urlToId || {}, e.stateToId = e.stateToId || {};
                for (t in h.idToState) h.idToState.hasOwnProperty(t) && (e.idToState[t] = h.idToState[t]);
                for (t in h.urlToId) h.urlToId.hasOwnProperty(t) && (e.urlToId[t] = h.urlToId[t]);
                for (t in h.stateToId) h.stateToId.hasOwnProperty(t) && (e.stateToId[t] = h.stateToId[t]);
                h.store = e, h.normalizeStore(), r = d.stringify(e);
                try {
                    n.setItem("History.store", r)
                } catch (s) {
                    if (s.code !== DOMException.QUOTA_EXCEEDED_ERR) throw s;
                    n.length && (n.removeItem("History.store"), n.setItem("History.store", r))
                }
            }, h.intervalList.push(l(h.onUnload, h.options.storeInterval)), h.Adapter.bind(e, "beforeunload", h.onUnload), h.Adapter.bind(e, "unload", h.onUnload)), h.emulated.pushState || (h.bugs.safariPoll && h.intervalList.push(l(h.safariStatePoll, h.options.safariPollInterval)), ("Apple Computer, Inc." === s.vendor || "Mozilla" === (s.appCodeName || "")) && (h.Adapter.bind(e, "hashchange", function() {
                h.Adapter.trigger(e, "popstate")
            }), h.getHash() && h.Adapter.onDomLoad(function() {
                h.Adapter.trigger(e, "hashchange")
            })))
        }, (!h.options || !h.options.delayInit) && h.init()
    }(window), RequireJS.define("js/vendor/history", function() {}),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/views/AccessView", ["jquery", "utility", "underscore", "underscore.string", "backbone", "js/student_account/models/LoginModel", "js/student_account/models/PasswordResetModel", "js/student_account/models/RegisterModel", "js/student_account/views/LoginView", "js/student_account/views/PasswordResetView", "js/student_account/views/RegisterView", "js/student_account/views/InstitutionLoginView", "js/student_account/views/HintedLoginView", "js/vendor/history"], function(e, t, r, i, s, n, a, o, l, u, d, c, h) {
            return s.View.extend({
                tpl: "#access-tpl",
                events: {
                    "click .form-toggle": "toggleForm"
                },
                subview: {
                    login: {},
                    register: {},
                    passwordHelp: {},
                    institutionLogin: {},
                    hintedLogin: {}
                },
                nextUrl: "/dashboard",
                activeForm: "",
                initialize: function(t) {
                    r.mixin(i.exports()), this.tpl = e(this.tpl).html(), this.activeForm = t.initial_mode || "login", this.thirdPartyAuth = t.third_party_auth || {
                        currentProvider: null,
                        providers: []
                    }, this.thirdPartyAuthHint = t.third_party_auth_hint || null, this.accountActivationMessages = t.account_activation_messages || [], t.login_redirect_url && (window.isExternal(t.login_redirect_url) || (this.nextUrl = t.login_redirect_url)), this.formDescriptions = {
                        login: t.login_form_desc,
                        register: t.registration_form_desc,
                        reset: t.password_reset_form_desc,
                        institution_login: null,
                        hinted_login: null
                    }, this.platformName = t.platform_name, this.supportURL = t.support_link, this.passwordResetSupportUrl = t.password_reset_support_link, this.createAccountOption = t.account_creation_allowed, this.hideAuthWarnings = t.hide_auth_warnings || !1, this.pipelineUserDetails = t.third_party_auth.pipeline_user_details, this.enterpriseName = t.enterprise_name || "", this.resetModel = new a({}, {
                        method: "GET",
                        url: "#"
                    }), this.render(), this.thirdPartyAuth.errorMessage = null, this.accountActivationMessages = []
                },
                render: function() {
                    return e(this.el).html(r.template(this.tpl)({
                        mode: this.activeForm
                    })), this.postRender(), this
                },
                postRender: function() {
                    "forgot-password-modal" === s.history.getHash() && this.resetPassword(), this.loadForm(this.activeForm)
                },
                loadForm: function(e) {
                    var t = r.bind(this.load[e], this);
                    t(this.formDescriptions[e])
                },
                load: {
                    login: function(e) {
                        var t = new n({}, {
                            method: e.method,
                            url: e.submit_url
                        });
                        this.subview.login = new l({
                            fields: e.fields,
                            model: t,
                            resetModel: this.resetModel,
                            thirdPartyAuth: this.thirdPartyAuth,
                            accountActivationMessages: this.accountActivationMessages,
                            platformName: this.platformName,
                            supportURL: this.supportURL,
                            passwordResetSupportUrl: this.passwordResetSupportUrl,
                            createAccountOption: this.createAccountOption,
                            hideAuthWarnings: this.hideAuthWarnings,
                            pipelineUserDetails: this.pipelineUserDetails,
                            enterpriseName: this.enterpriseName
                        }), this.listenTo(this.subview.login, "password-help", this.resetPassword), this.listenTo(this.subview.login, "auth-complete", this.authComplete)
                    },
                    reset: function(t) {
                        this.resetModel.ajaxType = t.method, this.resetModel.urlRoot = t.submit_url, this.subview.passwordHelp = new u({
                            fields: t.fields,
                            model: this.resetModel
                        }), this.listenTo(this.subview.passwordHelp, "password-email-sent", this.passwordEmailSent), e(".password-reset-form").focus()
                    },
                    register: function(e) {
                        var t = new o({}, {
                            method: e.method,
                            url: e.submit_url
                        });
                        this.subview.register = new d({
                            fields: e.fields,
                            model: t,
                            thirdPartyAuth: this.thirdPartyAuth,
                            platformName: this.platformName,
                            hideAuthWarnings: this.hideAuthWarnings
                        }), this.listenTo(this.subview.register, "auth-complete", this.authComplete)
                    },
                    institution_login: function(e) {
                        this.subview.institutionLogin = new c({
                            thirdPartyAuth: this.thirdPartyAuth,
                            platformName: this.platformName,
                            mode: this.activeForm
                        }), this.subview.institutionLogin.render()
                    },
                    hinted_login: function(e) {
                        this.subview.hintedLogin = new h({
                            thirdPartyAuth: this.thirdPartyAuth,
                            hintedProvider: this.thirdPartyAuthHint,
                            platformName: this.platformName
                        }), this.subview.hintedLogin.render()
                    }
                },
                passwordEmailSent: function() {
                    var t = e("#login-anchor");
                    this.element.hide(e(this.el).find("#password-reset-anchor")), this.element.show(t), this.element.scrollTop(t)
                },
                resetPassword: function() {
                    window.analytics.track("edx.bi.password_reset_form.viewed", {
                        category: "user-engagement"
                    }), this.element.hide(e(this.el).find("#login-anchor")), this.loadForm("reset"), this.element.scrollTop(e("#password-reset-anchor"))
                },
                toggleForm: function(t) {
                    var r = e(t.currentTarget).data("type"),
                        i = e("#" + r + "-form"),
                        s = window.scrollX,
                        n = window.scrollY,
                        a = url("?"),
                        o = a.length > 0 ? "?" + a : "";
                    t.preventDefault(), window.analytics.track("edx.bi." + r + "_form.toggled", {
                        category: "user-engagement"
                    }), this.form.isLoaded(i) && "institution_login" != r || this.loadForm(r), this.activeForm = r, this.element.hide(e(this.el).find(".submission-success")), this.element.hide(e(this.el).find(".form-wrapper")), this.element.show(i), "institution_login" != r && History.pushState(null, document.title, "/" + r + o), analytics.page("login_and_registration", r), e("#" + r).focus(), window.scrollTo(s, n)
                },
                authComplete: function() {
                    this.thirdPartyAuth && this.thirdPartyAuth.finishAuthUrl ? this.redirect(this.thirdPartyAuth.finishAuthUrl) : this.redirect(this.nextUrl)
                },
                redirect: function(e) {
                    window.location.replace(e)
                },
                form: {
                    isLoaded: function(e) {
                        return e.html().length > 0
                    }
                },
                element: {
                    hide: function(e) {
                        e.addClass("hidden")
                    },
                    scrollTop: function(t) {
                        e("html,body").animate({
                            scrollTop: t.offset().top
                        }, "slow")
                    },
                    show: function(e) {
                        e.removeClass("hidden")
                    }
                }
            })
        })
    }.call(this, define || RequireJS.define),
    function(e) {
        "use strict";
        RequireJS.define("js/student_account/logistration_factory", ["jquery", "js/student_account/views/AccessView"], function(e, t) {
            return function(r) {
                var i = e("#login-and-registration-container");
                new t(_.extend(r, {
                    el: i
                }))
            }
        })
    }.call(this, define || RequireJS.define);
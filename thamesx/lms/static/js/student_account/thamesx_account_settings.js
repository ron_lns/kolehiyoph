/*tst: thames account settings*/
window.tst = {
	user: null,
	getUser: function(username){
		$.ajax({
			type 	: 'GET',
			dataType: 'json',
			url 	: '/api/user/v1/accounts/' + username,
			data	: '',
			success : function(data){
				window.tst.user = data;
				window.tst.renderDataToForm()
			}
		});
	},

	patch: function(params, field, msg){
		var u = window.tst.user;

		$("" + field).empty();
		$.ajax({
			type 	: 'PATCH',
			dataType: 'json',
			contentType:"application/merge-patch+json",
			url 	: '/api/user/v1/accounts/' + u.username,
			data	: JSON.stringify(params),
   	 		processData: false,
			success : function(data){
				window.tst.user = data;
				$("" + field).append('<span class="fa fa-check message-success" aria-hidden="true"></span> ' + msg);
			},
			error: function (){
				$("" + field).append('Failed please try again.');
			}
		});
		
		$.ajax({
			type: 'PATCH',
			dataType: 'json',
			contentType: "application/merge-patch+json",
			url: '/api/user/v1/preferences/' + u.username,
			data: JSON.stringify(params),
			processData: false,
			success: function (data) {
				window.tst.user = data;
			},
			error: function () {
			}
		});
	},

	passwordreset: function(field, msg){
		var u = window.tst.user;

		$("" + field).empty();
		$.ajax({
			type 	: 'POST',
			dataType: 'json',
			url 	: '/password_reset/',
			data	: 'email='+u.email,
   	 		processData: false,
			success : function(data){
				window.tst.user = data;
				$("" + field).append('<span class="fa fa-check message-success" aria-hidden="true"></span> ' + msg);
				$("#u-field-link-password").prop('disabled', 'disabled');
			},
			error: function (){
				$("" + field).append('Failed please try again.');
			}
		});
	},

	renderDataToForm: function(){
		this.assignToField('#field-input-name', this.user.name);
		this.assignToField('#field-input-email', this.user.email);
		this.assignToField('#u-field-select-country', this.user.country);
		this.assignToField('#u-field-select-level_of_education', this.user.level_of_education);
		this.assignToField('#u-field-select-gender', this.user.gender);
		this.assignToField('#u-field-select-year_of_birth', this.user.year_of_birth);
		this.assignToField('#u-field-select-language_proficiencies', this.user.language_proficiencies[0].code);
		this.assignToField("#u-field-select-time_zone", this.user.time_zone);
		

		$("#u-field-value-username").text(this.user.username);
		this.assignToSocial();
		this.renderPref();
		console.log(this.user);
	},

	assignToField: function(e, v){
		$(""+e+"").val(""+v+"");
	},

	assignToSocial:function(){
		var s = this.user.social_links;
		$.each(s, function( index, value ) {
			window.tst.assignToField('#field-input-social_links_' + value.platform, value.social_link);
		});
	},

	renderPref: function(){
		$.ajax({
			type 	: 'GET',
			dataType: 'json',
			url 	: '/api/user/v1/preferences/' + window.tst.user.username,
			data	: '',
			success : function(data){
				$.each(data, function( index, value ) {
					if(index != 'language_proficiencies') {
						window.tst.assignToField('#u-field-select-' + index, value);
					}
					
				});
			}
		});
	}
}

$("#field-input-name").on('focusout', function(){
	var params = {
		'name': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-name .u-field-message-notification", msg);
});

$("#field-input-email").on('focusout', function(){
	var params = {
		'email': this.value
	};
	var msg = 'We\'ve sent a confirmation message to ' + this.value;
	window.tst.patch(params, "#u-field-message-email .u-field-message-notification", msg);
});

$("#u-field-select-pref-lang").on('focusout', function(){
	var params = {
		'pref-lang': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-pref-lang .u-field-message-notification", msg);
});

$("#u-field-select-country").on('focusout', function(){
	var params = {
		'country': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, ".u-field-country .u-field-message-notification", msg);
});

$("#u-field-select-time_zone").on('focusout', function(){
	var params = {
		'time_zone': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, ".u-field-time_zone .u-field-message-notification", msg);
});

$("#u-field-select-level_of_education").on('focusout', function(){
	var params = {
		'level_of_education': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, ".u-field-level_of_education .u-field-message-notification", msg);
});

$("#u-field-select-gender").on('focusout', function(){
	var params = {
		'gender': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-gender .u-field-message-notification", msg);
});

$("#u-field-select-year_of_birth").on('focusout', function(){
	var params = {
		'year_of_birth': this.value
	};
	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-year_of_birth .u-field-message-notification", msg);
});

$("#u-field-select-language_proficiencies").on('focusout', function(){
	var params = {
		'language_proficiencies': [{
			'code': this.value
		}]
	};

	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-language_proficiencies .u-field-message-notification", msg);
});

$("#field-input-social_links_facebook").on('focusout', function(){
	var params = {
		'social_links': [{
			'platform': 'facebook',
			'social_link': this.value
		}]
	};

	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-social_links_facebook .u-field-message-notification", msg);
});

$("#field-input-social_links_linkedin").on('focusout', function(){
	var params = {
		'social_links': [{
			'platform': 'linkedin',
			'social_link': this.value
		}]
	};

	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-social_links_linkedin .u-field-message-notification", msg);
});

$("#field-input-social_links_twitter").on('focusout', function(){
	var params = {
		'social_links': [{
			'platform': 'twitter',
			'social_link': this.value
		}]
	};

	var msg = 'Your changes have been saved.';
	window.tst.patch(params, "#u-field-message-social_links_twitter .u-field-message-notification", msg);
});

$("#u-field-link-password").on('click', function(){
	var msg = ' Check your email account for instructions to reset your password.';
	window.tst.passwordreset("#u-field-message-password .u-field-message-notification", msg);
});








